import rospy
import tf
import numpy as np

rospy.init_node('test_node')
listener = tf.TransformListener()

tfs = []
while not rospy.is_shutdown():
    trans, rot = listener.lookupTransform()
    tfs.append([coord[0], coord[1], phi])
    rospy.sleep(0.05)

np.save('/home/fc/catkin_ws/src/cross_view_localization/launch/odoms.npy', tfs)