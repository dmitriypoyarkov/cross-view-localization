import argparse

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2 as cv

class RepeatedImagePublisher(object):
    def __init__(self, args):
        self._pub = rospy.Publisher(args.topic, Image, queue_size=1)
        cv_image = cv.imread(args.image)
        rospy.loginfo(f'Image loaded, shape: {cv_image.shape}')
        bridge = CvBridge()
        self._imgmsg = bridge.cv2_to_imgmsg(cv_image, 'bgr8')
        rospy.Timer(rospy.Duration(args.rate), self.publish_image)

    def publish_image(self, _):
        rospy.loginfo(f'New image published')
        self._pub.publish(self._imgmsg)


def main(args : argparse.Namespace):
    rospy.init_node('repeat_img_publisher')
    publisher = RepeatedImagePublisher(args)
    rospy.spin()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--image', type=str,
        help='path to image to publish',
        required=True)
    parser.add_argument('--rate', type=float,
        default=1.0, help='image publish rate')
    parser.add_argument('--topic', type=str,
        default='/test_img',
        help='image publish topic')
    args = parser.parse_args()
    main(args)