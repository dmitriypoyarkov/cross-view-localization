import argparse

from cross_view_localization.tools.segmentor_tool import SatSegmentorTool

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Satellite Segmentor Tool")
    parser.add_argument('--img', required=True, help='Path to the image file')
    args = parser.parse_args()
    app = SatSegmentorTool(args.img)
    app.run()
