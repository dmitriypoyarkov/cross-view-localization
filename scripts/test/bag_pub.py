import argparse
import sys
import time

import rosbag
import rospy
import tf
import tf2_ros
from rosgraph_msgs.msg import Clock

from cross_view_localization import package_settings as settings

settings.initialize('cross_view_localization')


def validate_filename(filename):
    if not filename.endswith('.bag'):
        raise argparse.ArgumentTypeError(f"File '{filename}' does not have a .bag extension.")
    return filename


def import_msg_ns(msg_type : str):
    ns = msg_type.split('/')[0]
    globals()[ns] = __import__(ns)


def get_msg_type(msg_type : str):
    # Convert 'pkg_name/msg_name' to 'pkg_name.msg.MsgName'
    parts = msg_type.split('/')
    if len(parts) == 2:
        return eval(f"{parts[0]}.msg.{parts[1]}")
    else:
        rospy.logerr(f"Invalid message type format: {msg_type}")
        return None

parser = argparse.ArgumentParser(
    description="This program publishes bag files with tf_static for CVL system."
)

parser.add_argument(
    '--skip',
    type=float,
    default=0.0,
    help='A floating point value indicating the skip parameter (default: 0.0)'
)

parser.add_argument(
    '--speed',
    type=float,
    default=1.0,
    help='Bag playback speed (default: 1.0)'
)

parser.add_argument(
    'filenames',
    type=validate_filename,
    nargs='+',
    help='One or more .bag files to publish'
)

args = parser.parse_args()

print(f'Skip parameter: {args.skip}')
print('Filenames:', args.filenames)

bags = []
for filename in args.filenames:
    bags.append(rosbag.Bag(filename))
info = bags[0].get_type_and_topic_info()

types = {}

rospy.init_node('bag_pub')
pubs = {}
for topic, value in info.topics.items():
    msg_type_str = value.msg_type
    import_msg_ns(msg_type_str)

    pubs[topic] = rospy.Publisher(topic, get_msg_type(msg_type_str), queue_size=1)
    continue

static_br = tf2_ros.StaticTransformBroadcaster()
br = tf.TransformBroadcaster()


t_0 = None
py_t_0 = time.time()
for topic, msg, t in bags[0].read_messages(topics='/tf_static'): # type: ignore
    print('BAG_PUB: SENDING TF_STATIC')
    t_0 = t
    for transform in msg.transforms:
        static_br.sendTransform(transform)
        break

speed = args.speed
t_0 += rospy.Duration(args.skip)
last_clock = None
clock_pub = rospy.Publisher('/clock', Clock, queue_size=1)
while not rospy.is_shutdown():
    print('BAG_PUB: START PUBLISHING')
    last_seg_img_stamp = None
    py_t_0 = time.time()
    # TODO: rework
    for bagg in bags:
        for topic, msg, t in bagg.read_messages(): # type: ignore
            if t - t_0 < rospy.Duration(0):
                pass
            else:
                while speed * rospy.Duration(time.time() - py_t_0) < t - t_0: # type: ignore
                    time.sleep(0.05)
            if last_clock != t:
                clock_pub.publish(Clock(clock=t))
                last_clock = t
            if topic == '/tf':
                for transform in msg.transforms:
                    br.sendTransformMessage(transform)
                continue

            pubs[topic].publish(msg)
            if rospy.is_shutdown():
                exit(0)
