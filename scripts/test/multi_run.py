import argparse
import os
import subprocess
import sys

from cross_view_localization import package_settings as settings

settings.initialize('cross_view_localization')
output_dir = settings.abs_path('output/eval_001/')

def parse_arguments():
    parser = argparse.ArgumentParser(description='Argument Parser')
    parser.add_argument('--launch', help='Launch file for runs')
    parser.add_argument('--run_name', help='Name of the recorded run (run_001/run_002/etc)', required=True)

    return parser.parse_args()

def main():
    args = parse_arguments()
    launch = args.launch
    run = args.run_name
    # simple_runs = [
    #     'warp_area_size:=10',
    #     'warp_area_size:=20',
    #     'warp_area_size:=30',
    #     'warp_area_size:=50',
    # ]
    # runs = (
    #     ['warp_area_size:=10'] * 5 +
    #     ['warp_area_size:=15'] * 5 +
    #     ['warp_area_size:=20'] * 5 +
    #     ['warp_area_size:=25'] * 5
    # )

    cur_folder = os.path.dirname(os.path.abspath(__file__))
    run_script = os.path.join(cur_folder, 'run.py')
    runs = (
        ['rviz:=true warp_area_size:=10'] * 1
    )
    for i in range(len(runs)):
        command = [
            'python3',
            run_script,
            f'--launch={launch}',
            f'--run_name={run}',
            f'--run_id={i}',
            f'--run_args={runs[i]}'
        ]
        subprocess.run(command)


if __name__ == "__main__":
    main()
