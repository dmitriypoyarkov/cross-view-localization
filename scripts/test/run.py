import argparse
import fcntl
import json
import os
import signal
import subprocess
import sys
import time

import genpy
import psutil
import rospy
from geometry_msgs.msg import Point, PoseWithCovarianceStamped
from rosgraph_msgs.msg import Clock
from std_msgs.msg import String
from tqdm import tqdm

from cross_view_localization import package_settings as settings

settings.initialize('cross_view_localization')
output_dir = settings.abs_path('output/eval_001/')
OUTPUT_FILE = os.path.join(output_dir, 'run_stdout.txt')
ERROR_FILE = os.path.join(output_dir, 'run_stderr.txt')
ERROR_FILE = sys.stderr



def ensure_file_and_folders_exist(file_path):
    """
    Ensure that the file exists and all folders on its path exist.

    Args:
    - file_path: The path to the file whose existence is to be ensured.
    """
    # Ensure all folders on the path exist
    folder_path = os.path.dirname(file_path)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    # Ensure the file exists
    if not os.path.exists(file_path):
        open(file_path, 'a').close()  # Create an empty file if it doesn't exist


def run_process(command, timeout, stdout, stderr, cwd='.'):
    instance = subprocess.Popen(command.split(' '), stdout=stdout, stderr=stderr, cwd=cwd)
    print("Process opened successfully.")
    return instance


def check_for_file(file_path):
    return os.path.exists(file_path)


def is_file_open(file_path):
    try:
        with open(file_path, 'a') as f:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
            fcntl.flock(f, fcntl.LOCK_UN)
        return False  # Able to obtain an exclusive lock, file is not open
    except IOError:
        return True


def get_clock() -> genpy.Time:
    msg = rospy.wait_for_message('/clock', Clock)
    if msg is None:
        raise RuntimeError('Clock message not received')
    return msg.clock


def list_bag_files(directory):
    all_files = os.listdir(directory)
    bag_files = [file for file in all_files if file.endswith('.bag')]
    bag_files_abs_paths = [os.path.abspath(os.path.join(directory, file)) for file in bag_files]
    bag_files_abs_paths.sort()
    return bag_files_abs_paths


def run(launch : str, launch_args : str, run_name : str, skip : float, duration : float, speed : float):
    ensure_file_and_folders_exist(OUTPUT_FILE)
    # output_file = open(OUTPUT_FILE, 'w')
    output_file = sys.stdout
    error_file = sys.stderr
    bag_paths = list_bag_files(settings.run_path(run_name, 'bag'))
    bag_paths_arg = ' '.join(bag_paths)
    bag_pub_file = settings.abs_path('scripts/test/bag_pub.py')
    bag_command = f'python3 {bag_pub_file} --skip={skip} --speed={speed} {bag_paths_arg}'
    bag_proc = run_process(bag_command, 100000, output_file, error_file)
    now_time = get_clock()
    begin_time = now_time
    skip_until_time = begin_time + rospy.Duration(secs=skip) # type: ignore
    play_until_time = skip_until_time + rospy.Duration(secs=duration) # type: ignore

    pbar = tqdm(
        total=skip_until_time.to_nsec()-begin_time.to_nsec(),
        bar_format='\033[34m{l_bar}{bar}|\033[00m',
        desc='Skipping some bag time',
        # colour='blue',
        ncols=100,
        ascii=True,
    )
    while now_time < skip_until_time and not rospy.is_shutdown():
        now_time = get_clock()
        time.sleep(0.1)
        # print(now_time.to_nsec() - pbar.n)
        pbar_update = min(now_time.to_nsec() - begin_time.to_nsec() - pbar.n, pbar.total - pbar.n)
        pbar.update(pbar_update)
    pbar.close()

    args = launch_args
    command = f'roslaunch cross_view_localization {launch} {args}'
    print('EXECUTING:\n', command)

    process_timeout = 600
    launch_proc = run_process(command, process_timeout, stdout=output_file, stderr=error_file)
    set_pos_pub = rospy.Publisher('/initialpose_img', PoseWithCovarianceStamped, queue_size=1)
    while set_pos_pub.get_num_connections() == 0:
        time.sleep(0.5)
    pose_with_cov = PoseWithCovarianceStamped()
    pose_with_cov.pose.pose.position = Point(x=317, y=117, z=0)
    set_pos_pub.publish(pose_with_cov)
    state_pub = rospy.Publisher('/system_state', String, queue_size=1)
    while state_pub.get_num_connections() == 0:
        time.sleep(0.5)
    state_pub.publish(String(data='RUNNING'))

    start_time = time.time()
    while True:
        # if check_for_file(file_to_wait_for) and not is_file_open(file_to_wait_for):
        #     print(f"The file {file_to_wait_for} exists.")
        #     break

        # Check if the timeout period has elapsed
        elapsed_time = time.time() - start_time
        ros_time = get_clock()
        if ros_time >= play_until_time or elapsed_time >= process_timeout:
            print(f"Timeout: {process_timeout}. elapsed: {elapsed_time}. ros_now: {ros_time}. bag_end: {play_until_time}.")
            bag_proc.send_signal(signal.SIGINT)
            # launch_proc.terminate()
            launch_proc.send_signal(signal.SIGINT)
            # os.system(f"pkill -TERM -P {os.getpid()}")  # Send SIGTERM to the process group
            break
        time.sleep(1)
    error_file.close()
    current_process = psutil.Process()
    for child_process in current_process.children(recursive=True):
        try:
            child_process.send_signal(signal.SIGINT)
            child_process.wait(timeout=2)
        except psutil.TimeoutExpired:
            child_process.kill()
        except psutil.NoSuchProcess:
            pass
    rospy.signal_shutdown(reason='BECAUSE I SAID')


def parse_arguments():
    parser = argparse.ArgumentParser(description="Argument Parser")
    parser.add_argument('--launch', help='Launch file for run')
    parser.add_argument("--run_id", type=int, help="Run ID")
    parser.add_argument("--run_name", help="Run name")
    parser.add_argument("--run_args", help="Launch arguments for run")

    return parser.parse_args()


def main():
    args = parse_arguments()
    proc_id = args.run_id
    launch = args.launch
    launch_args = args.run_args
    run_name = args.run_name

    roscore_command = 'roscore'
    subprocess.Popen(roscore_command)
    rospy.init_node(f'cvl_autorun_{proc_id}')
    rospy.set_param('use_sim_time', 'true')

    run_config_path = settings.run_path(run_name, 'run.json')
    with open(run_config_path, 'r') as f:
        run_config = json.load(f)
    skip = float(run_config['skip'])
    duration = float(run_config['duration'])
    speed = float(run_config['speed'])
    run(launch, launch_args, run_name, skip=skip, duration=duration, speed=speed)


if __name__ == "__main__":
    main()
