# Script that generates several configuration files for project.
# Must be executed after changing certain configuration files. See readme.md
# for more information.
import json
import os
import sys
from typing import List

import cross_view_localization.package_settings as settings


def generate_launch_for_camera(cam_name : str, cam_params : dict):
    rgb_topic = cam_params['rgb_topic']
    rgb_last_part = rgb_topic.split('/')[-1]
    rgb_path = rgb_topic[:-(len(rgb_last_part) + 1)]
    depth_topic = cam_params['depth_topic']
    depth_last_part = depth_topic.split('/')[-1]
    depth_path = rgb_topic[:-(len(depth_last_part) + 1)]
    launch = f'''
    <node pkg="nodelet" type="nodelet" name="{cam_name}_depth_image_convertion_nodelet"
        args="load depth_image_proc/convert_metric nodelet_manager">
        <param name="queue_size" value="100"/>
        <remap from="image_raw" to="{cam_params['depth_topic']}" />
        <remap from="image" to="{depth_path}/image_aligned" />
    </node>
    <node pkg="nodelet" type="nodelet" name="{cam_name}_depth_image_registration_nodelet"
        args="load depth_image_proc/register nodelet_manager">
        <param name="queue_size" value="1000"/>
        <remap from="rgb/camera_info" to="{cam_params['rgb_cam_info_topic']}" />
        <remap from="depth/camera_info" to="{cam_params['depth_cam_info_topic']}" />
        <remap from="depth/image_rect" to="{depth_path}/image_aligned" />

        <remap from="depth_registered/camera_info"
            to="{cam_params['topic_for_depth_reg_cam_info']}" />
        <remap from="depth_registered/image_rect"
            to="{cam_params['topic_for_depth_reg']}" />
    </node>
    '''
    return launch


def unite_cam_launches(launches : List[str]):
    launches_together = '\n'.join(launches)
    united_launch = f'''
    <launch>
        <node pkg="nodelet" type="nodelet" name="nodelet_manager"  args="manager" output="screen"/>
        {launches_together}
    </launch>
    '''
    return united_launch


def save_launch_file(robot : str, launch : str):
    launch_file_path = settings.robot_path(robot, 'launch/generated/cameras.launch')
    os.makedirs(os.path.dirname(launch_file_path), exist_ok=True)
    with open(launch_file_path, 'w') as f:
        f.write(launch)
    green(f'SUCCESS: Cameras launch for robot {robot} ({launch_file_path})')

def green(text : str): 
    print(f"\033[92m{text}\033[00m")


def red(text : str):
    print(f"\033[91m{text}\033[00m")


def main():
    if len(sys.argv) != 2:
        print('usage: python3 generate_config.py <robot_name>\n\n' +
              'This script generates additional configuration files. It should be executed after ' +
              'creation or editing certain configs. More info in readme.md.')
        exit(1)
    robot = sys.argv[1]
    print(f'Generating configuration files for robot {robot}.')

    robot_path = settings.robot_path(robot, '')
    print(f'Looking for cameras.json inside {robot_path}...')
    cameras_config_path = settings.robot_path(robot, 'config/cameras.json')
    try:
        with open(cameras_config_path, 'r') as f:
            cameras_config = json.load(f)
    except FileNotFoundError:
        red(f'Cameras config for robot {robot} not found on {cameras_config_path}')
        exit(1)
    green(f'cameras.json for robot {robot} is present.')
    
    cam_launches = []
    for cam_name, cam_params in cameras_config.items():
        cam_launch = generate_launch_for_camera(cam_name, cam_params)
        cam_launches.append(cam_launch)
        
    launch = unite_cam_launches(cam_launches)
    
    save_launch_file(robot, launch)
    
    green(f'Configuration files for robot {robot} successfully generated!')



if __name__ == '__main__':
    main()
