import json
import os
from typing import Union

import numpy as np

from cross_view_localization import package_settings as settings
from cross_view_localization.objects.eval_data import EvalData
from cross_view_localization.objects.pipeline_data import PipelineData


class Evaluation(object):
    def __init__(
            self, run : str,
            pixels_per_meter : float,
            run_args : str = '1'
        ):
        with open(settings.run_path(run, 'run.json')) as f:
            run_config = json.load(f)
        self._start_pos = run_config['start_pos']

        self._ref_coords_img_path = settings.run_path(run, 'ref_path_img.json')
        self._ref_coords_path = settings.run_path(run, 'ref_path.json')

        if os.path.exists(self._ref_coords_img_path):
            with open(self._ref_coords_img_path, 'r') as f:
                self._ref_coords_img = np.array(json.load(f))
        else:
            self._ref_coords_img = None
        save_basepath = settings.output_path(f'{run}_eval/eval')
        os.makedirs(save_basepath, exist_ok=True)
        self._save_path = self.unique_filename(save_basepath, 'json')
        save_path_basepath = settings.output_path(f'{run}_eval/path')
        os.makedirs(save_path_basepath, exist_ok=True)
        self._path_save_path = self.unique_filename(save_path_basepath, 'json')
        self._odom_save_path = settings.output_path(f'{run}_eval/odom.json')
        self._odom_steps_save_path = settings.output_path(f'{run}_eval/odom_steps.json')

        self._pixels_per_meter = pixels_per_meter
        self._ref_coords_img_dense = self.interpolate_coordinates(self._ref_coords_img, 300)
        self._run_args = run_args
        self._path = []
        self._odom_steps = []

    @property
    def eval_data(self):
        return EvalData(
            self._path,
            self._ref_coords_img_dense,
            np.array(self._odom_steps),
            self._start_pos
        )


    def unique_filename(self, base_path, extension):
        index = 0
        while True:
            path = f'{base_path}_{index}.{extension}'
            if not os.path.exists(path):
                return path
            index += 1


    def add_evaluation(self, pipeline : Union[PipelineData, None]):
        if pipeline is None:
            return
        if len(self._path) > 0 and pipeline.timestamp.to_nsec() == self._path[-1][-1]:
            return
        self._path.append([*pipeline.robot_pos, pipeline.timestamp.to_nsec()])
        self._odom_steps.append(pipeline.odom_step_img)


    def distance_between_coordinates(self, coord1, coord2):
        return np.sqrt((coord1[0] - coord2[0])**2 + (coord1[1] - coord2[1])**2)


    def eval_and_save(self):
        eval = self.calculate(np.array(self._path))
        with open(self._save_path, 'w') as f:
            json.dump(eval, f, indent=4)
        with open(self._path_save_path, 'w') as f:
            json.dump(self._path, f, indent=4)
        with open(self._odom_save_path, 'w') as f:
            json.dump(
                self.make_odom_path(
                    self._odom_steps, self._start_pos,
                ), f, indent=4
            )
        with open(self._odom_steps_save_path, 'w') as f:
            json.dump(
                np.array(self._odom_steps).tolist(),
                f, indent=4
            )


    def calculate(self, path : np.ndarray):
        if self._ref_coords_img_dense is None:
            return
        skip_amounts = np.array([0., 10., 20.])
        skip_amounts_nsec = (1_000_000_000 * skip_amounts).astype(np.uint64)
        eval_res = {}
        for skip in skip_amounts_nsec:
            index = self.skip_time(path, skip)
            cur_path = path[index:]
            err_timed = self.method_with_time(self._ref_coords_img_dense, cur_path)
            err_nottimed = self.method_with_coordinates(self._ref_coords_img_dense, cur_path)
            eval_res[f'err_img_timed_skip{skip}'] = round(err_timed, 2)
            eval_res[f'err_img_nottimed_skip{skip}'] = round(err_nottimed, 2)
            eval_res[f'err_timed_skip{skip}'] = round(err_timed / self._pixels_per_meter, 2)
            eval_res[f'err_nottimed_skip{skip}'] = round(err_nottimed / self._pixels_per_meter, 2)
            eval_res['run_args'] = self._run_args
        return eval_res


    def interpolate_coordinates(self, points : Union[np.ndarray, None], num_points : int):
        if points is None:
            return None
        timestamps = points[:, 2]

        min_time = np.min(timestamps)
        max_time = np.max(timestamps)

        min_time_diff = np.min(np.diff(timestamps))
        min_required_steps = int((max_time - min_time) / min_time_diff)

        num_points = max(min_required_steps, num_points)

        step_size = (max_time - min_time) / (num_points + 1)

        intermediate_timestamps = np.arange(min_time + step_size, max_time, step_size)

        interpolated_coordinates = []
        for timestamp in intermediate_timestamps:
            if timestamp < timestamps[0]:
                before_point = points[0]
                after_point = points[1]
            elif timestamp > timestamps[-1]:
                before_point = points[-2]
                after_point = points[-1]
            else:
                before_idx = np.where(timestamps < timestamp)[0][-1]
                after_idx = np.where(timestamps > timestamp)[0][0]
                before_point = points[before_idx]
                after_point = points[after_idx]

            t_diff = after_point[2] - before_point[2]
            weight_after = (timestamp - before_point[2]) / t_diff
            weight_before = 1 - weight_after
            coord = weight_before * before_point[:2] + weight_after * after_point[:2]
            coord = [coord[0], coord[1], timestamp]

            interpolated_coordinates.append(coord)

        return np.array(interpolated_coordinates)


    def skip_time(self, exp_path, skip_amount):
        min_exp_time = np.min(exp_path[:, 3]) + skip_amount
        return np.argmin(np.abs(exp_path[:, 3] - min_exp_time))


    def method_with_time(self, ref_path, exp_path):
        diff_sum = 0
        for exp_point in exp_path:
            closest_ref_point = self.closest_by_time(exp_point[3], ref_path)
            diff_sum += np.linalg.norm(exp_point[:2] - closest_ref_point[:2])

        return diff_sum / len(exp_path)


    def method_with_coordinates(self, ref_path, exp_path):
        diff_sum = 0
        for exp_point in exp_path:
            closest_ref_point = self.closest_by_distance(exp_point[:2], ref_path)
            diff_sum += np.linalg.norm(exp_point[:2] - closest_ref_point[:2])

        return diff_sum / len(exp_path)


    def closest_by_distance(self, point, ref_path):
        closest_ref_idx = np.argmin(np.linalg.norm(ref_path[:, :2] - point, axis=1))
        return ref_path[closest_ref_idx]


    def closest_by_time(self, stamp, ref_path):
        closest_ref_idx = np.argmin(np.abs(ref_path[:, 2] - stamp))
        return ref_path[closest_ref_idx]


    def make_odom_path(self, odom_steps, start_pos):
        x, y, phi = start_pos
        odom_path = [start_pos]
        for d, d_phi in odom_steps:
            phi -= d_phi
            x += d * np.sin(phi)
            y -= d * np.cos(phi)
            odom_path.append([x, y, phi])
        return np.array(odom_path).tolist()