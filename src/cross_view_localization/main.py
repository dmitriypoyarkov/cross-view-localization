#! /usr/bin/python3
import threading
import time
from typing import List

import numpy as np
import rospy

from cross_view_localization import package_settings as settings
from cross_view_localization.debug_stage.interactive_pygame import \
    InteractivePygame
from cross_view_localization.debug_stage.save_pipeline import PipelineSaveDebug
from cross_view_localization.decoration.deco import WaitAnimation
from cross_view_localization.evaluation.evaluation import Evaluation
from cross_view_localization.misc.compile_numba_functions import \
    compile_numba_functions
from cross_view_localization.objects.cam_manager import CamManager
from cross_view_localization.system.system import CVLSystem
from cross_view_localization.tools.input_manager import CVLInputManager
from cross_view_localization.visualization.particle_img_pub import \
    ParticleImgPub
from cross_view_localization.visualization.pipeline_img import \
    PipelineVisualizer
from cross_view_localization.visualization.pipeline_img_pub import \
    PipelineImgPub
from cross_view_localization.visualization.rviz_pub import RvizPublisher

compile_numba_functions()

NODE_NAME = 'cvl'

INPUT_SET_POSITION_TOPIC = '/set_position'
INPUT_SYSTEM_STATE_TOPIC = '/system_state'
INPUT_POINTCLOUD_TOPIC = '/input_pointcloud'
INPUT_DEPTH_RGB_IMAGE = '/input_rgb_depth_img'

OUTPUT_WARPED_IMG_TOPIC_DEBUG = '/warped_img'
OUTPUT_PROCESSED_IMG_TOPIC_DEBUG = '/processed_img'
OUTPUT_SEGMENTED_IMG_TOPIC_DEBUG = '/segmented_img'
OUTPUT_PARTICLE_IMG_TOPIC_DEBUG = '/particle_img'

INPUT_SAVE_PIPELINE_TOPIC_DEBUG = '/save_pipeline'

rospy.init_node(NODE_NAME, log_level=rospy.DEBUG)

PACKAGE_NAME : str = rospy.get_param('~package_name') # type: ignore

settings.initialize(
    package_name=PACKAGE_NAME,
)

ROBOT : str = rospy.get_param('~robot') # type: ignore
AREA : str = rospy.get_param('~area') # type: ignore
RUN : str = rospy.get_param('~run', default='') # type: ignore

SAT_IMG_PATH = settings.area_path(AREA, 'sat_img.bmp')
REF_COORDS_FILE_DEBUG = settings.run_path(RUN, 'ref_path.npy') # reference trajectory
# REF_ODOM_DEBUG = settings.area_path('input/ref_odom.npy')

OUTPUT_METRIC_MAP_SAVE_PATH = settings.abs_path('output/metric_map.jpg')
OUTPUT_METRIC_MAP_TOPIC_DEBUG = '/output_metric_map'

ODOM_ORIGIN_DEBUG = [np.array([350.0, 220.0]), np.array(0.8)]
try:
    REF_ODOM = np.load(settings.abs_path('input/odom_path_timed.npy'))
    REF_ODOM = np.array([[step[0], step[2], step[1]] for step in REF_ODOM])
except Exception:
    REF_ODOM = []

BASE_LINK_FRAME : str = rospy.get_param('~base_link_frame') # type: ignore
ODOM_FRAME : str = rospy.get_param('~odom_frame') # type: ignore
MAP_FRAME : str = rospy.get_param('~map_frame') # type: ignore
ODOM_FACTOR = float(rospy.get_param('~odom_factor')) # type: ignore
WARP_AREA_SIZE = float(rospy.get_param('~warp_area_size')) # type: ignore
USE_DELINEAR = bool(rospy.get_param('~use_delinear', default=True))
ENABLE_GUI = bool(rospy.get_param('~gui', default=False))
ENABLE_RVIZ_PUB = bool(rospy.get_param('~rviz_pub', default=True))
ENABLE_PARTICLE_IMG_PUB = bool(rospy.get_param('~particle_img_pub', default=True))
ENABLE_EVAL = bool(rospy.get_param('~enable_eval', default=True))


LABELS_PATH : str = settings.config_path(rospy.get_param('~labels')) # type: ignore
PALETTE_PATH : str = settings.config_path(rospy.get_param('~palette'))  # type: ignore

cams = CamManager.from_path(settings.robot_path(ROBOT, 'config/cameras.json'))
CAMERAS : List[str] = cams.names

INPUT_DEPTH_RGB_IMAGE_TOPICS = cams.rgb_topics
INPUT_CAMERA_INFO_TOPICS = cams.rgb_cam_info_topics
CAMERA_FRAMES = cams.frames
INPUT_FINAL_DEPTH_TOPICS = cams.final_depth_topics

seg_plugin = rospy.get_param('~seg_plugin')
prep_seg_required = seg_plugin == 'cross_view_localization.plugins.seg_plugin_prepared.SegPluginPrepared'
# try:
#     input_manager = CVLInputManager(
#         palette_path=PALETTE_PATH,
#         labels_path=LABELS_PATH,
#         sat_img_path=SAT_IMG_PATH,
#         ref_path_path=REF_COORDS_FILE_DEBUG,
#         run=RUN,
#     )
# except RuntimeError:
#     rospy.logerr('CVL: Input data incorrect, shutting down system')
#     exit(1)

system = CVLSystem(
    seg_plugin=rospy.get_param('~seg_plugin'), # type: ignore
    proc_plugin=rospy.get_param('~proc_plugin'), # type: ignore
    warp_plugin=rospy.get_param('~warp_plugin'), # type: ignore
    comp_plugin=rospy.get_param('~comp_plugin'), # type: ignore
    filter_plugin=rospy.get_param('~filter_plugin'), # type: ignore
    sat_img_path=SAT_IMG_PATH,
    labels_path=LABELS_PATH,
    palette_path=PALETTE_PATH,
    robot_frame=BASE_LINK_FRAME,
    odom_frame=ODOM_FRAME,
    map_frame=MAP_FRAME,
    input_depth_topics=INPUT_FINAL_DEPTH_TOPICS,
    cam_info_topics=INPUT_CAMERA_INFO_TOPICS,
    cam_frames=CAMERA_FRAMES,
    cam_names=CAMERAS,
    input_depth_rgb_topics=INPUT_DEPTH_RGB_IMAGE_TOPICS,
    odom_factor=ODOM_FACTOR,
    use_ai_depth=rospy.get_param('~use_ai_depth'), # type: ignore
)


def run_system_debug():
    system.wait_ready()
    # TODO: handle it properly
    anim = WaitAnimation('CVL: SYSTEM IS NOT IN RUNNING STATE. WAITING')
    while not rospy.is_shutdown():
        try:
            timestamp = system.wait_next_frame()
        except RuntimeError:
            anim.update()
            time.sleep(0.1)
            continue
        system.try_pipeline(timestamp)
    anim.stop()
    print('\x1b[?25h')


# def run_system():
#     system.wait_ready() # TODO: do something to prevent running without it
#     while not rospy.is_shutdown():
#         try:
#             timestamp = system.wait_next_frame()
#         except RuntimeError:
#             time.sleep(0.1)
#             continue
#         system.pipeline(timestamp)

system_thread = threading.Thread(target=run_system_debug)
system_thread.start()

# def update_pygame():
#     while not rospy.is_shutdown():
#         # TODO: move all get functions to system itself
#         interactive_pygame.update(
#             warped_img=system.filter.get_last_processed_img(),
#             odometry_step_img=system.filter.get_last_odom_step(),
#             particles=system.filter.get_particles(),
#             weights=system.filter.get_weights(),
#             state_estimate=system.filter.get_output_coord(),
#             timestamp=system.filter.get_last_timestamp()
#         )
#         time.sleep(0.4)


# if ENABLE_GUI:
#     rospy.logdebug('CVL: ENABLE_GUI: True. OPENING GUI...')
#     interactive_pygame = InteractivePygame(
#         system.comparer, system.initpos_and_run, ref_coords=ref_coords_debug
#     )
#     pygame_update_thread = threading.Thread(target=update_pygame)
#     pygame_update_thread.start()
#     interactive_pygame.run()

if ENABLE_RVIZ_PUB:
    rospy.logdebug('CVL: ENABLE_RVIZ_PUB: TRUE. INITIALIZING RVIZ PUB...')
    def publish_rviz():
        while not rospy.is_shutdown():
            rviz_pub.publish(
                # timestamp=rospy.Time.from_sec(time.time()),
                timestamp=system.filter.get_last_timestamp(),
                robot_position=system.output_coord_world,
                particles=system.particles_world,
            )
            time.sleep(0.3)
    rviz_pub = RvizPublisher(
        odom_frame=ODOM_FRAME,
        base_link_frame=BASE_LINK_FRAME,
        map_frame=MAP_FRAME,
        sat_img=system.sat_img,
    )
    rviz_pub_thread = threading.Thread(target=publish_rviz)
    rviz_pub_thread.start()


if ENABLE_PARTICLE_IMG_PUB:
    def publish_particle_img():
        while not rospy.is_shutdown():
            if system.last_pipeline is not None:
                if evaluation is not None:
                    eval_data = evaluation.eval_data
                else:
                    eval_data = None
                particle_pub.draw_and_publish(
                    system.last_pipeline, eval_data
                )

            time.sleep(0.5)
    rospy.logdebug('CVL: ENABLE_PARTICLE_IMG_PUB: TRUE. INITIALIZING PUB...')
    particle_pub = ParticleImgPub(
        sat_img=system.sat_img,
        pub_topic=OUTPUT_PARTICLE_IMG_TOPIC_DEBUG,
        odom_provider=system._odom_provider
    )
    particle_pub_thread = threading.Thread(target=publish_particle_img)
    particle_pub_thread.start()

evaluation = None
if ENABLE_EVAL:
    evaluation = Evaluation(
        run=RUN, pixels_per_meter=system.pixels_per_meter
    )
    def eval_loop():
        while not rospy.is_shutdown():
            evaluation.add_evaluation(system.last_pipeline)
            time.sleep(0.3)
        evaluation.eval_and_save()
    eval_thread = threading.Thread(target=eval_loop)
    eval_thread.start()

save_pipeline_sub = PipelineSaveDebug(
    system=system,
    output_folder=settings.output_path('saved_pipelines'),
)
save_pipeline_sub.subscribe(INPUT_SAVE_PIPELINE_TOPIC_DEBUG)

ENABLE_PIPELINE_IMG_PUB = True

if ENABLE_PIPELINE_IMG_PUB:
    pipe_pub = PipelineImgPub(SAT_IMG_PATH)
    def pipe_vis_loop():
        print('HIHIHIHIH')
        last_pipe_ts = None
        while not rospy.is_shutdown():
            new_pipe = system.last_pipeline
            if (new_pipe is not None and
                    new_pipe.timestamp != last_pipe_ts):
                pipe_pub.publish(new_pipe)
                last_pipe_ts = new_pipe.timestamp
            time.sleep(0.1)
    pipe_pub_thread = threading.Thread(target=pipe_vis_loop)
    pipe_pub_thread.start()