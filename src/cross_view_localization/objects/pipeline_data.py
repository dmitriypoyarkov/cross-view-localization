
from typing import Union

import numpy as np

from cross_view_localization.filtering.cluster import Clusters
from cross_view_localization.tools.warp_data import (LandImg, WarpData,
                                                     WarpDataSet)


class PipelineData(object):
    def __init__(
            self,
            warp_data : WarpDataSet,
            warped_img : LandImg,
            processed_img : LandImg,
            odom_step : np.ndarray,
            odom_step_img : np.ndarray,
            robot_pos : np.ndarray,
            particles : np.ndarray,
            weights : np.ndarray,
            clusters : Union[Clusters, None] = None,
        ):
        self._warp_data = warp_data
        self._timestamp = self._warp_data.timestamp
        self._warped_img = warped_img
        self._processed_img = processed_img
        self._odom_step = odom_step
        self._odom_step_img = odom_step_img
        self._robot_pos = robot_pos
        self._particles = particles
        self._weights = weights
        self._clusters = clusters

    @property
    def clusters(self):
        return self._clusters

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def warp_data(self):
        return self._warp_data

    @property
    def warped_img(self):
        return self._warped_img

    @property
    def odom_step(self):
        return self._odom_step

    @property
    def robot_pos(self):
        return self._robot_pos

    @property
    def particles(self):
        return self._particles

    @property
    def processed_img(self):
        return self._processed_img

    @property
    def odom_step_img(self):
        return self._odom_step_img

    @property
    def weights(self):
        return self._weights