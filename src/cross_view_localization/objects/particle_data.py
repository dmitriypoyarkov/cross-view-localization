from typing import Union

import numpy as np


class ParticleData(object):
    def __init__(
            self,
            particles: np.ndarray,
            weights: np.ndarray,
            state_estimate : Union[np.ndarray, None] = None,
        ):
        self._particles = particles
        self._weights = weights
        self._state_estimete = state_estimate

    @property
    def particles(self):
        return self._particles
    
    @property
    def weights(self):
        return self._weights
    
    @property
    def state_estimate(self):
        return self._state_estimete