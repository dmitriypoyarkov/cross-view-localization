import json
from typing import List


class StereoCamera(object):
    def __init__(
            self,
            name : str,
            frame : str,
            rgb_topic : str,
            depth_topic : str,
            depth_cam_info_topic : str,
            rgb_cam_info_topic : str,
            topic_for_depth_reg : str,
            topic_for_depth_reg_cam_info : str,
            final_depth_topic : str = '',
        ):
        self._name = name
        self._frame = frame
        self._depth_topic = depth_topic
        self._rgb_topic = rgb_topic
        self._depth_cam_info_topic = depth_cam_info_topic
        self._rgb_cam_info_topic = rgb_cam_info_topic
        self._depth_reg_topic = topic_for_depth_reg
        self._depth_reg_cam_info_topic = topic_for_depth_reg_cam_info

        if len(final_depth_topic) > 0:
            self._final_depth_topic = final_depth_topic
        else:
            self._final_depth_topic = self._depth_reg_topic

    @property
    def depth_reg_cam_info_topic(self):
        return self._depth_reg_cam_info_topic

    @property
    def depth_topic(self):
        return self._depth_topic

    @property
    def name(self):
        return self._name

    @property
    def frame(self):
        return self._frame

    @property
    def rgb_topic(self):
        return self._rgb_topic

    @property
    def depth_cam_info_topic(self):
        return self._depth_cam_info_topic

    @property
    def rgb_cam_info_topic(self):
        return self._rgb_cam_info_topic

    @property
    def depth_reg_topic(self):
        return self._depth_reg_topic

    @property
    def final_depth_topic(self):
        return self._final_depth_topic


class CamManager(object):
    def __init__(self, cams : List[StereoCamera]):
        cams_sorted = sorted(cams, key=lambda cam: cam.name)
        self._cams = cams_sorted

    @property
    def names(self):
        return [cam.name for cam in self._cams]

    @classmethod
    def from_path(cls, path : str):
        with open(path, 'r') as f:
            data = json.load(f)
        cams = []
        for key, value in data.items():
            cam = StereoCamera(name=key, **(value))
            cams.append(cam)

        return cls(cams)

    @property
    def rgb_topics(self):
        return [cam.rgb_topic for cam in self._cams]

    @property
    def depth_topics(self):
        return [cam.depth_topic for cam in self._cams]

    @property
    def cams(self):
        return self._cams

    @property
    def frames(self):
        return [cam.frame for cam in self._cams]

    @property
    def depth_cam_info_topics(self):
        return [cam.depth_cam_info_topic for cam in self._cams]

    @property
    def rgb_cam_info_topics(self):
        return [cam.rgb_cam_info_topic for cam in self._cams]

    @property
    def num_cams(self):
        return len(self._cams)

    @property
    def depth_reg_topics(self):
        return [cam.depth_reg_topic for cam in self._cams]

    @property
    def final_depth_topics(self):
        return [cam.final_depth_topic for cam in self._cams]