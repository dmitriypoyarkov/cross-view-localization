import json
from typing import List

import numpy as np


class PaletteUnit(object):
    '''Defines the correspondance between label name and its color. Also sets its priority.'''
    def __init__(self, label_name : str, color : List[int], priority : int):
        self._label_name = label_name
        if len(color) != 3:
            raise ValueError(f'Color length is not 3! It is {len(color)}')
        self._color = np.array(color)
        self._priority = priority

    @property
    def label_name(self):
        return self._label_name

    @property
    def color(self):
        return self._color

    @property
    def priority(self):
        return self._priority


class Palette(object):
    def __init__(self, palette : List[PaletteUnit]):
        self._palette = palette


    @classmethod
    def from_json(cls, palette_json : dict):
        palette = []
        for unit_json in palette_json.items():
            palette.append(
                PaletteUnit(
                    unit_json[0],
                    unit_json[1]['color'],
                    unit_json[1]['priority']
                )
            )
        return cls(palette)


    def get_unit(self, label_name : str):
        for unit in self._palette:
            if unit.label_name == label_name:
                return unit
        raise RuntimeError(f'No unit with label {label_name}!')


    def get_sorted_colors(self):
        sorted_units = sorted(self._palette, key=lambda unit: unit.priority)
        sorted_colors = np.array([unit.color for unit in sorted_units])
        return sorted_colors


class LabelManager(object):
    def __init__(self, labels : List[str], palette : Palette):
        self._labels = labels
        self._palette = palette
        self._colors_sorted = palette.get_sorted_colors()
        self._label_colors = self.make_label_colors(labels, palette)


    @classmethod
    def from_files(cls, labels_file : str, palette_file : str):
        with open(labels_file, 'r') as f:
            labels = json.load(f)
        with open(palette_file, 'r') as f:
            palette_json = json.load(f)
        palette = Palette.from_json(palette_json)
        return cls(labels, palette)

    @property
    def labels(self):
        '''
        List of labels (strings). Defined for a specific segmentation module with respect to the palette.
        Neural networks give an output with certain numbers for each label: 0, 1, 2 and so on.
        If 0 is for grass, 1 is for road, 2 is for building, then labels may look like this::

            ["vegetation", "road", "building"].

        If 0 for grass, 1 for road, 2 for building, 3 for trees, 4 for sidewalks, it may look like this::

            ["vegetation", "road", "building", "vegetation", "vegetation"].

        Depends on how it suits the comparison module.
        Usually, neural networks have a lot of labels that can be grouped in a larger label.
        '''
        return self._labels


    @property
    def palette(self):
        '''
        List of PaletteUnit.
        It's contents are set for the system and do not depend on the segmentation module.
        Colors can be changed freely, but keys can not: comparison module or other modules can depend on them.
        Labels should be defined with respect to the current palette.
        '''
        return self._palette

    @property
    def colors_sorted(self):
        '''
        An array with colors from palette sorted by priority.
        Example::

            [
                [255, 0, 0],
                [0, 255, 0],
                [0, 0, 255],
            ]
        '''
        return self._colors_sorted

    @property
    def label_colors(self):
        '''
        An array with colors that correspond to the certain neural network.
        It is labels variable but string labels are replaced with actual colors from palette.
        Example::

            [
                [255, 0, 0],
                [0, 255, 0],
                [0, 0, 255],
            ]
        '''
        return self._label_colors


    def make_label_colors(self, labels : List[str], palette : Palette):
        label_colors = np.zeros((len(labels), 3))
        for i, label in enumerate(labels):
            label_colors[i] = palette.get_unit(label).color
        return np.array(label_colors)