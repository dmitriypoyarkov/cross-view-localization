
import cv2 as cv
import numpy as np
import rospy
from cv2.typing import MatLike

from cross_view_localization.tools.warp_data import LandImg


class FilterInput(object):
    def __init__(
            self, timestamp : rospy.Time,
            odom_step : np.ndarray, proc_img : LandImg,
        ):
        self._timestamp = timestamp
        self._odom_step = odom_step
        self._proc_img = proc_img


    @property
    def timestamp(self):
        return self._timestamp

    @property
    def odom_step(self):
        return self._odom_step

    @odom_step.setter
    def odom_step(self, value : np.ndarray):
        self._odom_step = value

    @property
    def proc_img(self):
        return self._proc_img
