import numpy as np
import rospy

from cross_view_localization.tools.warp_data import WarpDataSet


class PipelineInput(object):
    def __init__(
            self, timestamp : rospy.Time,
            odom_step_img : np.ndarray, warp_dataset : WarpDataSet,
            odom_step : np.ndarray
        ):
        self._timestamp = timestamp
        self._odom_step = odom_step
        self._odom_step_img = odom_step_img
        self._warp_dataset = warp_dataset

    @property
    def odom_step_img(self):
        return self._odom_step_img

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def odom_step(self):
        return self._odom_step

    @property
    def warp_dataset(self):
        return self._warp_dataset
