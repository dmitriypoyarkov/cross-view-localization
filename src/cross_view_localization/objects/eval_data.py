import json
from typing import Union

import numpy as np
import rospy


class EvalData(object):
    def __init__(self, path, ref_path : Union[np.ndarray, None], odom_steps : np.ndarray, start_pos):
        if ref_path is not None and len(ref_path) == 0:
            raise Exception('Ref path is empty. This is not allowed')
        self._path = path
        self._ref_path = ref_path
        self._odom_steps = odom_steps
        self._start_pos = start_pos

    @property
    def start_pos(self):
        return self._start_pos

    @property
    def path(self):
        return self._path

    @property
    def ref_path(self):
        return self._ref_path

    @property
    def odom_steps(self):
        return self._odom_steps

    def get_ref_pose(self, ts_nsec : int):
        if self._ref_path is None:
            return None
        index = np.argmin(np.abs(self._ref_path[:, -1] - ts_nsec))
        return self._ref_path[index]