import json
import os
import pathlib

import cv2 as cv
from cv2.typing import MatLike


class SatelliteImage(object):
    def __init__(self, img : MatLike,
                 width : int, height : int,
                 center_x : float, center_y : float):
        self._img = img
        self._width = width
        self._height = height
        self._center_x = center_x
        self._center_y = center_y

        self._img_center_y = self._img.shape[0] / 2
        self._img_center_x = self._img.shape[1] / 2
        self._meters_per_pixel = width / self._img.shape[1]
        self._pixels_per_meter = 1 / self._meters_per_pixel

    @property
    def meters_per_pixel(self):
        return self._meters_per_pixel
    
    @property
    def pixels_per_meter(self):
        return self._pixels_per_meter

    @property
    def img(self):
        return self._img

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def center_x(self):
        return self._center_x

    @property
    def center_y(self):
        return self._center_y

    @property
    def img_center_x(self):
        return self._img_center_x

    @property
    def img_center_y(self):
        return self._img_center_y

    # requires meta file alongside
    @classmethod
    def from_file(cls, file_path : str):
        try:
            img = cv.cvtColor(cv.imread(file_path), cv.COLOR_BGR2RGB)
        except cv.error as e:
            print(f'Satellite image loading error. Path: {file_path}, Error: {str(e)}')
            raise e
        meta_file_path = f'{file_path}.meta'
        try:
            with open(meta_file_path, 'r') as f:
                meta = json.load(f)
        except Exception as e:
            print(f'Image meta file loading error. Path: {meta_file_path}, Error: {str(e)}')
            raise e
        return cls(
            img=img,
            width=meta['width'],
            height=meta['height'],
            center_x=meta['center_x'],
            center_y=meta['center_y'],
        )