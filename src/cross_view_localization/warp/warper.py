import numpy as np
from cv_bridge import CvBridge
from numba import njit
from numba.pycc import CC
from tf.transformations import quaternion_matrix

from cross_view_localization.tools.warp_data import LandImg, WarpDataSet

cc = CC(__name__.split('.')[-1])
cc.verbose = True


X_N = 0
Y_N = 1
Z_N = 2

KERNEL = np.ones((3, 3), np.uint8)

definition = 'void(u1[:,:,:], f4[:,:], f4[:], f4[:, :], f4[:,:], f4[:], f4, u1[:,:,:,:], u1[:, :])'
@njit
@cc.export('warp_njit_depth_multi', definition)
def warp_njit_depth_multi(segmented_img,
        depth_img, trans, q, cam_K, visible_area_shift,
        pixels_per_meter, color_maps, labels):
    for v in range(depth_img.shape[0]):
        for u in range(depth_img.shape[1]):
            depth = depth_img[v, u]
            if np.isnan(depth) or depth == 0.:
                continue
            pixel_cam = pixel_to_world_njit(v, u, depth, cam_K)
            transformation_matrix = q ###########
            transformation_matrix[:3, 3] = trans
            pixel_cam_homog = np.array([pixel_cam[0], pixel_cam[1], pixel_cam[2], 1.0])
            pixel_world = np.dot(transformation_matrix, pixel_cam_homog)
            # pixel_world = pixel_cam
            color = segmented_img[v, u]
            proj_vu = ((
                np.array(
                    [pixel_world[0], pixel_world[1]]
                ) + visible_area_shift
            ) * pixels_per_meter).astype(np.int64)
            shap = color_maps[0].shape
            out_of_bounds = (
                proj_vu[0] >= shap[0] or
                proj_vu[1] >= shap[1] or
                proj_vu[0] < 0 or
                proj_vu[1] < 0
            )
            if out_of_bounds:
                continue
            for k in range(len(labels)):
                if np.all(labels[k] == color):
                    color_maps[k, shap[0] - proj_vu[0] - 1, shap[1] - proj_vu[1]] = 255
    for k in range(len(labels)):
        shap = color_maps[k].shape
        color_maps[k,
            shap[0] // 2 - 1:shap[0] // 2 + 1,
            shap[1] // 2 - 1:shap[1] // 2 + 1
        ] = 0

definition = 'void(u1, u1, f4, f4[:, :])'
@njit
@cc.export('pixel_to_world_njit', definition)
def pixel_to_world_njit(v, u, depth, cam_K):
    fx = cam_K[0, 0]
    fy = cam_K[1, 1]
    cx = cam_K[0, 2]
    cy = cam_K[1, 2]
    ray_dir = np.array([
        1,
        - (u - cx) / fx,
        - (v - cy) / fy,
    ])

    ray_dir *= depth / np.linalg.norm(ray_dir)

    # Return the normalized ray direction vector
    return ray_dir


# definition = 'void(u1[:,:,:], u1[:,:,:], f4[:], f4, f4, f4[:], f4, u1[:,:,:,:])'
# @njit
# @cc.export('warp_njit_flat_multi', definition)
# def warp_njit_flat_multi(out_warped_img, segmented_img,
#         pointcloud_flat, pointcloud_height,
#         pointcloud_width, visible_area_shift,
#         pixels_per_meter, color_maps):
#     i = 0
#     v = 0
#     u = 0
#     while v < pointcloud_height:
#         u = 0
#         while u < pointcloud_width:
#             if not np.isnan(pointcloud_flat[i]):
#                 color = segmented_img[v, u]
#                 proj_vu = ((
#                     np.array(
#                         [pointcloud_flat[i + Y_N], pointcloud_flat[i + X_N]]
#                     ) + visible_area_shift
#                 ) * pixels_per_meter).astype(np.int64)
#                 for k in range(len(LABELS)):
#                     if np.all(LABELS[k] == color):
#                         color_maps[k, out_warped_img.shape[0] - proj_vu[0] - 1, proj_vu[1]] = 255
#                     color_maps[k,
#                         out_warped_img.shape[0] // 2 - 1:out_warped_img.shape[0] // 2 + 1,
#                         out_warped_img.shape[1] // 2 - 1:out_warped_img.shape[1] // 2 + 1
#                     ] = 0
#             i += 4
#             u += 1
#         v += 1


class Warper(object):
    def __init__(self, colors_sorted : np.ndarray,
            projection_shape = np.array([64, 64]),
            warp_area_size : float = 10.
        ):
        self._colors_sorted = colors_sorted
        self._projection_shape = projection_shape
        self._points_density_factor = 0.08

        self.set_warp_area_size(warp_area_size)

        self._bridge = CvBridge()

        self._colormaps_cache = None


    def set_warp_area_size(self, size : float):
        self._visible_area_shape = np.array([1, -1, 1, -1]) * size
        self._visible_area_size = np.array(
            [self._visible_area_shape[0] - self._visible_area_shape[1],
            self._visible_area_shape[2] - self._visible_area_shape[3]]
        )
        self._pixels_per_meter = self._projection_shape / self._visible_area_size
        self._meters_per_pixel = 1 / self._pixels_per_meter


    def warp_multi(self, warp_datas : WarpDataSet):
        max_distance_horizontal = max(
            self._visible_area_shape[0],
            -self._visible_area_shape[1]
        )
        max_distance_vertical = max(
            self._visible_area_shape[2],
            -self._visible_area_shape[3]
        )

        pixels_per_meter = np.array([
            (self._projection_shape[0] - 1) / (2 * max_distance_vertical),
            (self._projection_shape[1] - 1) / (2 * max_distance_horizontal)
        ])
        visible_area_shift = np.array([
            max_distance_vertical,
            max_distance_horizontal
        ])
        out_warped_img = np.full((
            self._projection_shape[0],
            self._projection_shape[1], 3),
            [0, 0, 0],
            dtype=np.uint8
        )

        self._colormaps_cache = np.zeros(
            (
                len(self._colors_sorted),
                out_warped_img.shape[0],
                out_warped_img.shape[1]
            ), dtype=np.uint8
        )
        for warp_data in warp_datas.warp_datas:
            trans_matrix = quaternion_matrix(warp_data.rot)
            depth_img = np.array(warp_data.depth, dtype=np.float32)
            warp_njit_depth_multi(
                warp_data.segmented_img, depth_img,
                warp_data.trans, trans_matrix, warp_data.cam_K, visible_area_shift,
                pixels_per_meter, self._colormaps_cache, self._colors_sorted
            )

        # out_warped_img = cv.rotate(out_warped_img, cv.ROTATE_90_COUNTERCLOCKWISE)
        # print('WARP MULTI END')
        land_img = LandImg(
            pixels_per_meter[0],
            self._colors_sorted,
            masks=self._colormaps_cache
        )
        return land_img


