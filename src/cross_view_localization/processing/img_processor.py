import copy
import os
import sys
import time
from typing import Dict, List

import cv2 as cv
import numpy as np
from cv2.typing import MatLike

from cross_view_localization.objects.labels import Palette, PaletteUnit
from cross_view_localization.tools.warp_data import LandImg

BLACK = np.array([0,0,0], dtype=np.uint8)


class LandImgProcessor(object):
    def __init__(self, palette : Palette):
        # self._required_radius = required_radius

        self._kernel_size = 5

        self._palette = palette
        self._building_unit = self._palette.get_unit('building')
        self._units_to_extend = [self._building_unit]


    def process(self, warped_img : LandImg):
        processed_img = copy.deepcopy(warped_img)
        for color in processed_img.colors_sorted:
            mask = processed_img.get_mask_by_color(color)
            smooth_mask = self.smoothen_mask(mask)
            processed_img.set_mask(color, smooth_mask)
        for unit in self._units_to_extend:
            # print(f'\nEXTENDING {unit.color}')
            label_mask = processed_img.get_mask_by_color(unit.color)
            # print(f'\nMASK {label_mask}')
            init_mask = copy.deepcopy(label_mask)
            new_mask = self.extend_mask(label_mask)
            # print(f'\nEXETENDED {np.all(init_mask == new_mask)}')
            processed_img.set_mask(unit.color, new_mask)
        processed_img.combine_masks()

        return processed_img


    def process_mask(self, mask : MatLike):
        self.smoothen_mask(mask)


    # def extend_labels(self, land_img : LandImg, labels : List[PaletteUnit]):
    #     for label in labels:
    #         label_mask = land_img.get_mask_by_color(label.color)
    #         new_mask = self.extend_mask(label_mask)
    #         land_img.set_mask(label.color, new_mask)
    #     land_img.combine_masks()


    def extend_mask(self, mask : MatLike):
        angles_num = 80
        angles, step = np.linspace(0., 2 * np.pi, angles_num, retstep=True)
        extension_img = np.zeros_like(mask)
        for angle in angles:
            sector_start = angle - step / 2
            sector_end = angle + step / 2
            self.walk_sector(mask, extension_img, sector_start, sector_end)
        cv.bitwise_or(mask, extension_img, mask)
        return mask


    # angle clockwise, zero is upward direction
    def step_from_center(
            self, center : np.ndarray,
            distance : float, angle : float
        ):
        point = np.array([
            center[0] - distance * np.cos(angle),
            center[1] + distance * np.sin(angle),
        ])
        return point


    def polygon_not_empty(self, image, polygon):
        """
        Check if there is at least one white pixel inside the given polygon.

        Args:
        - image (numpy.ndarray): Binary image.
        - polygon (numpy.ndarray): Array containing the vertices of the polygon.
                The shape of the array should be (N, 1, 2), where N is the number of vertices,
                each vertex represented as a coordinate (x, y).

        Returns:
        - bool: True if there is at least one white pixel inside the polygon, otherwise False.
        """
        # fixed_polygon = polygon.reshape((-1, 1, 2))
        x, y, w, h = cv.boundingRect(polygon)

        # Iterate through each pixel within the bounding box
        for i in range(x, x + w):
            for j in range(y, y + h):
                # Check if the pixel lies inside the polygon and is white
                if cv.pointPolygonTest(polygon, (i, j), False) == 1 and image[j, i] == 255:
                    return True

        return False

    # TODO: possible optimization: collect all polygons
    # inside of a numpy array and then draw them with a single
    # call of fillPoly()
    def walk_sector(
            self, img : MatLike, out_img : MatLike,
            sector_start : float, sector_end : float,
        ):
        step = 3 # pixels
        center = np.array([img.shape[0] // 2, img.shape[1] // 2])
        distance = step
        extension_size = 8 # pixels
        required_draw_steps = int(extension_size / step)
        draw_steps = 0

        while True:
            distance_start = distance
            distance_end = distance + step
            corner0 = self.step_from_center(
                center, distance_start, sector_start
            )
            corner1 = self.step_from_center(
                center, distance_end, sector_start
            )
            corner2 = self.step_from_center(
                center, distance_end, sector_end
            )
            corner3 = self.step_from_center(
                center, distance_start, sector_end
            )
            pts = np.array(
                [[corner0, corner1, corner2, corner3]],
                dtype=np.int32
            )
            for pt in pts[0]:
                if (
                    pt[0] >= img.shape[0] or pt[1] >= img.shape[1] or
                    pt[0] < 0 or pt[1] < 0
                ):
                    return

            if self.polygon_not_empty(img, pts) and draw_steps == 0:
                draw_steps = required_draw_steps

            if draw_steps > 0:
                img = cv.fillPoly(out_img, pts, 255) # type: ignore
                draw_steps -= 1
                if draw_steps == 0:
                    return
            distance += step


    def radiuses_for_sectors(self, sectors, colormap):
        angles_and_radiuses = []
        center_x = colormap.shape[1] // 2
        center_y = colormap.shape[0] // 2
        for y in range(colormap.shape[0]):
            for x in range(colormap.shape[1]):
                pixel = colormap[y, x]
                if pixel != 255:
                    continue
                angles_and_radiuses.append((
                    np.arctan2(x - center_x, -(y - center_y)),
                    ((x - center_x)**2 + (y - center_y)**2)**0.5
                ))
        sector_min_radiuses = []
        for sector in sectors:
            radiuses_from_sector = [
                pixel[1] for pixel in angles_and_radiuses
                if pixel[0] >= sector[0] and pixel[0] <= sector[1]
            ]
            if len(radiuses_from_sector) == 0:
                sector_min_radiuses.append(-1)
                continue
            sector_min_radiuses.append(int(min(radiuses_from_sector)))
        return sector_min_radiuses


    def smoothen_mask_old(self, mask : MatLike):
        kernel = np.ones((self._kernel_size, self._kernel_size), np.uint8)
        iterations = 1
        for _ in range(iterations):
            mask = cv.dilate(mask, kernel, iterations=1)
            mask = cv.erode(mask, kernel, iterations=1)
        return mask


    def smoothen_mask(self, mask : MatLike):
        # mask[mask != 0] = 255
        blurred_image = cv.GaussianBlur(mask, (5, 5), 50)

        _, thresh_img = cv.threshold(blurred_image, 90, 255, cv.THRESH_BINARY)
        return thresh_img


    # def draw_sectors_for_color(self, sectors, width, height, color, radiuses):
    #     size = int((height**2 + width**2)**0.5 + 2)
    #     img = np.zeros((height, width, 3), dtype=np.uint8)
    #     bigger_img = np.zeros((size, size, 3), dtype=np.uint8)
    #     center_x, center_y = size // 2, size // 2
    #     max_radius = size - 1
    #     thickness = 10

    #     for i in range(len(sectors)):
    #         if radiuses[i] == -1:
    #             continue
    #         shape = np.zeros(bigger_img.shape[:2])
    #         start_angle = np.rad2deg(sectors[i][0])
    #         end_angle = np.rad2deg(sectors[i][1])

    #         radius = min(max_radius, radiuses[i] + thickness)
    #         cv.ellipse(
    #             img=shape, center=(center_x, center_y),
    #             axes=(radius, radius), angle=-90, startAngle=start_angle,
    #             endAngle=end_angle, color=255, thickness=-1
    #         )
    #         cv.circle(shape, (center_x, center_y), radiuses[i] - 1, 0, -1)
    #         y0 = center_y - height//2
    #         y1 = img.shape[0] + y0
    #         x0 = center_x - width//2
    #         x1 = img.shape[1] + x0
    #         img[
    #             shape[y0:y1,x0:x1]==255
    #         ] = color
    #     return img


    def overlap_imgs(self, img0, img1, mask):
        result = cv.bitwise_and(img0, img0, mask=~mask)
        result += cv.bitwise_and(img1, img1, mask=mask)
        return result

    def nearest_angle(self, target_angle, pixels_per_angle_r : dict):
        closest_angle = None
        min_difference = float('inf')

        for angle in pixels_per_angle_r.keys():
            # Calculate the absolute difference between the target angle and the current angle
            difference = abs(target_angle - angle)

            # Check if this angle is closer than the previous closest angle
            if difference < min_difference:
                min_difference = difference
                closest_angle = angle

        return closest_angle


    def nearest_radius(self, vector_x, vector_y):
        return np.round(
            np.linalg.norm([vector_x, vector_y])
        ).astype(np.int16)


    def angles_for_radius(self, radius, center_x, center_y):
        angles_set = set()
        for angle in np.linspace(0, 2 * np.pi, radius * 8):
            x = int(center_x + radius * np.cos(angle))
            y = int(center_y + radius * np.sin(angle))

            vector_x = x - center_x
            vector_y = y - center_y

            discrete_angle = round(self.pixel_angle(vector_x, vector_y), 4)

            angles_set.add(discrete_angle)
        return angles_set


    def pixel_angle(self, vector_x, vector_y):
        return np.arctan2(vector_y, vector_x)



# if __name__ == '__main__':
#     path = '/home/fc/catkin_ws/src/cross_view_localization/output/saved_pipelines/pipeline_5/test_warp.png'
#     dir = os.path.dirname(path)
#     mask = cv.imread(
#         path
#     )
#     processor = LandImgProcessor()

#     start = time.time()
#     result = processor.smoothen_mask(mask)
#     result = processor.extend_labels(result)
#     print(f'time: {time.time() - start}')

#     cv.imwrite(os.path.join(dir, 'processed.bmp'), result)