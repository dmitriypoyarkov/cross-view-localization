import os
from typing import Callable

from cross_view_localization.tools.package_path import PackagePath

DEFAULT_PACKAGE_NAME = 'cross_view_localization'
initialized = False

def initialize(package_name):
    global pack, initialized
    pack = PackagePath(package_name)
    initialized = True


def require_initialized(fun : Callable):
    def wrapper(*args):
        if not initialized:
            raise RuntimeError("Package settings not initialized")
        return fun(*args)
    return wrapper

@require_initialized
def abs_path(path : str):
    return pack.abs_path(path)

@require_initialized
def input_path(path : str):
    return pack.abs_path(os.path.join('input/', path))

@require_initialized
def output_path(path : str):
    return pack.abs_path(os.path.join('output/', path))

@require_initialized
def config_path(path : str):
    return pack.abs_path(os.path.join('config/', path))

@require_initialized
def launch_path(path : str):
    return pack.abs_path(os.path.join('launch/', path))

@require_initialized
def robot_path(robot : str, path : str):
    robot_rel_path = os.path.join('robots', robot)
    rel_path = os.path.join(robot_rel_path, path)

    return pack.abs_path(rel_path)

@require_initialized
def area_path(area : str, path : str):
    area_rel_path = os.path.join('areas', area)
    rel_path = os.path.join(area_rel_path, path)

    return pack.abs_path(rel_path)

@require_initialized
def run_path(run : str, path : str):
    if run == '':
        return ''
    run_rel_path = os.path.join('recorded_runs', run)
    rel_path = os.path.join(run_rel_path, path)

    return pack.abs_path(rel_path)


initialize(DEFAULT_PACKAGE_NAME)