from typing import Dict, Tuple

import cv2 as cv
import numpy as np
from numba import njit
from numba.pycc import CC

cc = CC(__name__.split('.')[-1])
cc.verbose = True

# UNDEFINED = np.array([10, 10, 10], dtype=np.uint8)
# ROTATION_ARTIFACT = np.array([0, 0, 0], dtype=np.uint8)
# EXCLUDED_COLORS = [UNDEFINED, ROTATION_ARTIFACT]

# GRASS = np.array([0, 255, 0], dtype=np.uint8)
# ROAD = np.array((128,  64, 128), dtype=np.uint8)

# SKY = np.array((128, 128, 128), dtype=np.uint8)
# BUILDING = np.array((128,   0,   0), dtype=np.uint8)

# # GRASS = np.array([0, 255, 0], dtype=np.uint8)
# # ROAD = np.array((105, 105, 105), dtype=np.uint8)

# # SKY = np.array((128, 128, 128), dtype=np.uint8)
# # BUILDING = np.array((0, 0, 255), dtype=np.uint8)

# POLE = np.array((192, 192, 128), dtype=np.uint8)
# PAVEMENT = np.array(( 60,  40, 222), dtype=np.uint8)
# TREE = np.array((128, 128,   0), dtype=np.uint8)
# SIGN_SYMBOL = np.array((192, 128, 128), dtype=np.uint8)
# FENCE = np.array(( 64,  64, 128), dtype=np.uint8)
# CAR = np.array(( 64,   0, 128), dtype=np.uint8)
# PEDESTRIAN = np.array(( 64,  64,   0), dtype=np.uint8)
# BYCICLIST = np.array((  0, 128, 192), dtype=np.uint8)


# MATCH_SCORES = [
#     # (GRASS, GRASS_PROBABLY, 0.5),
#     # (ROAD, ROAD_PROBABLY, 0.5),
#     # (BUILDING, BUILDING_PROBABLY, 0.8)
# ]

# LABELS = np.array([
#     GRASS,
#     ROAD,
#     BUILDING,
#     SKY,
#     POLE,
#     ROAD,
#     PAVEMENT,
#     TREE,
#     SIGN_SYMBOL,
#     FENCE,
#     CAR,
#     PEDESTRIAN,
#     BYCICLIST,
# ])

@njit
@cc.export('compare_pixels_advanced_njit', 'f8(u1[:], u1[:])')
def compare_pixels_advanced_njit(sample_color : np.ndarray, map_color : np.ndarray):
    pair = (sample_color, map_color)
    if pair[0] == pair[1]:
        return 1.
    # for score in MATCH_SCORES:
    #     if score[:2] == pair:
    #         return score[2]
    return 0.


@njit
@cc.export('compare_pixels_njit', 'f8(u1[:], u1[:])')
def compare_pixels_njit(p1 : np.ndarray, p2 : np.ndarray):
    # if np.all(p1 == UNDEFINED) or np.all(p2 == UNDEFINED):
    #     return 0.5
    if np.all(p1 == p2):
        return 1.
    return 0.


@njit
@cc.export('compare_img_njit', 'f8(u1[:, :, :], u1[:, :, :], u2)')
def compare_img_njit(img1 : cv.Mat, img2 : cv.Mat, valued_pixels_count : int):
    similarity_points = 0.
    if valued_pixels_count == 0:
        return 0.
    for y in range(img1.shape[0]):
        for x in range(img1.shape[1]):
            similarity_points += compare_pixels_njit(img1[y, x], img2[y, x])

    not_matched_points = valued_pixels_count - similarity_points
    not_matched_percent = not_matched_points / valued_pixels_count
    # normalized_nonlinear = np.exp(-0.5 * (not_matched_points/sigma)**2)
    normalized_nonlinear = 1 / (1 + ((-not_matched_percent + 1) / 0.35) ** (2 * 1.7))
    return normalized_nonlinear


@njit
@cc.export('matched_pixels_img_njit', 'f8(u1[:, :, :], u1[:, :, :])')
def matched_pixels_img_njit(img1 : cv.Mat, img2 : cv.Mat):
    matched_pixels = np.zeros_like(img1)
    for y in range(img1.shape[0]):
        for x in range(img1.shape[1]):
            similarity = int(compare_pixels_njit(img1[y, x], img2[y, x]))
            if similarity == 1:
                matched_pixels[y, x] = [255, 255, 255]
            elif similarity == 0:
                matched_pixels[y, x] = [0, 0, 0]
    return matched_pixels


@njit
@cc.export('compare_img_label_aware', 'f8(u1[:, :, :], u1[:, :, :], u1[:, :], u1[:])')
def compare_img_label_aware(img1 : cv.Mat, img2 : cv.Mat,
    colors : np.ndarray, color_amounts : np.ndarray):
    metric = 0.
    constant_part_full = 0.4
    constant_part = constant_part_full / colors.shape[0]
    weights = (1. - constant_part_full) * color_amounts / np.sum(color_amounts)
    for i in range(colors.shape[0]):
        matched = calculate_matches_for_color(img1, img2, colors[i])
        non_matched_percent = (color_amounts[i] - matched) / color_amounts[i]
        if non_matched_percent >= 0.8:
            continue
        metric += constant_part + 1. / (1. + np.abs((non_matched_percent) / 0.15) ** (2. * 1.77)) * weights[i]
        # metric += np.exp(-0.5 * (2 * non_matched_percent)**2) * weights[i]

    return metric


@njit
@cc.export('calculate_matches_for_color', 'u1(u1[:, :, :], u1[:, :, :], u1[:])')
def calculate_matches_for_color(img1 : cv.Mat, img2 : cv.Mat, color : np.ndarray):
    matched_amount = 0
    for y in range(img1.shape[0]):
        for x in range(img1.shape[1]):
            if (np.all(img1[y, x] == img2[y, x]) and
                np.all(img2[y, x] == color)):
                matched_amount += 1
    return matched_amount


@njit
@cc.export('reshape_njit', 'u1[:, :](u1[:, :, :])')
def reshape_njit(img):
    return img.reshape((img.shape[0] * img.shape[1], 3))


@njit
@cc.export('get_unique_colors_njit', 'u1[:, :](u1[:, :, :])')
def get_unique_colors_njit(image):
    max_unique_colors = 10
    cur_unique_colors = 0
    unique_colors = np.zeros((max_unique_colors, 3),
        dtype=np.uint8)
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            color = image[y, x]
            is_unique = True
            for unique_color in unique_colors:
                if np.all(unique_color == color):
                    is_unique = False
                    break
            if is_unique:
                unique_colors[cur_unique_colors] = color
                cur_unique_colors += 1
                if cur_unique_colors >= max_unique_colors:
                    return unique_colors

    return unique_colors[:cur_unique_colors]


@njit
@cc.export('color_amounts_njit', 'u2[:](u1[:, :], u1[:, :])')
def color_amounts_njit(reshaped_img, colors):
    color_amounts = np.zeros(colors.shape[0], dtype=np.int16)
    for i in range(colors.shape[0]):
        color = colors[i]
        count = 0
        for j in range(reshaped_img.shape[0]):
            if np.all(reshaped_img[j] == color):
                count += 1
        color_amounts[i] = count
    return color_amounts


@njit
@cc.export('color_amounts_init_njit', 'u2[:](u1[:, :, :], u1[:, :])')
def color_amounts_init_njit(img : cv.Mat, labels : np.ndarray):
    color_amounts = np.zeros(labels.shape[0], dtype=np.int16)
    for i in range(labels.shape[0]):
        color = labels[i]
        count = 0
        for y in range(img.shape[0]):
            for x in range(img.shape[1]):
                if np.all(img[y, x] == color):
                    count += 1
        color_amounts[i] = count
    return color_amounts


@njit
@cc.export('color_amounts_noreshape_njit', 'u2[:](u1[:, :, :], u1[:, :])')
def color_amounts_noreshape_njit(img : cv.Mat, colors : np.ndarray):
    color_amounts = np.zeros(colors.shape[0], dtype=np.int16)
    for i in range(colors.shape[0]):
        color = colors[i]
        count = 0
        for y in range(img.shape[0]):
            for x in range(img.shape[1]):
                if np.all(img[y, x] == color):
                    count += 1
        color_amounts[i] = count
    return color_amounts


class ProcessedImageComparer(object):
    def __init__(self, labels = None):
        self._labels = labels


    def compare_img(self, img1 : cv.Mat,
            img2 : cv.Mat,
            colors : np.ndarray,
            color_amounts : np.ndarray,
            valued_pixels_count : int):
        if img1.shape != img2.shape:
            raise ValueError(f'Image shapes do not match: {img1.shape}, {img2.shape}')
        # similarity_normalized = compare_img_label_aware(
        #     img1, img2, colors, color_amounts
        # )
        similarity_normalized = compare_img_njit(
            img1, img2, valued_pixels_count
        )

        return similarity_normalized


    def compare_pixels(self, p1 : np.ndarray,
            p2 : np.ndarray):
        return int(np.all(p1 == p2))


    def matched_pixels(self, img1, img2):
        return matched_pixels_img_njit(img1, img2)


    def get_color_amounts(self, img : cv.Mat, threshold : int):
        color_amounts = color_amounts_init_njit(img, self._labels)
        colors = self._labels[color_amounts > threshold]
        color_amounts = color_amounts[color_amounts > threshold]
        return colors, color_amounts
    

    def get_color_amounts_for_colors(self, img : cv.Mat, colors : np.ndarray):
        return color_amounts_noreshape_njit(img, colors)