from enum import Enum
from typing import Callable, Dict, List

import cv2
import numpy as np
from sklearn.cluster import DBSCAN, AgglomerativeClustering


class Cluster(object):
    def __init__(
            self, particles : np.ndarray,
            weights : np.ndarray
        ):
        self._particles = particles
        self._weights = weights
        self._total_weight = np.sum(weights)
        self._center = self.calculate_center(self._particles, self._weights)
        self._radius = self.calculate_radius(self._particles, self._center)

    @property
    def total_weight(self):
        return self._total_weight

    @property
    def center(self):
        return self._center

    @property
    def radius(self):
        return self._radius


    def calculate_radius(self, particles : np.ndarray, center : np.ndarray) -> float:
        distances = np.linalg.norm(particles[:, :2] - center[:2], axis=1)
        radius = np.max(distances)
        return radius


    def calculate_center(self, particles : np.ndarray, weights : np.ndarray):
        weighted_sum = np.zeros((3))
        weighted_sum[:2] = np.average(
            particles[:, :2], axis=0, weights=weights
        )

        x = np.sum(np.cos(particles[:, 2]) * weights)
        y = np.sum(np.sin(particles[:, 2]) * weights)
        weighted_sum[2] = np.arctan2(y, x)
        return weighted_sum


class Clusters(object):
    class Method(Enum):
        GMM = 0
        AGGLOMERATIVE = 1
        DBSCAN = 2

    def __init__(
            self, clusters : List[Cluster]
        ):
        self._clusters = clusters
        self._best = self.best_cluster(clusters)

    @property
    def best(self):
        return self._best

    @property
    def clusters(self):
        return self._clusters

    @classmethod
    def from_particles(
            cls, particles : np.ndarray,
            weights : np.ndarray, method=Method.AGGLOMERATIVE
        ):
        cluster_funcs : Dict[Clusters.Method, Callable] = {
            Clusters.Method.AGGLOMERATIVE : cls.agglo_cluster,
            Clusters.Method.DBSCAN : cls.DBSCAN_cluster,
            Clusters.Method.GMM : cls.GMM_cluster,
        }
        cluster_fun = cluster_funcs[method]
        clusters = cluster_fun(particles, weights)
        return cls(clusters)

    @classmethod
    def GMM_cluster(
            cls, particles : np.ndarray,
            weights : np.ndarray,
        ):
        pass

    @classmethod
    def agglo_cluster(cls, particles: np.ndarray, weights: np.ndarray):
        clustering = AgglomerativeClustering(
            n_clusters=None, distance_threshold=700
        )
        labels = clustering.fit_predict(particles[:, :2])
        n_clusters = len(np.unique(labels))
        clusters = []
        for i in range(n_clusters):
            mask = (labels == i)
            clusters.append(Cluster(
                particles[mask], weights[mask]
            ))

        return clusters

    @classmethod
    def DBSCAN_cluster(
            cls, particles : np.ndarray,
            weights : np.ndarray,
            eps=5, min_samples=20
        ):
        if len(particles) == 0:
            raise ValueError('Zero particles not allowed for clustering')
        clustering = DBSCAN(
            eps=eps, min_samples=min_samples
        ).fit(particles[:, :2])
        labels = clustering.labels_
        unique_labels = set(labels)
        clusters = []
        if len(unique_labels) == 1: # meaning no clusters
            return [Cluster(particles, weights)]
        for k in unique_labels:
            if k == -1:
                continue
            label_mask = (labels == k)
            clusters.append(Cluster(
                particles[label_mask],
                weights[label_mask],
            ))
        return clusters


    def best_cluster(self, clusters : List[Cluster]):
        return max(clusters, key=lambda cluster : cluster.total_weight)


if __name__ == '__main__':
    def generate_particles(gravity_points):
        particles = []
        weights = []
        for point, num_particles, weight, std_dev in gravity_points:
            particles.append(np.random.normal(loc=point, scale=std_dev, size=(num_particles, 3)))
            weights.append(np.full(num_particles, weight / num_particles))
        particles = np.vstack(particles)
        weights = np.hstack(weights)
        return particles, weights


    def visualize_particles(particles, estimated_position, image_size=(600, 600)):
        image = np.zeros(image_size + (3,), dtype=np.uint8)
        for particle in particles:
            x, y = int(particle[0]), int(particle[1])
            cv2.circle(image, (x, y), 2, (255, 255, 255), -1)
        ex, ey = int(estimated_position[0]), int(estimated_position[1])
        cv2.circle(image, (ex, ey), 5, (0, 0, 255), -1)
        return image

    gravity_points = [
        ([230, 230, 0.5], 999, 0.8, [15, 15, 0.1]),
        ([300, 300, 1.0], 200, 0.5, [15, 15, 0.1]),
        ([450, 450, 1.5], 100, 0.2, [15, 15, 0.1])
    ]

    particles, weights = generate_particles(gravity_points)
    clusters = Clusters.from_particles(particles, weights)
    best_cluster = clusters.best

    if best_cluster is not None:
        estimation = best_cluster.center
        print("Estimated Position:", estimation)
        visualization = visualize_particles(particles, estimation)
        cv2.imshow('Particle Filter Visualization', visualization)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    else:
        print("No valid cluster found.")
