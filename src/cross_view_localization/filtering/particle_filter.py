import json
import math
import multiprocessing
import random
from typing import Tuple

import cv2 as cv
import numpy as np
import rospy

from cross_view_localization.filtering.cluster import Clusters
from cross_view_localization.filtering.post_filter import PostFilter
from cross_view_localization.plugins.cvl_comp_plugin import CVLCompPlugin
from cross_view_localization.tools.image_rotation_cacher import \
    ImageRotationCacher
from cross_view_localization.tools.odometry_provider import OdometryProvider
from cross_view_localization.tools.warp_data import LandImg

BLACK = np.array([0,0,0], dtype=np.uint8)

class ParticleFilter(object):
    def __init__(
                self,
                sat_provider : CVLCompPlugin,
                odometry_provider : OdometryProvider,
                num_particles : int,
                sample_shape : Tuple[int, int],
                relative_dist_std_dev : float = 0.5,
                relative_angle_std_dev : float = 0.2,
                abs_dist_std_dev : float = 2.,
                abs_angle_std_dev : float = 0.3,
                use_delinear : bool = True,
                no_motion_no_update : bool = False,
            ):
        self._use_delinear = use_delinear
        self._no_motion_no_update = no_motion_no_update

        self._sat_provider = sat_provider
        self._odometry_provider = odometry_provider
        self._num_particles = num_particles

        self._post_filter = PostFilter(avg_window=3)

        self._relative_dist_std_dev = relative_dist_std_dev
        self._relative_angle_std_dev = relative_angle_std_dev
        self._abs_dist_std_dev = abs_dist_std_dev
        self._abs_angle_std_dev = abs_angle_std_dev

        self._bound_gap = math.ceil(
            math.sqrt(sample_shape[0]**2 + sample_shape[1]**2)
        )

        self._resamp_alpha = 45.
        self._resamp_beta = 9.

        self._weights_inertia = 0.0

        self._colors_threshold = 80
        self._high_weights_increase = 1.5

        self._state_estimate = np.array([0., 0., 0.])

        self._rotation_cacher = ImageRotationCacher(
            None, 18, # type: ignore
            prepare_rotations=False
        )

        self._slow_alpha = 0.001
        self._fast_alpha = 0.01
        self._weight_slow = 0.
        self._weight_fast = 0.
        self._average_weight = 0

        self._particles = np.array([], dtype=np.float64)
        self._weights = np.array([], dtype=np.float64)
        # DEBUG
        self._last_odom_step = np.array([0., 0.])
        self._last_timestamp = rospy.Time(secs=0)
        self._last_processed_img = None

    @property
    def state_estimate(self):
        return self._state_estimate

    @property
    def last_processed_img(self):
        if self._last_processed_img is None:
            return None
        return self._last_processed_img.img

    @property
    def clusters(self):
        return self._clusters

    @classmethod
    def from_config(
            cls, comp_plugin : CVLCompPlugin,
            odom_provider : OdometryProvider,
            config_path : str
        ):
        with open(config_path, 'r') as f:
            config = json.load(f)
        return cls(comp_plugin, odom_provider, **config)


    def set_odom_provider(self, odom_provider : OdometryProvider):
        self._odometry_provider = odom_provider


    def set_sat_provider(self, sat_provider : CVLCompPlugin):
        self._sat_provider = sat_provider


    def reset(self, x : float, y : float):
        self._particles = self.initialize_particles_around(x, y, 20)
        self._state_estimate = np.array([x, y, 0])

        self._weights = np.ones((
            self._particles.shape[0]
        )) / self._particles.shape[0]

    # State estimate: [x, y, phi] on a sat_img, phi in radians,
    # positive phi = clockwise rotation
    # TODO: prichesat
    def step(
            self, timestamp : rospy.Time,
            processed_img : LandImg,
            odometry_step_img : np.ndarray
        ):
        self._last_timestamp = timestamp
        self._last_odom_step = odometry_step_img
        self._last_processed_img = processed_img
        # PREDICTION
        self._particles = self.make_prediction(
            self._particles, odometry_step_img
        )
        new_metrics = self.generate_metrics(
            self._particles,
            processed_img
        )
        if self._no_motion_no_update:
            motion_amount = self.motion_amount(
                odometry_step_img[0], odometry_step_img[1]
            )
        else:
            motion_amount = 1.
        # TODO: fix with no_motion_no_update. this won't work with it
        sum = np.sum(new_metrics)

        # self._weights = new_metrics * motion_amount + self._weights * (1 - motion_amount)
        # sum = np.sum(self._weights)
        if np.allclose(sum, 0.):
            rospy.logwarn(f'ALL WEIGHTS ARE ZERO!')
            # self._weights = np.ones_like(self._weights) / len(self._weights)
        else:
            self._weights = new_metrics
            self._weights = self._weights / sum

        resampled_indices = self.systematic_resample(
            self._weights, motion_amount
        )
        self._particles = self._particles[
            resampled_indices
        ]
        self._weights = self._weights[
            resampled_indices
        ]

        self._clusters = Clusters.from_particles(self._particles, self._weights)
        self._state_estimate = self._clusters.best.center
        self._state_estimate = self._post_filter.filter(self._state_estimate, timestamp=timestamp)
        # self._state_estimate : np.ndarray = self.make_estimation(
        #     self._particles, self._weights
        # )

        # self._particles = self.amcl(self._particles, self._weights)

        return self._state_estimate


    def make_estimation(self, particles : np.ndarray, weights : np.ndarray):
        estimation = np.zeros((3))
        estimation[:2] = np.average(
            particles[:, :2], axis=0, weights=weights
        )

        x = np.sum(np.cos(particles[:, 2]) * weights)
        y = np.sum(np.sin(particles[:, 2]) * weights)
        estimation[2] = np.arctan2(y, x)
        return estimation


    def clip_x(self, x : float):
        return np.clip(
            x, self._bound_gap, self._sat_provider.get_sat_img_shape()[1] - self._bound_gap
        )


    def clip_y(self, y : float):
        return np.clip(
            y, self._bound_gap, self._sat_provider.get_sat_img_shape()[0] - self._bound_gap
        )


    def initialize_particles(self):
        return np.array([
            [self._bound_gap + (self._sat_provider.get_sat_img_shape()[0] - 2 * self._bound_gap) * random.random(),
             self._bound_gap + (self._sat_provider.get_sat_img_shape()[1] - 2 * self._bound_gap) * random.random(),
             2 * math.pi * random.random() - math.pi]
             for _ in range(self._num_particles)
        ])


    def initialize_particles_around(self, x_init : float, y_init : float, stddev : float):
        x_offsets, y_offsets = np.random.normal(scale=stddev, size=(2, self._num_particles))
        # Generate random angles in radians
        angles = np.random.uniform(low=-np.pi, high=np.pi, size=self._num_particles)

        # Combine the x and y offsets, angles, and initial coordinates into a set of particles
        particles = []
        for i in range(self._num_particles):
            x = x_init + x_offsets[i]
            y = y_init + y_offsets[i]
            phi = angles[i]
            particles.append([x, y, phi])

        return np.array(particles)


    def make_prediction(self, particles : np.ndarray,
            particle_delta : np.ndarray):
        '''particle_delta is [d, phi]'''
        d, phi = particle_delta
        angle_dev = np.abs(phi * self._relative_angle_std_dev) + self._abs_angle_std_dev
        pos_dev = np.max([0, np.abs(d) * self._relative_dist_std_dev + self._abs_dist_std_dev])

        phi_noised = [phi + np.random.normal(scale=angle_dev)
            for _ in range(particles.shape[0])]
        d_noised = [d + np.sign(d) * np.random.normal(scale=pos_dev)
            for _ in range(particles.shape[0])]
        particles[:, 2] = np.array([
            ((particles[:, 2][i] + math.pi + phi_noised[i])
                 % (2*math.pi) - math.pi)
            for i in range(particles.shape[0])
        ])

        pos_deltas = np.array([
            [np.sin(particles[i][2]) * d_noised[i], -np.cos(particles[i][2]) * d_noised[i]]
            for i in range(particles.shape[0])
        ])
        particles[:, 0] = np.array([
            self.clip_y(particles[i][0] + pos_deltas[i][0])
            for i in range(particles.shape[0])
        ])
        particles[:, 1] = np.array([
            self.clip_x(particles[i][1] + pos_deltas[i][1])
            for i in range(particles.shape[0])
        ])

        return particles


    def generate_metrics(self, particles : np.ndarray,
            processed_img : LandImg):
        img = processed_img.img
        valued_pixels_count = 0
        for row in img:
            for pixel in row:
                if not np.all(pixel == BLACK):
                    valued_pixels_count += 1

        eval_multiproc = multiprocessing.Array('f', particles.shape[0])
        evaluations = np.frombuffer(eval_multiproc.get_obj(), dtype=np.float32) # type: ignore

        self.metrics_task(
            processed_img,
            particles,
            colors = None,
            color_amounts = None,
            evaluations_out=evaluations,
            start=0,
            finish=self._num_particles
        )
        eval_sum = np.sum(evaluations)
        if eval_sum > 0:
            eval_max = np.max(evaluations)
            evaluations /= eval_max
            if self._use_delinear:
                # evaluations = 1 / (1 + ((1 - evaluations) / 0.10) ** (2 * 0.63))
                evaluations = self.softmax(evaluations, 0.18)
            weights = evaluations / np.sum(evaluations)
        else:
            weights = np.zeros_like(evaluations)
        return weights


    def softmax(self, x, temperature=0.5):
        """
        Compute the softmax of vector x with temperature scaling.

        Parameters:
        x (array-like): Input vector or 2D array for which to compute the softmax.
        temperature (float): Temperature parameter for scaling. Default is 1.0.

        Returns:
        np.ndarray: Softmax of the input.
        """
        # Subtracting the max value for numerical stability
        x_max = np.max(x, axis=-1, keepdims=True)
        e_x = np.exp((x - x_max) / temperature)
        return e_x / np.sum(e_x, axis=-1, keepdims=True)


    def metrics_task(
            self,
            img : LandImg,
            particles,
            colors,
            color_amounts,
            evaluations_out,
            start : int, finish : int
        ):
        self._sat_provider.prepare_img(img)
        for i in range(start, finish):
            # index = img_rotations.calculate_index(particles[i][2])
            # rotated_sample = self._rotation_cacher.get_image(phi=particles[i][2])
            # color_amounts = None
            # color_amounts = img_rotations.get_color_amounts(index)

            evaluation = self._sat_provider.compare_at(
                particles[i][0],
                particles[i][1],
                particles[i][2],
            )
            evaluations_out[i] = evaluation
            i += 1


    def multinomial_resample(self, particles : np.ndarray,
            weights : np.ndarray):
        cum_weights = np.cumsum(weights)
        u = np.random.uniform(size=len(particles))
        resampled_particles : np.ndarray = particles[np.searchsorted(cum_weights, u)]
        return resampled_particles

    # Returns float from 0 to 1
    def motion_amount(self, d : float, phi : float):
        x = (self._resamp_alpha * d +
             self._resamp_beta * np.abs(phi))
        return 2 / (1 + np.exp(-x)) - 1


    def systematic_resample(self, weights, resample_frac):
        N = len(weights)
        n_resample = round(resample_frac * N)
        if n_resample == 0:
            # avoid resampling if the resample fraction is too small
            return np.arange(N)
        cum_weights = np.cumsum(weights)
        cum_weights[-1] = 1.0  # avoid rounding errors
        indices = np.empty(N, dtype=np.int32)
        u = np.random.uniform(0, 1/N)
        j = 0
        for i in range(n_resample):
            while u > cum_weights[j]:
                j += 1
            indices[i] = j
            u += 1/N
        for i in range(n_resample, N):
            indices[i] = i
        return indices


    def draw_particles(self):
        img = self._sat_provider.get_sat_img().copy()
        i = 0
        max_weight = np.max(self._weights)
        if np.allclose(max_weight, 0.):
            return img
        while i < len(self._particles):
            x, y, phi = self._particles[i]
            dx = np.sin(-phi) * 13
            dy = -np.cos(-phi) * 13
            pt1 = (int(x), int(y))
            pt2 = (int(x+dx), int(y+dy))
            cv.arrowedLine(img,
                pt1, pt2,
                (
                    int(128 + (255 - 128) * self._weights[i]/max_weight),
                    int(128 + (0 - 128) * self._weights[i]/max_weight),
                    int(128 + (0 - 128) * self._weights[i]/max_weight)
                ),
                thickness=2,
                tipLength=self._weights[i]**0.4)
            i += 1
        if self._state_estimate is not None:
            x, y, phi = self._state_estimate
            dx = np.sin(-phi) * 13
            dy = -np.cos(-phi) * 13
            pt1 = (int(x), int(y))
            pt2 = (int(x+dx), int(y+dy))
            cv.arrowedLine(img,
                pt1, pt2, (0, 0, 0), thickness=4,
                tipLength=1)

        d, phi = self._last_odom_step
        dx = np.sin(-phi) * 19 * d
        dy = -np.cos(-phi) * 19 * d
        pt1 = (int(self._sat_provider.get_sat_img_shape()[1] - 15),
               int(self._sat_provider.get_sat_img_shape()[0] - 15))
        pt2 = (int(pt1[0] + dx), int(pt1[1] + dy))
        cv.arrowedLine(img,
            pt1, pt2, (0, 255, 0), thickness=2,
            tipLength=0.5)

        return img


    def high_weight_indices(self, weights):
        return np.where(self._weights > np.max(self._weights) / 2)[0]


    def amcl(self, particles : np.ndarray, weights : np.ndarray):
        self._average_weight = np.average(weights)
        self._weight_slow = self._slow_alpha * (self._average_weight - self._weight_slow)
        self._weight_fast = self._fast_alpha * (self._average_weight - self._weight_fast)
        probability = max(0., 1. - self._weight_fast/self._weight_slow) # type: ignore
        # rospy.logdebug(
        #     f'AMCL: w_slow={self._weight_slow}, w_fast={self._weight_fast}'+
        #     f'w_avg={self._average_weight}, prob={probability}'
        # )
        mask = np.random.rand(*particles.shape) < probability
        particles[mask] = self.initialize_particles()[mask]
        return particles
