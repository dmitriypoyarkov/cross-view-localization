import math
from collections import deque
from typing import Deque, List, Tuple, Union

import cv2
import numpy as np
import rospy
from rospy import Duration, Time


class PostFilter(object):
    def __init__(self, avg_window: int):
        self._avg_window = avg_window
        self._values = deque(maxlen=avg_window)
        self._timestamps : Deque[Union[Time, None]] = deque(maxlen=avg_window)


    def calculate_average(self) -> np.ndarray:
        if len(self._values) == 0:
            return np.array([0.0, 0.0, 0.0])

        values_array = np.array(self._values)
        avg_x = np.mean(values_array[:, 0])
        avg_y = np.mean(values_array[:, 1])
        sin_sum = np.sum(np.sin(values_array[:, 2]))
        cos_sum = np.sum(np.cos(values_array[:, 2]))
        avg_phi = math.atan2(sin_sum, cos_sum)

        return np.array([avg_x, avg_y, avg_phi])


    def filter(
            self, value: np.ndarray,
            timestamp : Union[Time, None] = None
        ) -> np.ndarray:
        self._timestamps.append(timestamp)
        self._values.append(value)
        return self.calculate_average()


    def get_timestamp(self) -> Time:
        '''
        Filter value has a delay because of window.
        This function returns the timestamp of the last
        value that is calculated based on window size.
        '''
        time0 = self._timestamps[0]
        time1 = self._timestamps[-1]
        if time0 is None or time1 is None:
            return rospy.Time(0)
        delta : Duration = (time1 - time0)
        result_ts : Time = time0 + delta / 2 # type: ignore
        return result_ts


if __name__ == '__main__':
    def simulate_robot_movement(num_steps: int, noise_level: float, radius: float) -> np.ndarray:
        path = []
        angle_step = 2 * np.pi / num_steps
        for i in range(num_steps):
            angle = i * angle_step
            x = radius * math.cos(angle) + np.random.normal(0, noise_level)
            y = radius * math.sin(angle) + np.random.normal(0, noise_level)
            phi = angle + np.random.normal(0, noise_level)
            path.append([x, y, phi])
        return np.array(path)

    def draw_path(image: np.ndarray, path: np.ndarray, color: Tuple[int, int, int]) -> None:
        for i in range(1, len(path)):
            cv2.line(image, (int(path[i-1][0]), int(path[i-1][1])), (int(path[i][0]), int(path[i][1])), color, 2)

    # Parameters
    num_steps = 100
    noise_level = 5.0
    avg_window = 3
    radius = 100

    # Simulate robot movement
    path = simulate_robot_movement(num_steps, noise_level, radius)

    # Create filter
    filter = PostFilter(avg_window=avg_window)

    # Apply filter
    filtered_path = np.array([filter.filter(step) for step in path])

    # Visualization
    image_size = 500
    image = np.zeros((image_size, image_size, 3), dtype=np.uint8)
    center = image_size // 2
    path[:, 0] += center
    path[:, 1] += center
    filtered_path[:, 0] += center
    filtered_path[:, 1] += center

    draw_path(image, path, (0, 0, 255))  # Original path in red
    draw_path(image, filtered_path, (0, 255, 0))  # Filtered path in green

    cv2.imshow('Robot Path', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
