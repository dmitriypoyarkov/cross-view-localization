

from typing import List, Tuple, Union

import numpy as np
import rospy

from cross_view_localization.objects.filter_input import FilterInput
from cross_view_localization.pipeline.motion_checker import MotionChecker
from cross_view_localization.tools.utils import (angle_z_from_vector_img,
                                                 vector_from_angle_z_img)


class PreFilter(object):
    '''
    This class takes the filter input and decides whether
    this input passes through. If it does, it is also saves
    the input to compare it to the newer inputs in the future.

    The point is to slow down the filter because too frequent
    updates are presumably harmful for the result.
    '''
    CHECK_TIME = 1 << 0 # 0001
    CHECK_MOTION = 1 << 1 # 0010
    def __init__(self, flags=CHECK_TIME | CHECK_MOTION,
                 time_threshold=0.2, distance_threshold=0.0,
                 angle_threshold=0.0):
        self._checks = {
            self.CHECK_TIME : self.check_time,
            self.CHECK_MOTION : self.check_motion
        }
        self._flags = flags
        self._last_input : Union[FilterInput, None] = None
        self._odom_steps = []

        self._motion_checker = MotionChecker(
            d_threshold=distance_threshold,
            phi_threshold=angle_threshold
        )

        self._time_threshold = rospy.Duration(secs=time_threshold) # type: ignore


    def check_time(self, filter_input : FilterInput):
        if self._last_input is None:
            return True
        if filter_input.timestamp - self._last_input.timestamp >= self._time_threshold:
            return True
        return False


    def check_motion(self, filter_input : FilterInput):
        if self._last_input is None:
            return True
        return self._motion_checker.check_motion(filter_input.odom_step)


    def accumulate_input(self, filter_input : FilterInput):
        self._odom_steps.append(filter_input.odom_step)
        filter_input.odom_step = self.odom_sum(self._odom_steps)
        return filter_input


    def odom_sum(self, odom_steps : List):
        vector = np.array([0., 0., 0.]) # [x, y, phi]
        for step in odom_steps:
            pos_delta = step[0] * vector_from_angle_z_img(step[1])
            next_vector = np.array([pos_delta[0], pos_delta[1], step[1]])
            vector += next_vector
        vector_dir = angle_z_from_vector_img(vector[:2])
        is_forward = int(1 if np.abs(vector_dir - vector[2]) <= np.pi / 4 else -1)
        return np.array([is_forward * np.linalg.norm(vector[:2]), vector[2]])


    def reset_accumulated_input(self, filter_input : FilterInput):
        self._odom_steps = []
        self._last_input = filter_input


    def check_input(
            self, filter_input : FilterInput
        ):
        filter_input = self.accumulate_input(filter_input)
        # print(f'PRE FILTER: {filter_input.odom_step}')
        for flag, perform_check in self._checks.items():
            check_required = flag & self._flags
            if not check_required:
                continue
            if not perform_check(filter_input):
                return False
        self.reset_accumulated_input(filter_input)
        # print(f'PRE FILTER: SUCCES')
        return True