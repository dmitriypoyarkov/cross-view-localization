import random
import shutil
import sys
import time
from typing import List

import wcwidth

BLUE = "\033[34m"
COLOR_END = "\033[0m"

class WaitAnimation(object):
    def __init__(self, text=''):
        self._animation = [".    ", "..   ", "...  ", ".... ", "....."]
        self.set_text(text)
        self._animation = self.paint(BLUE, self._animation)
        self._cur_index = 0
        self._started = False

        self._text_refresh_period = 8. # seconds
        self._last_text_refresh = time.time()


    def set_text(self, text : str):
        self._text = self.paint_text(BLUE, text)
        self._refreshable_text_part = text.split('\n')[-1]
        self._cr = ''
        for i in range(len(self._animation[0]) + len(self._refreshable_text_part)):
            self._cr += '\033[1D'
        self._initial_animation_uncolored = [
            self._refreshable_text_part + frame for frame in self._animation
        ]
        self._actual_animation_uncolored = self._initial_animation_uncolored
        self._actual_animation = self.paint(BLUE, self._actual_animation_uncolored)
        self.fit_to_terminal()



    def add_text(self, text : str, animation):
        return [
            f'{text}{a}'
            for a in animation
        ]


    def paint_text(self, color : str, text : str):
        return f'{color}{text}{COLOR_END}'


    def paint(self, color, animation : List[str]):
        return [
            f'{color}{a}{COLOR_END}'
            for a in animation
        ]


    def write_text(self, text : str):
        sys.stdout.write(f'\n{text}')
        self._last_text_refresh = time.time()


    def update_old(self, new_text=None):
        if new_text is not None:
            new_text_painted = self.paint_text(BLUE, new_text)
            if self._text != new_text_painted:
                self._text = new_text_painted
                self._started = False
        if not self._started:

            self.write_text(self._text)
            self._started = True
        if time.time() - self._last_text_refresh > self._text_refresh_period:
            self.write_text(self._text)

        sys.stdout.write(self._animation[self._cur_index] + self._cr)
        sys.stdout.flush()
        self._cur_index = (self._cur_index + 1) % len(self._animation)


    def get_actual_width(self, string : str):
        return sum(wcwidth.wcswidth(c) for c in string)


    def fit_to_terminal(self):
        terminal_size = shutil.get_terminal_size()[0]
        resize_not_needed = True
        if len(self._actual_animation_uncolored[0]) > terminal_size:
            resize_not_needed = False
        # if we use cut text and terminal size is bigger than it
        init_anim_len = len(self._initial_animation_uncolored[0])
        using_cut_text = (init_anim_len > len(self._actual_animation_uncolored[0]))
        if using_cut_text and terminal_size > len(self._actual_animation_uncolored[0]):
            resize_not_needed = False

        if resize_not_needed:
            return

        target_anim_len = min(init_anim_len, terminal_size)

        self._actual_animation_uncolored = [
            frame[-target_anim_len:] for frame in self._initial_animation_uncolored
        ]
        self._actual_animation = self.paint(BLUE, self._actual_animation_uncolored)


    def update(self, new_text=None):
        if new_text is not None:
            new_text_painted = self.paint_text(BLUE, new_text)
            if self._text != new_text_painted:
                self.set_text(new_text)
                self._started = False
        if not self._started:
            self.write_text(self._text)
            self._started = True

        self.fit_to_terminal()

        sys.stdout.write(self._actual_animation[self._cur_index] + self._cr)
        sys.stdout.flush()
        self._cur_index = (self._cur_index + 1) % len(self._animation)


    def stop(self):
        if not self._started:
            return
        sys.stdout.write('\n')
        sys.stdout.flush()


if __name__ == "__main__":
    wa = WaitAnimation(text='waitDFfs<GFKHGfdghfdhghgfhjdjhtytujuykjyukjyfumyguiygmyujumiyuffhgrfdhytjytjtuyfjuytfjyffugjb yj yt jkyuk gf kjgf kuykuykuyf ky fkkKLSDGFLHDGFLGFLKHDGF LFKGLUIELUIH GLIH ILUG HUILing')
    for i in range(1000):
        time.sleep(random.random() * 0.5 + 0.05)
        wa.update()