from typing import Tuple, Union

import cv2 as cv
import numpy as np
import rospy
from cv2.typing import MatLike
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

from cross_view_localization.debug_stage.img_utils import add_text_to_image
from cross_view_localization.filtering.cluster import Cluster
from cross_view_localization.objects.eval_data import EvalData
from cross_view_localization.objects.particle_data import ParticleData
from cross_view_localization.objects.pipeline_data import PipelineData
from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.tools.odometry_provider import OdometryProvider
from cross_view_localization.tools.utils import rotate_pt


class ParticleImgPub(object):
    def __init__(
            self,
            sat_img : SatelliteImage,
            pub_topic : str, odom_provider : OdometryProvider
        ):
        self._sat_img = sat_img
        self._pub = rospy.Publisher(pub_topic, Image, queue_size=1)
        self._bridge = CvBridge()

        self._draw_clusters = False
        self._draw_odom = rospy.get_param('~draw_odom', default=False)
        self._draw_ref = False

        self._odom_provider = odom_provider
        self._odom_pos_list = []
        self._center = [sat_img.img_center_x, sat_img.img_center_y, 0.]


    def draw_and_publish(
            self, pipeline : PipelineData,
            eval_data : Union[EvalData, None] = None
        ):
        msg = self._bridge.cv2_to_imgmsg(
            self.draw_particle_img(pipeline, eval_data), 'rgb8'
        )
        self._pub.publish(msg)


    def draw_particle_img(
            self, pipeline : PipelineData, eval_data : Union[EvalData, None]
        ):
        img = self._sat_img.img.copy()
        img = self.draw_particles(img, pipeline)
        img = self.draw_eval(img, pipeline, eval_data)
        img = self.put_coord(img, pipeline)
        img = self.draw_odom_pose(img, pipeline)
        return img


    def draw_odom_pose(self, img, pipeline : PipelineData):
        try:
            next_pos = self._odom_provider.get_odom_pose_rel(pipeline.timestamp)
        except Exception:
            return img
        shifted_pos = np.array(next_pos) + np.array(self._center)
        self._odom_pos_list.append(shifted_pos)
        img = self.draw_pos_list(img, self._odom_pos_list)
        return img


    def put_coord(self, img : MatLike, pipeline : PipelineData):
        if pipeline.clusters is None:
            return img
        coord = pipeline.clusters.best.center
        coord_str = f'x: {coord[0]:.1f} y: {coord[1]:.1f} phi: {coord[2]:.2f}]'
        return add_text_to_image(img, coord_str)


    def draw_eval(self, img, pipeline : PipelineData, eval_data : Union[EvalData, None]):
        if eval_data is None:
            return img
        if self._draw_ref:
            ref_pos = eval_data.get_ref_pose(pipeline.timestamp.to_nsec())
            cv.circle(img, (int(ref_pos[0]), int(ref_pos[1])), 4, (0, 0, 0), -1)
        # draw odom
        if self._draw_odom:
            img = self.draw_odom(img, eval_data)
        # draw cur ref pos
        return img

    # TODO: inspect
    def draw_odom(self, img, eval_data : EvalData):
        x, y, phi = eval_data.start_pos
        # phi = -phi + np.pi / 2
        prev_x, prev_y = x, y
        cv.circle(img, (int(x), int(y)), 5, (0, 0, 255), -1)
        for d, d_phi in eval_data.odom_steps:
            phi += d_phi
            step = d * rotate_pt(
                np.array([0., -1.]), np.array([0., 0.]), phi
            )
            x += step[0]
            y += step[1]
            cv.line(img, (int(prev_x), int(prev_y)), (int(x), int(y)), (0, 0, 0), 2)
            prev_x, prev_y = x, y
        return img


    def draw_pos_list(self, img, pos_list):
        if len(pos_list) <= 1:
            return img
        color = (255, 0, 255)
        x, y = pos_list[0][:2]
        for pos in pos_list[1:]:
            n_x, n_y = pos[:2]
            img = cv.line(img, (int(x), int(y)), (int(n_x), int(n_y)), color, 2)
            x, y = n_x, n_y
        img = self.draw_arrow(img, pos_list[-1], color)
        return img


    def draw_particles(self, img : MatLike, pipeline : PipelineData):
        i = 0
        particles = pipeline.particles
        weights = pipeline.weights
        if len(particles) == 0:
            return img
        max_weight = np.max(weights)
        if np.allclose(max_weight, 0.):
            return img
        for i in range(len(particles)):
            color = (
                int(128 + (255 - 128) * weights[i]/max_weight),
                int(128 + (0 - 128) * weights[i]/max_weight),
                int(128 + (0 - 128) * weights[i]/max_weight)
            )
            self.draw_arrow(img, particles[i], color, tip_len=weights[i] ** 3)
        if self._draw_clusters and pipeline.clusters is not None:
            for cluster in pipeline.clusters.clusters:
                img = self.draw_cluster(img, cluster)
        if pipeline.clusters is not None:
            img = self.draw_arrow(img, pipeline.clusters.best.center, (0, 0, 0))
        img = self.draw_odom_arrow(img, pipeline.odom_step_img)
        return img


    def draw_odom_arrow(self, img : MatLike, odom_step : np.ndarray):
        d, phi = odom_step
        origin = img.shape[1] - 15, img.shape[0] - 15
        particle = np.array([origin[0], origin[1], phi])
        arrow_len = 16 * d
        return self.draw_arrow(img, particle, (0, 0, 0), arrow_len)


    def draw_cluster(self, img : MatLike, cluster : Cluster):
        x, y, _ = cluster.center
        center = (int(x), int(y))
        img = cv.circle(img, center, int(cluster.radius), (255, 255, 255), 2)
        img = self.draw_arrow(img, cluster.center, (0, 0, 0))
        return img


    def draw_arrow(
            self, img : MatLike,
            particle : np.ndarray, color : Tuple,
            arrow_len = 13, tip_len : float = 1
        ):
        pos = particle[:2]
        phi = particle[2]
        arrow_tip = np.array([pos[0], pos[1] - arrow_len])
        arrow_tip = rotate_pt(arrow_tip, pos, phi)
        pt1 = (int(pos[0]), int(pos[1]))
        pt2 = (int(arrow_tip[0]), int(arrow_tip[1]))
        return cv.arrowedLine(
            img, pt1, pt2,
            color, thickness=2,
            tipLength=tip_len
        )