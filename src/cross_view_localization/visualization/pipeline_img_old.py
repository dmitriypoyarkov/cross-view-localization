
from typing import List

import cv2
import numpy as np


class PipelineVisualizer(object):
    def __init__(self):
        self._font_scale = 1.
        self._thickness = 2
        self._cell_padding = 5


    def create_grid_img(self, content_grid):
        column_widths, row_heights = self.calculate_column_widths_and_row_heights(content_grid)
        return self.create_grid_image_with_content(content_grid, column_widths, row_heights)


    def create_img_with_text(self, img, text_lines : List[str]):
        img_height, img_width = img.shape[:2]
        text_height = cv2.getTextSize('A', cv2.FONT_HERSHEY_SIMPLEX,
                                      self._font_scale, self._thickness)[0][1]
        new_img_height = img_height + len(text_lines) * (text_height + 5)
        new_img = np.zeros((new_img_height, img_width, 3), dtype=np.uint8)
        new_img.fill(255)
        new_img[:img_height, :, :] = img
        text_y = img_height + text_height
        for line in text_lines:
            cv2.putText(new_img, line, (5, text_y),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        self._font_scale, (0, 0, 0), self._thickness)
            text_y += text_height + 5
        return new_img


    def calculate_column_widths_and_row_heights(self, content_grid):
        num_rows, num_cols = content_grid.shape
        column_widths = [0] * num_cols
        row_heights = [0] * num_rows

        for row in range(num_rows):
            for col in range(num_cols):
                content = content_grid[row, col]
                if isinstance(content, str):  # Calculate size for text
                    text_size = cv2.getTextSize(
                        content, cv2.FONT_HERSHEY_SIMPLEX,
                        self._font_scale, self._thickness
                    )[0]
                    content_width, content_height = text_size[0], text_size[1]
                elif isinstance(content, np.ndarray):  # Use size for images
                    content_height, content_width = content.shape[:2]
                else:
                    raise ValueError("Unsupported content type. Only str (text) or np.ndarray (image) are supported.")

                if content_width > column_widths[col]:
                    column_widths[col] = content_width
                if content_height > row_heights[row]:
                    row_heights[row] = content_height

        return column_widths, row_heights


    def create_grid_image_with_content(
            self, content_grid, column_widths, row_heights
        ):
        num_rows, num_cols = content_grid.shape

        grid_height = sum(row_heights) + (num_rows + 1) * self._cell_padding
        grid_width = sum(column_widths) + (num_cols + 1) * self._cell_padding

        grid_image = np.ones((grid_height, grid_width, 3), dtype=np.uint8) * 255

        current_y = self._cell_padding
        for row in range(num_rows):
            current_x = self._cell_padding
            for col in range(num_cols):
                top_left = (current_x, current_y)
                bottom_right = (current_x + column_widths[col], current_y + row_heights[row])
                cv2.rectangle(grid_image, top_left, bottom_right, (0, 0, 0), 1)

                content = content_grid[row, col]
                if isinstance(content, str):
                    text_image = self.create_text_image(content, column_widths[col], row_heights[row])
                    self.place_image(grid_image, text_image, top_left)
                elif isinstance(content, np.ndarray):
                    content_resized = self.resize_and_pad_image(content, column_widths[col], row_heights[row])
                    self.place_image(grid_image, content_resized, top_left)

                current_x += column_widths[col] + self._cell_padding
            current_y += row_heights[row] + self._cell_padding

        return grid_image

    def create_text_image(self, text, width, height):
        text_image = np.ones((height, width, 3), dtype=np.uint8) * 255
        text_size = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, self._font_scale, self._thickness)[0]
        text_x = (width - text_size[0]) // 2
        text_y = (height + text_size[1]) // 2
        cv2.putText(
            text_image, text, (text_x, text_y),
            cv2.FONT_HERSHEY_SIMPLEX, self._font_scale,
            (0, 0, 0), self._thickness
        )
        return text_image

    def resize_and_pad_image(self, image, target_width, target_height):
        old_height, old_width = image.shape[:2]
        new_image = np.ones((target_height, target_width, 3), dtype=np.uint8) * 255

        aspect_ratio = old_width / old_height
        if target_width / target_height > aspect_ratio:
            new_height = target_height
            new_width = int(new_height * aspect_ratio)
        else:
            new_width = target_width
            new_height = int(new_width / aspect_ratio)

        resized_image = cv2.resize(image, (new_width, new_height))

        x_center = (target_width - new_width) // 2
        y_center = (target_height - new_height) // 2

        new_image[y_center:y_center + new_height, x_center:x_center + new_width] = resized_image
        return new_image

    def place_image(self, grid_image, content_image, top_left):
        x_start, y_start = top_left
        content_height, content_width = content_image.shape[:2]
        y_end, x_end = y_start + content_height, x_start + content_width

        test_part = grid_image[y_start:y_end, x_start:x_end]
        print(test_part.shape)
        print(content_image.shape[:2])
        grid_image[y_start:y_end, x_start:x_end] = content_image


if __name__ == '__main__':

    content_grid = np.array([
        ["Text 1", "Text 2", np.zeros((250, 100, 3), dtype=np.uint8)],
        [np.ones((100, 50, 3), dtype=np.uint8) * 255, "Text 3", "Text 4"]
    ]) # type: ignore

    vis = PipelineVisualizer()
    grid_img = vis.create_grid_img(content_grid)

    cv2.imshow('Grid Image', grid_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
