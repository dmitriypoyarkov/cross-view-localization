from typing import List

import cv2 as cv
import numpy as np
from cv2.typing import MatLike

import cross_view_localization.package_settings as settings
from cross_view_localization.debug_stage.img_utils import (create_sample_image,
                                                           depth_to_rgb,
                                                           put_warp_on_map)
from cross_view_localization.objects.pipeline_data import PipelineData
from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.tools.satellite_image_provider import \
    PreparedSampleImg


class CardColumn(object):
    def __init__(self, cards : List[MatLike], padding : int):
        self._cards = cards
        self._padding = padding

        self._height = (
            sum([card.shape[0] for card in self._cards]) +
            (len(self._cards) - 1) * self._padding
        )
        self._width = max([
            card.shape[1] for card in self._cards
        ])

    @property
    def height(self):
        return self._height

    @property
    def width(self):
        return self._width


    def put_on_img(self, img : MatLike, x : int, y : int):
        '''x, y - top left'''
        y1 = y - self._padding
        x0 = x
        for i in range(len(self._cards)):
            card = self._cards[i]
            y0 = y1 + self._padding
            y1 = y0 + card.shape[0]
            x1 = x0 + card.shape[1]
            img[y0:y1, x0:x1] = card
        return img


class CardRow(object):
    def __init__(self, columns : List[CardColumn], padding : int):
        self._columns = columns
        self._padding = padding
        self._width = sum([col.width for col in columns])
        self._height = max([col.height for col in columns])

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height


    def put_on_img(self, img : MatLike, x : int, y : int):
        '''x, y - top left'''
        for i in range(len(self._columns)):
            col = self._columns[i]
            col.put_on_img(img, x, y)
            x += col.width + self._padding
        return img


class PipelineVisualizer(object):
    def __init__(self, sat_img : SatelliteImage):
        self._font = cv.FONT_HERSHEY_SIMPLEX
        self._font_scale = .6
        self._font_thickness = 2
        self._font_color = (0, 0, 0)
        self._padding = 10
        self._big_padding = 20
        self._sat_img = sat_img


    def create_map_card(self, pipeline : PipelineData):
        sat = self._sat_img.img.copy()
        warp = pipeline.warped_img.img
        prep = PreparedSampleImg(pipeline.warped_img, self._sat_img.pixels_per_meter)
        if pipeline.clusters is not None:
            coords = pipeline.clusters.best.center
            sat = put_warp_on_map(prep.img, sat, coords)
        return sat


    def create_pipeline_img(self, pipeline : PipelineData):
        warp_datas = pipeline.warp_data.warp_datas
        depth_cards = []
        rgb_cards = []
        for data in warp_datas:
            rgb_card = self.create_card(
                data.rgb_img, [str(data.timestamp_rgb)]
            )
            rgb_cards.append(rgb_card)
            depth_card = self.create_card(
                depth_to_rgb(data.depth), [str(data.timestamp_depth)]
            )
            depth_cards.append(depth_card)
        warped_img = cv.resize(pipeline.warped_img.img, (200, 200))
        proc_img = cv.resize(pipeline.processed_img.img, (200, 200))
        warp_card = self.create_card(warped_img, ['warp'])
        proc_card = self.create_card(proc_img, ['proc'])
        map_card = self.create_map_card(pipeline)

        column_padding = self._big_padding
        rgb_column = CardColumn(rgb_cards, column_padding)
        depth_column = CardColumn(depth_cards, column_padding)
        warp_column = CardColumn([warp_card, proc_card], column_padding)
        map_column = CardColumn([map_card], column_padding)
        columns = [rgb_column, depth_column, warp_column, map_column]

        row = CardRow(columns, column_padding)
        height = row.height + 2 * self._big_padding
        width = row.width + (len(columns) + 1) * self._big_padding

        pipe_img = np.ones(
            (height, width, 3), dtype=np.uint8
        ) * 255

        row.put_on_img(pipe_img, self._big_padding, self._big_padding)

        return pipe_img


    def create_card(self, img : MatLike, text_lines : List[str]):
        line_heights = []
        for line in text_lines:
            line_heights.append(cv.getTextSize(
                line, self._font, self._font_scale, self._font_thickness
            )[0][1])

        final_height = (
            img.shape[0] + sum(line_heights) +
            (len(text_lines) + 1) * self._padding
        )
        img_with_text = np.ones(
            (final_height, img.shape[1], 3),
            dtype=np.uint8
        ) * 255
        img_with_text[:img.shape[0], :img.shape[1]] = img
        for i, line in enumerate(text_lines):
            img_y = (
                img.shape[0] + sum(line_heights[:i])
                + line_heights[0] + (i+1) * self._padding
            )
            cv.putText(
                img_with_text, line, (self._padding, img_y),
                self._font, self._font_scale,
                self._font_color, self._font_thickness
            )
        return img_with_text


def main():
    img = create_sample_image(400, 400)
    text_lines = ['depth_oakd_fr', 'timestamp: 124.12']
    path = settings.area_path('building_190', 'sat_img.bmp')
    vis = PipelineVisualizer(SatelliteImage.from_file(path))
    img = vis.create_card(img, text_lines)
    cv.imshow('win', img)
    cv.waitKey(0)
    cv.destroyAllWindows()


if __name__ == '__main__':
    main()