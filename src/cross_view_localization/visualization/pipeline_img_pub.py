import cv2 as cv
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

from cross_view_localization.objects.pipeline_data import PipelineData
from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.visualization.pipeline_img import \
    PipelineVisualizer


class PipelineImgPub(object):
    def __init__(self, sat_img_path : str):
        self._vis = PipelineVisualizer(SatelliteImage.from_file(sat_img_path))
        self._pub = rospy.Publisher('~pipeline_img', Image, queue_size=1)
        self._bridge = CvBridge()


    def publish(self, pipeline : PipelineData):
        img_cv = cv.cvtColor(
            self._vis.create_pipeline_img(pipeline), cv.COLOR_BGR2RGB
        )
        imgmsg = self._bridge.cv2_to_imgmsg(img_cv)
        self._pub.publish(imgmsg)