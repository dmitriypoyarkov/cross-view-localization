import json
import os

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MaxNLocator

import cross_view_localization.package_settings as settings
from cross_view_localization.objects.sat_img import SatelliteImage


class EvalVisualizer(object):
    def __init__(self, eval_file, path_file, area, run):
        # load files
        # load sat img
        # draw ref
        # draw exp
        # draw odom
        self._run = run
        self._ref_coords_img_path = settings.run_path(self._run, 'ref_path_img.json')
        self._ref_coords_path = settings.run_path(self._run, 'ref_path.json')
        self._odom_file = settings.output_path(f'{self._run}_eval/odom_steps.json')

        with open(settings.run_path(run, 'run.json')) as f:
            run_config = json.load(f)
        self._start_pos = run_config['start_pos']

        with open(self._ref_coords_img_path, 'r') as f:
            self._ref_path = np.array(json.load(f))
        with open(path_file, 'r') as f:
            self._exp_path = np.array(json.load(f))
        with open(self._odom_file, 'r') as f:
            self._odom_steps = np.array(json.load(f))


        self._ref_path_dense = self.interpolate_coordinates(self._ref_path, 300)

        self._sat_img = SatelliteImage.from_file(settings.area_path(area, 'sat_img.bmp'))
        self._pixels_per_meter = self._sat_img.pixels_per_meter

        self._eval_file = eval_file
        # with open(eval_file, 'r') as f:
        #     eval_json = json.load(f)

    def draw_ref(self):
        ref_img = self._sat_img.img.copy()

        for i in range(len(self._ref_path) - 1):
            cur = self._ref_path[i].astype(np.uint16)
            next = self._ref_path[i + 1].astype(np.uint16)
            cv.line(ref_img, (cur[0], cur[1]), (next[0], next[1]), (0, 0, 0), 2)

        cv.imwrite(settings.run_path(self._run, 'ref_path.bmp'), cv.cvtColor(ref_img, cv.COLOR_BGR2RGB))


    def make_odom_path(self, odom_steps, start_pos):
        x, y, phi = start_pos
        odom_path = [start_pos]
        for d, d_phi in odom_steps:
            phi -= d_phi
            x += d * np.sin(phi)
            y -= d * np.cos(phi)
            odom_path.append([x, y, phi])
        return np.array(odom_path).tolist()


    def draw_odom(self):
        img = self._sat_img.img.copy()

        odom_path = np.array(self.make_odom_path(self._odom_steps, self._start_pos))
        for i in range(len(odom_path) - 1):
            cur = odom_path[i].astype(np.uint16)
            next = odom_path[i + 1].astype(np.uint16)
            cv.line(img, (cur[0], cur[1]), (next[0], next[1]), (0, 0, 0), 2)

        cv.imwrite(settings.run_path(self._run, 'odom_path.bmp'), cv.cvtColor(img, cv.COLOR_BGR2RGB))


    def interpolate_coordinates(self, points : np.ndarray, num_points : int):
        timestamps = points[:, 2]

        min_time = np.min(timestamps)
        max_time = np.max(timestamps)

        min_time_diff = np.min(np.diff(timestamps))
        min_required_steps = int((max_time - min_time) / min_time_diff)

        num_points = max(min_required_steps, num_points)

        step_size = (max_time - min_time) / (num_points + 1)

        intermediate_timestamps = np.arange(min_time + step_size, max_time, step_size)

        interpolated_coordinates = []
        for timestamp in intermediate_timestamps:
            if timestamp < timestamps[0]:
                before_point = points[0]
                after_point = points[1]
            elif timestamp > timestamps[-1]:
                before_point = points[-2]
                after_point = points[-1]
            else:
                before_idx = np.where(timestamps < timestamp)[0][-1]
                after_idx = np.where(timestamps > timestamp)[0][0]
                before_point = points[before_idx]
                after_point = points[after_idx]

            t_diff = after_point[2] - before_point[2]
            weight_after = (timestamp - before_point[2]) / t_diff
            weight_before = 1 - weight_after
            coord = weight_before * before_point[:2] + weight_after * after_point[:2]
            coord = [coord[0], coord[1], timestamp]

            interpolated_coordinates.append(coord)

        return np.array(interpolated_coordinates)


    def draw_exp(self):
        img = self._sat_img.img.copy()

        for i in range(len(self._exp_path) - 1):
            cur = self._exp_path[i].astype(np.uint16)
            next = self._exp_path[i + 1].astype(np.uint16)
            cv.line(img, (cur[0], cur[1]), (next[0], next[1]), (0, 0, 0), 2)

        path_parts = self._eval_file.split('.')
        if len(path_parts) <= 1:
            raise Exception('???????')
        path_parts[-1] = 'bmp'
        self._save_path = '.'.join(path_parts)
        cv.imwrite(self._save_path, cv.cvtColor(img, cv.COLOR_RGB2BGR))

    @classmethod
    def draw_plot(cls, medians, medians_t, mads, mads_t):
        x = np.arange(len(medians))

        plt.figure(figsize=(10, 6))
        plt.rcParams.update({'font.size': 14})
        plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
        plt.plot(x, medians, label='Медиана отклонения', color='b', marker='o')
        plt.plot(x, medians_t, label='Медиана отклонения (t)', color='g', marker='s')

        # Add MAD as an error bar
        plt.errorbar(x, medians, yerr=mads, fmt='o', ecolor='b', capsize=5, label='Разброс медианы')
        # plt.errorbar(x, medians_t, yerr=mads_t, fmt='s', ecolor='g', capsize=5, label='MAD (t)')

        # Customizations
        plt.title('')
        plt.xlabel('Номер запуска')
        plt.ylabel('Отклонение, м')
        plt.ylim(0, 5)
        plt.legend()
        plt.grid(True)
        # plt.style.use('seaborn-darkgrid')  # Use a different style
        plt.show()


    def skip_time(self, exp_path, skip_amount):
        min_exp_time = np.min(exp_path[:, 3]) + skip_amount
        return np.argmin(np.abs(exp_path[:, 3] - min_exp_time))


    def err_timed(self):
        diffs = []
        for exp_point in self._exp_path:
            closest_ref_point = self.closest_by_time(exp_point[3], self._ref_path_dense)
            diffs.append(np.linalg.norm(exp_point[:2] - closest_ref_point[:2]))

        diffs = np.array(diffs)
        median = np.median(diffs)
        mad = np.median(np.fabs(diffs - median))
        return median / self._pixels_per_meter, mad / self._pixels_per_meter


    def err_nottimed(self):
        diffs = []
        for exp_point in self._exp_path:
            closest_ref_point = self.closest_by_distance(exp_point[:2], self._ref_path_dense)
            diffs.append(np.linalg.norm(exp_point[:2] - closest_ref_point[:2]))

        diffs = np.array(diffs)
        median = np.median(diffs)
        mad = np.median(np.fabs(diffs - median))
        return median / self._pixels_per_meter, mad / self._pixels_per_meter


    def closest_by_distance(self, point, ref_path):
        closest_ref_idx = np.argmin(np.linalg.norm(ref_path[:, :2] - point, axis=1))
        return ref_path[closest_ref_idx]


    def closest_by_time(self, stamp, ref_path):
        closest_ref_idx = np.argmin(np.abs(ref_path[:, 2] - stamp))
        return ref_path[closest_ref_idx]


def draw_plots():
    medians_arr = []
    mads_arr = []
    medians_arr_t = []
    mads_arr_t = []
    for i in range(10):
        eval_file = settings.output_path(f'run_001_eval/eval_{i}.json')
        path_file = settings.output_path(f'run_001_eval/path_{i}.json')
        vis = EvalVisualizer(eval_file, path_file, 'building_190', 'run_001')
        # vis.draw_exp()
        median, mad = vis.err_nottimed()
        median_t, mad_t = vis.err_timed()
        medians_arr.append(median)
        medians_arr_t.append(median_t)
        mads_arr.append(mad)
        mads_arr_t.append(mad_t)

    EvalVisualizer.draw_plot(medians_arr, medians_arr_t, mads_arr, mads_arr_t)

def main():
    eval_file = settings.output_path(f'run_001_eval/eval_{0}.json')
    path_file = settings.output_path(f'run_001_eval/path_{0}.json')
    vis = EvalVisualizer(eval_file, path_file, 'building_190', 'run_001')
    vis.draw_odom()


if __name__ == '__main__':
    main()