import time
from typing import List

import cv2 as cv
import numpy as np
import rospy
import tf
import tf2_ros as tf2
import tf.transformations as tr
from cv2.typing import MatLike
from geometry_msgs.msg import (Point, Pose, PoseArray, PoseStamped, Quaternion,
                               Transform, TransformStamped, Vector3)
from nav_msgs.msg import MapMetaData, OccupancyGrid
from std_msgs.msg import Header

from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.tools.transorm_listener_extended import \
    TransformListenerExtended
from cross_view_localization.tools.utils import (angle_z_from_quaternion,
                                                 quaternion_from_angle_z,
                                                 unpack_ts)


class RvizPublisher(object):
    def __init__(
            self, odom_frame : str,
            base_link_frame : str,
            map_frame : str,
            sat_img = None,
        ):
        self._map_frame = map_frame
        if sat_img is not None:
            self._map = self.convert_map(sat_img, rospy.Time(0))
        self._map_pub = rospy.Publisher('sat_map', OccupancyGrid, queue_size=1)
        self._particle_pub = rospy.Publisher('particles', PoseArray, queue_size=1)
        self._robot_pos_pub = rospy.Publisher('base_link_cvl', PoseStamped, queue_size=1)
        self._listener = TransformListenerExtended()
        self._odom_frame = odom_frame
        self._base_link_frame = base_link_frame
        self._tf_br = tf.TransformBroadcaster()
        self._last_published_walltime = rospy.Time(0)
        self._last_map_pub_time = rospy.Time(0)
        self._map_pub_interval = rospy.Duration(10)
        self._last_published_timestamp = rospy.Time(0)


    def publish(
            self, timestamp : rospy.Time,
            robot_position : np.ndarray, particles : np.ndarray
        ):
        walltime = rospy.Time.from_sec(time.time())
        if walltime <= self._last_published_walltime:
            return
        if (
            walltime - self._last_map_pub_time >= self._map_pub_interval and
            self._map is not None
        ):
            self._map.header.stamp = walltime
            self._map_pub.publish(self._map)
            self._last_map_pub_time = walltime


        robot_ps = self.make_stamped(
            self.particle_to_pose(robot_position),
            walltime, self._map_frame
        )
        self._robot_pos_pub.publish(
            robot_ps
        )

        self._particle_pub.publish(self.convert_particles(particles, walltime))
        self._last_published_walltime = walltime

        if timestamp <= self._last_published_timestamp:
            return
        try:
            self.publish_robot_tf(robot_position, timestamp)
            self._last_published_timestamp = timestamp
        # if no robot yet, simply don't publish to odom
        except tf2.LookupException: # type: ignore
            pass
        except tf2.ExtrapolationException as e: # type: ignore
            rospy.logwarn(f'CVL: RVIZ_PUB: {e}')



    def publish_robot_tf(self, robot_particle : np.ndarray, timestamp : rospy.Time):
        odom_bl_tf : TransformStamped = self._listener._buffer.lookup_transform(
            self._odom_frame,
            self._base_link_frame,
            timestamp
        )
        _, q = unpack_ts(odom_bl_tf)
        angles = [0., 0., angle_z_from_quaternion(q)]
        odom_bl_mat = tr.compose_matrix(
            translate=[odom_bl_tf.transform.translation.x,
                         odom_bl_tf.transform.translation.y,
                         odom_bl_tf.transform.translation.z],
            angles=angles
        )

        map_bl_mat = tr.compose_matrix(
            translate = [robot_particle[0], robot_particle[1], 0.],
            angles = [0., 0., robot_particle[2]]
        )

        map_odom_mat = np.dot(map_bl_mat, np.linalg.inv(odom_bl_mat))

        _, _, angles, trans, _ = tr.decompose_matrix(map_odom_mat)

        angle_z = angles[2]

        q = quaternion_from_angle_z(angle_z)
        # skill issue
        q_msg = Quaternion(
            **dict(zip(["x", "y", "z", "w"], q))
        )
        tfs = TransformStamped()

        tfs.header.stamp = timestamp
        tfs.header.frame_id = self._map_frame
        tfs.child_frame_id = self._odom_frame
        tfs.transform.translation.x = trans[0]
        tfs.transform.translation.y = trans[1]
        tfs.transform.translation.z = trans[2]
        tfs.transform.rotation = q_msg

        self._tf_br.sendTransformMessage(tfs)


    def particle_to_pose(self, particle : np.ndarray) -> Pose:
        position = particle[:2]
        quat = quaternion_from_angle_z(particle[2])
        pose = Pose(
            position=Point(x=position[0], y=position[1], z=0.1),
            orientation=Quaternion(x=quat[0], y=quat[1], z=quat[2], w=quat[3])
        )
        return pose


    def make_stamped(self, pose : Pose, timestamp : rospy.Time, frame : str):
        pose_stamped_msg = PoseStamped(
            header=Header(
                stamp=timestamp,
                frame_id=frame
            ),
            pose=pose
        )
        return pose_stamped_msg


    def make_transform_stamped(self, ps : PoseStamped, child_frame : str):
        ts = TransformStamped(
            header=ps.header,
            child_frame_id=child_frame,
            transform=Transform(
                translation=Vector3(
                    ps.pose.position.x,
                    ps.pose.position.y,
                    ps.pose.position.z
                ),
                rotation=ps.pose.orientation
            )
        )
        return ts


    def convert_particles(self, particles : np.ndarray, timestamp : rospy.Time) -> PoseArray:
        particle_msg = PoseArray()
        particle_msg.header.frame_id = self._map_frame
        particle_msg.header.stamp = timestamp
        particle_msg.poses = []
        for particle in particles:
            pose = self.particle_to_pose(particle)
            particle_msg.poses.append(pose)
        return particle_msg


    def convert_map(self, map : SatelliteImage, timestamp : rospy.Time) -> OccupancyGrid:
        # occup_ar = np.array(
        #     cv.cvtColor(map.img, cv.COLOR_BGR2GRAY).flatten() * (100 / 255),
        #     dtype=np.uint8
        # )
        gray = np.array(
            (100 / 255) * cv.cvtColor(map.img, cv.COLOR_BGR2GRAY), # type: ignore
            dtype=np.uint8
        )
        res = 1.
        cells_x = int(map.width / res)
        cells_y = int(map.height / res)
        gray_grid_shape : MatLike = cv.resize(gray, (cells_x, cells_y), interpolation=cv.INTER_NEAREST)
        gray_grid_shape = cv.flip(gray_grid_shape, 0)
        gray_grid_shape = cv.flip(gray_grid_shape, 1)
        gray_list = gray_grid_shape.flatten(order='F').tolist()
        origin_x = - (cells_x / 2) * res
        origin_y = - (cells_y / 2) * res
        map_msg = OccupancyGrid(
            header=Header(
                frame_id=self._map_frame,
                stamp=timestamp,
            ),
            info=MapMetaData(
                resolution = res,
                width=int(map.width),
                height=int(map.height),
                origin=Pose(
                    position=Point(x=origin_x, y=origin_y, z=0),
                    orientation=Quaternion(x=0, y=0, z=0, w=1)
                ),
            ),
            data=gray_list
        )

        return map_msg


