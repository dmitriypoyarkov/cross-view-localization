import sys

import pygame

import cross_view_localization.package_settings as settings

# Constants
WIDTH, HEIGHT = 800, 600
TOOL_PANEL_WIDTH = 200
CANVAS_WIDTH, CANVAS_HEIGHT = WIDTH - TOOL_PANEL_WIDTH, HEIGHT
ZOOM_INCREMENT = 1.1
MIN_ZOOM, MAX_ZOOM = 0.5, 4.0
MIN_BRUSH_SIZE, MAX_BRUSH_SIZE = 5, 50
BUTTON_HEIGHT = 30
BUTTON_WIDTH = TOOL_PANEL_WIDTH - 20

# Colors
WHITE = (255, 255, 255)
GRAY = (200, 200, 200)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
ERASER_COLOR = WHITE

class DrawingApp:
    def __init__(self, screen : pygame.Surface):
        self.screen = screen
        self.canvas = pygame.Surface((
            self.screen.get_width() - TOOL_PANEL_WIDTH,
            self.screen.get_height()
        ))
        self.canvas.fill(WHITE)
        self.overlay = None
        self.zoom_level = 1.0
        self.offset_x = 0
        self.offset_y = 0
        self.dragging = False
        self.drawing = False
        self.space_pressed = False
        self.drag_start_pos = (0, 0)
        self.brush_color = BLACK
        self.brush_size = 10

        self.history = [self.canvas.copy()]
        self.history_index = 0

        # Tool panel buttons
        self.color_buttons = [
            (RED, pygame.Rect(CANVAS_WIDTH + 10, 10, BUTTON_WIDTH, BUTTON_HEIGHT)),
            (GREEN, pygame.Rect(CANVAS_WIDTH + 10, 50, BUTTON_WIDTH, BUTTON_HEIGHT)),
            (BLUE, pygame.Rect(CANVAS_WIDTH + 10, 90, BUTTON_WIDTH, BUTTON_HEIGHT)),
        ]
        self.eraser_button = pygame.Rect(CANVAS_WIDTH + 10, 130, BUTTON_WIDTH, BUTTON_HEIGHT)
        self.save_button = pygame.Rect(CANVAS_WIDTH + 10, HEIGHT - 50, BUTTON_WIDTH, BUTTON_HEIGHT)
        self.brush_size_slider = pygame.Rect(CANVAS_WIDTH + 10, 170, BUTTON_WIDTH, BUTTON_HEIGHT)

        self.current_pos = (0, 0)

    def apply_overlay(self, overlay_image):
        self.overlay = overlay_image.convert_alpha()
        self.overlay.set_alpha(128)  # Set transparency to 50%

    def draw_tool_panel(self):
        pygame.draw.rect(self.screen, GRAY, (CANVAS_WIDTH, 0, TOOL_PANEL_WIDTH, HEIGHT))

        for color, rect in self.color_buttons:
            pygame.draw.rect(self.screen, color, rect)

        pygame.draw.rect(self.screen, BLACK, self.eraser_button)
        self.draw_text("Eraser", self.eraser_button, WHITE)

        pygame.draw.rect(self.screen, BLACK, self.save_button)
        self.draw_text("Save", self.save_button, WHITE)

        pygame.draw.rect(self.screen, BLACK, self.brush_size_slider)
        pygame.draw.rect(self.screen, WHITE, self.brush_size_slider.inflate(-4, -4))
        self.draw_text(f"Size: {self.brush_size}", self.brush_size_slider, BLACK)

    def draw_text(self, text, rect, color):
        font = pygame.font.Font(None, 24)
        text_surf = font.render(text, True, color)
        text_rect = text_surf.get_rect(center=rect.center)
        self.screen.blit(text_surf, text_rect)

    def draw_canvas(self):
        zoomed_canvas = pygame.transform.smoothscale(
            self.canvas,
            (int(CANVAS_WIDTH * self.zoom_level), int(CANVAS_HEIGHT * self.zoom_level))
        )
        self.screen.blit(zoomed_canvas, (self.offset_x, self.offset_y))

        if self.overlay:
            zoomed_overlay = pygame.transform.smoothscale(
                self.overlay,
                (int(CANVAS_WIDTH * self.zoom_level), int(CANVAS_HEIGHT * self.zoom_level))
            )
            self.screen.blit(zoomed_overlay, (self.offset_x, self.offset_y))

        self.draw_brush_cursor()

    def draw_brush_cursor(self):
        if self.space_pressed or self.drawing:
            return

        x, y = self.current_pos
        zoomed_brush_size = self.zoom_level * self.brush_size
        brush_rect = pygame.Rect(x, y, zoomed_brush_size, zoomed_brush_size)
# pygame.draw.rect(self.canvas, self.brush_color, (x, y, self.brush_size, self.brush_size))
        if self.brush_color == ERASER_COLOR:
            pygame.draw.rect(self.screen, BLACK, brush_rect, 1)
        else:
            pygame.draw.rect(self.screen, self.brush_color, brush_rect)

    def handle_events(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                self.space_pressed = True
                pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
            elif event.key == pygame.K_z and (pygame.key.get_mods() & pygame.KMOD_CTRL):
                self.undo()
            elif event.key == pygame.K_y and (pygame.key.get_mods() & pygame.KMOD_CTRL):
                self.redo()

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_SPACE:
                self.space_pressed = False
                pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)

        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                if self.space_pressed:
                    self.dragging = True
                    self.drag_start_pos = event.pos
                else:
                    self.check_tool_panel_click(event.pos)
                    self.drawing = True
                    self.draw(event.pos)
            elif event.button == 4:  # Scroll up
                self.zoom_in(event.pos)
            elif event.button == 5:  # Scroll down
                self.zoom_out(event.pos)

        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                if self.drawing:
                    self.save_history()
                self.dragging = False
                self.drawing = False

        elif event.type == pygame.MOUSEMOTION:
            self.current_pos = event.pos
            if self.dragging:
                dx = event.pos[0] - self.drag_start_pos[0]
                dy = event.pos[1] - self.drag_start_pos[1]
                self.offset_x += dx
                self.offset_y += dy
                self.drag_start_pos = event.pos
            elif self.drawing:
                self.draw(event.pos)

    def check_tool_panel_click(self, pos):
        for color, rect in self.color_buttons:
            if rect.collidepoint(pos):
                self.brush_color = color
                return

        if self.eraser_button.collidepoint(pos):
            self.brush_color = ERASER_COLOR
            return

        if self.save_button.collidepoint(pos):
            self.save_canvas()
            return

        if self.brush_size_slider.collidepoint(pos):
            relative_x = pos[0] - self.brush_size_slider.x
            self.brush_size = int(MIN_BRUSH_SIZE + (MAX_BRUSH_SIZE - MIN_BRUSH_SIZE) * (relative_x / BUTTON_WIDTH))
            return

    def draw(self, pos):
        x = (pos[0] - self.offset_x) / self.zoom_level
        y = (pos[1] - self.offset_y) / self.zoom_level
        pygame.draw.rect(self.canvas, self.brush_color, (x, y, self.brush_size, self.brush_size))

    def zoom_in(self, mouse_pos):
        self.zoom_level = min(self.zoom_level * ZOOM_INCREMENT, MAX_ZOOM)
        self.adjust_offset(mouse_pos)

    def zoom_out(self, mouse_pos):
        self.zoom_level = max(self.zoom_level / ZOOM_INCREMENT, MIN_ZOOM)
        self.adjust_offset(mouse_pos)

    def adjust_offset(self, mouse_pos):
        mx, my = mouse_pos
        cx, cy = mx - self.offset_x, my - self.offset_y
        cx /= self.zoom_level
        cy /= self.zoom_level
        self.offset_x = mx - cx * self.zoom_level
        self.offset_y = my - cy * self.zoom_level

    def save_history(self):
        if self.history_index < len(self.history) - 1:
            self.history = self.history[:self.history_index + 1]
        self.history.append(self.canvas.copy())
        self.history_index += 1

    def undo(self):
        if self.history_index > 0:
            self.history_index -= 1
            self.canvas = self.history[self.history_index].copy()

    def redo(self):
        if self.history_index < len(self.history) - 1:
            self.history_index += 1
            self.canvas = self.history[self.history_index].copy()

    def save_canvas(self):
        pygame.image.save(self.canvas, "drawing.png")

def main():
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Simple Drawing App")
    clock = pygame.time.Clock()

    app = DrawingApp(screen)

    # For testing: create a simple overlay image
    # overlay_image = pygame.Surface((CANVAS_WIDTH, CANVAS_HEIGHT), pygame.SRCALPHA)
    # overlay_image.fill((255, 0, 0, 128))  # Semi-transparent red overlay
    overlay_image = pygame.image.load(settings.area_path('building_190', 'sat_img.bmp'))
    app.apply_overlay(overlay_image)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            app.handle_events(event)

        screen.fill(BLACK)
        app.draw_canvas()
        app.draw_tool_panel()
        pygame.display.flip()
        clock.tick(60)

if __name__ == "__main__":
    main()
