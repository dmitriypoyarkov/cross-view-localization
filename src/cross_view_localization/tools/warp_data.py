from typing import List, Union

import cv2 as cv
import numpy as np
import rospy
from cv2.typing import MatLike
from cv_bridge import CvBridge
from numba import njit
from numba.pycc import CC
from tf.transformations import quaternion_matrix
from visualization_msgs.msg import Marker, MarkerArray

from cross_view_localization.tools.transorm_listener_extended import \
    TransformListenerExtended
from cross_view_localization.tools.utils import unpack_ts
from cross_view_localization.tools.warp_message_set import WarpMessageSet

cc = CC(__name__.split('.')[-1])
cc.verbose = True

@njit
@cc.export('pointcloud_flat_njit', 'f4[:](u1[:])')
def pointcloud_flat_njit(data : bytes):
    big_ar = np.frombuffer(data, np.float32)
    return big_ar

@njit
@cc.export('transform_pointcloud_njit', 'f4[:](f4[:], f4[:], f4[:])')
def transform_pointcloud_njit(
        pointcloud_flat : np.ndarray,
        trans : np.ndarray,
        rot : np.ndarray
    ):
    i = 0

    pointcloud_flat_world = np.zeros_like(pointcloud_flat)
    while i < pointcloud_flat.shape[0]:
        if not np.isnan(pointcloud_flat[i]):
            pointcloud_flat_world[i:i+3] = (
                trans + rot @ pointcloud_flat[i:i+3]
            )
        i += 4 # ???????????????????????
    return pointcloud_flat_world


class WarpData(object):
    '''Data necessary to perform a warp'''
    BRIDGE = CvBridge()
    def __init__(self, msg_set : WarpMessageSet,
            listener : TransformListenerExtended,
            central_frame : str):
        self._rgb_img = self.BRIDGE.imgmsg_to_cv2(
            msg_set.depth_rgb_img, 'rgb8'
        )
        self._depth = np.array(self.BRIDGE.imgmsg_to_cv2(
            msg_set.depth, 'passthrough'
        ))
        self._cam_frame = msg_set.cam_frame
        self._cam_K = np.array(msg_set.cam_info.K).reshape((3,3))
        self._trans, self._rot = listener.wait_and_lookup(
            central_frame, msg_set.frame, msg_set.timestamp
        )
        self._trans = np.array(self._trans)
        self._timestamp = msg_set.timestamp
        self._timestamp_depth = msg_set.timestamp_depth
        self._timestamp_rgb = msg_set.timestamp_rgb
        rot_matrix = np.array(quaternion_matrix(self._rot), dtype=np.float32)[:3, :3]
        self._segmented_img = None


    def generate_cube_grid(self, side_length, points_per_side):
        x, y, z = np.meshgrid(np.linspace(-side_length/2, side_length/2, points_per_side),
                            np.linspace(-side_length/2, side_length/2, points_per_side),
                            np.linspace(-side_length/2, side_length/2, points_per_side))
        coordinates = np.vstack([x.ravel(), y.ravel(), z.ravel(), np.zeros(points_per_side**3)]).T.flatten()
        return coordinates


    def generate_colors(self, side_length):
        num_points = side_length ** 3

        # Generate the grid of points
        x = np.linspace(0, side_length - 1, side_length)
        y = np.linspace(0, side_length - 1, side_length)
        z = np.linspace(0, side_length - 1, side_length)
        xx, yy, zz = np.meshgrid(x, y, z)
        points = np.vstack((xx.flatten(), yy.flatten(), zz.flatten(), np.zeros(num_points))).T.flatten()

        # Generate the color array
        colors = np.zeros((num_points, 4))

        # Define the colors for each face of the cube
        face_colors = [
            [1, 0, 0, 1],  # red
            [0, 1, 0, 1],  # green
            [0, 0, 1, 1],  # blue
            [1, 1, 0, 1],  # yellow
            [1, 0, 1, 1],  # magenta
            [0, 1, 1, 1],  # cyan
        ]

        # Assign colors to each point based on which face of the cube it belongs to
        for i in range(num_points):
            x, y, z, a = points[i*4:i*4+4]
            if x == 0:
                colors[i] = face_colors[0]
            elif x == side_length - 1:
                colors[i] = face_colors[1]
            elif y == 0:
                colors[i] = face_colors[2]
            elif y == side_length - 1:
                colors[i] = face_colors[3]
            elif z == 0:
                colors[i] = face_colors[4]
            elif z == side_length - 1:
                colors[i] = face_colors[5]
        return colors


    def publish_sphere_markers(self, points, colors, ns):
        marker_array = MarkerArray()


        # Iterate through points and colors and add sphere markers to the MarkerArray
        for i in range(0, len(points), 4):
            # Extract x, y, z coordinates from point array
            x, y, z = points[i:i+3]

            # Extract color values from color array
            r, g, b, a = colors[i//4]

            # Create Marker message for sphere
            sphere_marker = Marker()
            sphere_marker.header.frame_id = 'ritrover/base_link'
            # sphere_marker.header.frame_id = 'ritrover/oakd_fl_rgb_camera_frame'
            sphere_marker.header.stamp = rospy.Time.now()
            sphere_marker.ns = ns
            sphere_marker.id = i//4
            sphere_marker.type = Marker.SPHERE
            # sphere_marker.action = Marker.ADD
            sphere_marker.pose.position.x = x
            sphere_marker.pose.position.y = y
            sphere_marker.pose.position.z = z
            sphere_marker.scale.x = 0.1
            sphere_marker.scale.y = 0.1
            sphere_marker.scale.z = 0.1
            sphere_marker.color.r = r
            sphere_marker.color.g = g
            sphere_marker.color.b = b
            sphere_marker.color.a = a

            # Add sphere marker to MarkerArray
            marker_array.markers.append(sphere_marker)

        self.marker_pub.publish(marker_array)

    @property
    def trans(self):
        return self._trans

    @property
    def rot(self):
        return self._rot

    @property
    def depth(self):
        return self._depth

    @property
    def rgb_img(self):
        return self._rgb_img

    @property
    def cam_K(self):
        return self._cam_K

    @property
    def tf_trans_mat(self):
        return self._trans

    @property
    def tf_rot_mat(self):
        return self._rot

    @property
    def segmented_img(self):
        return self._segmented_img

    @segmented_img.setter
    def segmented_img(self, value : MatLike):
        self._segmented_img = value

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def timestamp_rgb(self):
        return self._timestamp_rgb

    @property
    def timestamp_depth(self):
        return self._timestamp_depth


class WarpDataSet(object):
    def __init__(self, warp_datas : List[WarpData]):
        self._warp_datas = warp_datas

    @property
    def warp_datas(self):
        return self._warp_datas

    @property
    def timestamp(self):
        return self._warp_datas[0].timestamp


    def set_segmented(self, segmented_imgs : List[MatLike]):
        if not segmented_imgs:
            return
        for i in range(len(self.warp_datas)):
            self.warp_datas[i].segmented_img = segmented_imgs[i]
        self.warp_datas[0].cam_K


class LandImg(object):
    def __init__(
        self,
        pixels_per_meter : float,
        colors_sorted : np.ndarray,
        img : Union[MatLike, None] = None,
        masks : Union[np.ndarray, None] = None,
    ):
        self._colors_sorted = colors_sorted
        if masks is not None:
            self.masks = masks
            self.combine_masks()
        elif img is not None:
            self._img = img
            self.masks = self.extract_masks(self._img)
        else:
            raise ValueError('Either masks or img should be not None')
        self._pixels_per_meter = pixels_per_meter
        if self._img.shape[0] != self._img.shape[1]:
            raise ValueError('Image has to be square')
        self._size = self._img.shape[0]

    @property
    def size(self):
        return self._size

    @property
    def img(self):
        return self._img

    @img.setter
    def img(self, value : MatLike):
        self._img = value

    @property
    def pixels_per_meter(self):
        return self._pixels_per_meter

    @property
    def colors_sorted(self):
        return self._colors_sorted

    @property
    def masks(self):
        raise AttributeError('write-only property!')

    @masks.setter
    def masks(self, value : np.ndarray):
        '''Setter uses the _colors_sorted field to construct new value'''
        if len(value) == 0:
            rospy.logerr('CVL: For some reason, new masks are empty')
            self._colormasks_dict = {}
            return
        self._mask_shape = value[0].shape
        for mask in value:
            if not np.all(mask.shape == self._mask_shape):
                raise ValueError('All masks should have equal shape!')

        self._colormasks_dict = {
            tuple(self._colors_sorted[i]): value[i]
            for i in range(len(self._colors_sorted))
        }


    def get_mask_by_color(self, color : np.ndarray):
        return self._colormasks_dict[tuple(color)]


    def set_mask(self, mask_color : np.ndarray, new_mask: np.ndarray):
        self._masks_updated = True
        if not np.all(new_mask.shape == self._mask_shape):
            raise ValueError(f'New mask must be same shape: {self._mask_shape}')
        self._colormasks_dict[tuple(mask_color)] = new_mask


    def combine_masks(self):
        new_img = np.zeros(
            (self._mask_shape[0], self._mask_shape[1], 3),
            dtype=np.uint8
        )

        for color in self._colors_sorted:
            mask = self.get_mask_by_color(color)
            new_img[mask == 255] = color
        self._img = new_img


    def extract_masks(self, img : MatLike):
        masks = np.array((len(self._colors_sorted), img.shape[0], img.shape[1]))

        # Iterate through each color in the color list
        for i, color in enumerate(self._colors_sorted):
            lower_bound = np.array(color)
            upper_bound = np.array(color)

            mask = cv.inRange(img, lower_bound, upper_bound)
            masks[i] = np.array(mask)
        return masks
