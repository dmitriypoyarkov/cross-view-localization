from typing import List

import genpy
import rospy


class TopicCollector(object):
    def __init__(self, topic_name : str, msg_type : type):
        self._topic_name = topic_name
        self._sub = rospy.Subscriber(
            topic_name,
            msg_type,
            self.collect_msg,
            queue_size=1,
        )
        self._msg_arr : List[genpy.Message] = []
        self._msg_arr_max_size = 10


    def get_nearest_msg(self, timestamp : rospy.Time):
        while len(self._msg_arr) == 0:
            rospy.sleep(.5)
        return min(self._msg_arr,
            key=lambda msg : msg.header.stamp - timestamp)


    def collect_msg(self, msg : genpy.Message):
        if len(self._msg_arr) > self._msg_arr_max_size:
            self._msg_arr.pop()
        self._msg_arr.append(msg)