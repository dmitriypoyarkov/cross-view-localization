import math

import cv2 as cv
import numpy as np
from cv2.typing import MatLike
from imutils import rotate_bound
from numba import njit
from numba.pycc import CC

from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.tools.warp_data import LandImg

cc = CC(__name__.split('.')[-1])
cc.verbose = True


SQRT_OF_2 = math.sqrt(2)


INNER_RADIUS = 0.6
OUTER_RADIUS = 1.
INNER_INCREASE = 1.3
OUTER_DECREASE = 0.7


def generate_factor_matrix(matrix_shape, INNER_RADIUS, OUTER_RADIUS, INNER_INCREASE, OUTER_DECREASE):
    # Calculate the center coordinates based on the matrix shape
    Cx, Cy = matrix_shape[1] / 2, matrix_shape[0] / 2

    # Create an array to store the function values
    values = np.zeros(matrix_shape)

    # Iterate through the coordinates and calculate the function values
    for i in range(matrix_shape[0]):
        for j in range(matrix_shape[1]):
            x = j - Cx
            y = i - Cy
            inner_term = (OUTER_DECREASE - INNER_INCREASE) / (INNER_RADIUS - OUTER_RADIUS)
            outer_term = INNER_RADIUS - (x**2 + y**2)
            values[i, j] = min(max(inner_term * outer_term + INNER_INCREASE, 0), 255)  # Clamp to [0, 255]

    return values

# FACTOR_MATRIX = generate_factor_matrix(100, 100, ) # TODO

definition = 'f4(u1[:, :, :], u1[:, :, :], u1, u1, f4, u4)'
@njit
@cc.export('compare_at_coord_inplace_njit', definition)
def compare_at_coord_inplace_njit(
        source_img : MatLike, img : MatLike,
        X, Y, phi, valued_pixels
    ):
    cy = img.shape[0] / 2
    cx = img.shape[1] / 2
    cos_value = np.cos(phi)
    sin_value = np.sin(phi)
    similarity = 0.
    for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            similarity += np.all(
                source_img[
                    int(Y + (x - cx) * sin_value + (y - cy) * cos_value),
                    int(X + (x - cx) * cos_value - (y - cy) * sin_value)
                ] == img[y, x]
            )
    matched_percent = similarity / valued_pixels
    return matched_percent
    normalized_nonlinear = 1 / (1 + ((1 - matched_percent) / 0.10) ** (2 * 0.63))
    return normalized_nonlinear


definition = 'f4(u1[:, :, :], u1[:, :, :], u1, u1, f4, u4)'
@njit
@cc.export('get_similarity_njit_debug', definition)
def get_similarity_njit_debug(source_img, img : MatLike,
        X, Y, phi, valued_pixels):
    cy = img.shape[0] / 2
    cx = img.shape[1] / 2
    cos_value = np.cos(phi)
    sin_value = np.sin(phi)
    similarity = 0.
    for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            similarity += np.all(
                source_img[
                    int(Y - (x - cx) * sin_value + (y - cy) * cos_value),
                    int(X + (x - cx) * cos_value + (y - cy) * sin_value)
                ] == img[y, x]
            )
    return similarity


class PreparedSampleImg(object):
    def __init__(self, sample_img : LandImg, pixels_per_meter : float):
        self._orig_img = sample_img
        self._img = self.align_sample_size(sample_img, pixels_per_meter)
        self._valued_pixels = self.count_valued_pixels(self._img)

    @property
    def img(self):
        return self._img

    @property
    def valued_pixels(self):
        return self._valued_pixels


    def align_sample_size(self, sample_img : LandImg, pixels_per_meter : float):
        factor = pixels_per_meter / sample_img.pixels_per_meter
        result_sample_size = int(sample_img.size * factor)

        aligned_img = cv.resize(
            sample_img.img, (result_sample_size, result_sample_size),
            interpolation=cv.INTER_NEAREST
        )
        return aligned_img


    def count_valued_pixels(self, img : MatLike):
        return np.sum(img != 0)


class SatelliteImageProvider(object):
    def __init__(self, sat_img : SatelliteImage):
        self._img = sat_img.img
        self._meters_per_pixel = sat_img.height/sat_img.img.shape[0]
        self._pixels_per_meter = 1 / self._meters_per_pixel
        self._img_center_world = np.array([sat_img.center_x, sat_img.center_y])

        self._valued_pixels_threshold = 400


    @property
    def img_shape(self):
        return self._img.shape

    @property
    def meters_per_pixel(self):
        return self._meters_per_pixel

    @property
    def pixels_per_meter(self):
        return self._pixels_per_meter

    @property
    def img_center_x(self):
        return self._img.shape[1]/2

    @property
    def img_center_y(self):
        return self._img.shape[0]/2


    @property
    def sat_img(self):
        return self._img


    def set_sat_img(self, sat_img : SatelliteImage):
        self._img = sat_img.img


    # Overlap img over satellite_img and compare using the comparison function.
    # img is required to be centered. phi in radians.
    # def compare_at_coord(
    #         self,
    #         rotated_img : cv.Mat,
    #         x : int, y : int,
    #         colors, color_amounts : np.ndarray = None,
    #         valued_pixels_count : int = None
    #     ):
    #     # rotated_img : cv.Mat = rotate_bound(img, -np.rad2deg(phi))
    #     bounds = self.bounds_for_rect(
    #         x, y, rotated_img.shape,
    #         self.img_shape
    #     )
    #     piece_from_satellite = self._img[
    #         bounds[0]:bounds[1],
    #         bounds[2]:bounds[3]
    #     ]
    #     return self._comparer.compare_img(
    #         rotated_img, piece_from_satellite,
    #         colors,
    #         color_amounts,
    #         valued_pixels_count
    #     )


    def cut_rect_from_satellite_img(self, x : int, y : int, shape : tuple,
            source : np.ndarray):
        bounds = self.bounds_for_rect(
            x, y, shape,
            source.shape
        )
        return source[
            bounds[0]:bounds[1],
            bounds[2]:bounds[3]
        ]


    def cut_rotated_from_satellite_img(self, x : int, y : int, phi : float,
            shape : tuple):
        bigger_side_length_increased = math.ceil(
            max(shape[0], shape[1]) * SQRT_OF_2
        )
        square_increased_from_satellite = self.cut_rect_from_satellite_img(
            x, y,
            (bigger_side_length_increased,
            bigger_side_length_increased),
            self._img
        )
        full_image_unrotated = self.cut_rect_from_satellite_img(
            x, y, square_increased_from_satellite.shape,
            self._img
        )
        full_image_rotated = cv.Mat(rotate_bound(
            full_image_unrotated, np.rad2deg(phi)
        ))
        return full_image_rotated


    def bounds_for_rect(self, x : int, y : int,
            shape_target : tuple, shape_source : tuple):
        bounds = [math.ceil(y - shape_target[0] / 2), math.ceil(y + shape_target[0] / 2),
                  math.ceil(x - shape_target[1] / 2), math.ceil(x + shape_target[1] / 2)]
        if (bounds[0] < 0 or bounds[1] > shape_source[0] or
                bounds[2] < 0 or bounds[3] > shape_source[1]):
            raise ValueError(f'Too close or out of bounds: {bounds}. Shape source: {shape_source}')
        return bounds


    # def align_sample_size(self, sample : MatLike, pixels_per_meter : float):

    #     factor = self.pixels_per_meter / pixels_per_meter
    #     print('FACTOR:', factor, self.pixels_per_meter, pixels_per_meter )
    #     # Calculate the new dimensions of the image
    #     new_height = int(sample.shape[0] * factor)
    #     new_width = int(sample.shape[1] * factor)

    #     # Resize the image using nearest neighbor interpolation
    #     sample = cv.resize(sample, (new_width, new_height), interpolation=cv.INTER_NEAREST)
    #     return sample


    def get_matched_pixels(self, img, x, y, phi):
        rotated_img = cv.Mat(rotate_bound(img, -np.rad2deg(phi)))

        bounds = self.bounds_for_rect(
            x, y, rotated_img.shape,
            self.img_shape
        )
        piece_from_satellite = self._img[
            bounds[0]:bounds[1],
            bounds[2]:bounds[3]
        ]
        # TODO: do something
        # return self._comparer.matched_pixels(rotated_img, piece_from_satellite)


    def rotate_sample(self, img : cv.Mat, phi : float):
        return rotate_bound(img, -np.rad2deg(phi))

    # Returns [x, y, phi] as np.ndarray
    def convert_world_to_img(self, coord : np.ndarray):
        coord_img = np.array([
            -(coord[1] - self._img_center_world[1]) * self._pixels_per_meter + self.img_center_y,
            -(coord[0] - self._img_center_world[0]) * self._pixels_per_meter + self.img_center_x,
            coord[2]
        ])
        return coord_img


    # def get_real_pose_img(self, timestamp : rospy.Time):
    #     robot_trans, robot_rot = self._odometry_provider.get_real_pose(timestamp)
    #     img_robot_trans = self.convert_world_to_img(
    #         robot_trans
    #     )
    #     img_robot_phi = angle_z_from_quaternion(robot_rot)
    #     rospy.logdebug(f'ROBOT REAL POSE: {robot_trans, img_robot_phi}')
    #     rospy.logdebug(f'ROBOT IMG POSE: {img_robot_trans, img_robot_phi}')
    #     return img_robot_trans, img_robot_phi


    # def get_odometry_step_img(self, timestamp : rospy.Time):
    #     real_step = self._odometry_provider.get_odometry_step(timestamp)
    #     return np.array([
    #         real_step[0] * self.pixels_per_meter,
    #         real_step[1]
    #     ])


    def compare_at_coord_inplace(self, X, Y, phi):
        if self._prepared_img.valued_pixels <= self._valued_pixels_threshold:
            return 0.

        return compare_at_coord_inplace_njit(
            self.sat_img, self._prepared_img.img,
            X, Y, phi, self._prepared_img.valued_pixels
        )


    def get_similarity_inplace(self, img : cv.Mat,
            X, Y, phi, valued_pixels):
        return get_similarity_njit_debug(
            self._img, img, X, Y, phi, valued_pixels
        )


    def odometry_img_to_real(self, odometry_step_img : np.ndarray):
        return np.array([
            self.meters_per_pixel * odometry_step_img[0],
            odometry_step_img[1]
        ])


    def odometry_real_to_img(self, odometry_step : np.ndarray):
        return np.array([
            self.pixels_per_meter * odometry_step[0],
            odometry_step[1]
        ])


    def img_to_real(self, coord : np.ndarray):
        return self.convert_img_to_world(coord)


    def real_to_img(self, coord):
        return self.convert_world_to_img(coord)


    def convert_img_to_world(self, coord_img: np.ndarray):
        coord_world = np.array([
            (self.img_center_x - coord_img[1]) / self.pixels_per_meter + self._img_center_world[1],
            (self.img_center_y - coord_img[0]) / self.pixels_per_meter + self._img_center_world[0],
            coord_img[2]
        ], dtype=np.float64)
        return coord_world


    def prepare_img(self, sample_img : LandImg):
        self._prepared_img = PreparedSampleImg(sample_img, self._pixels_per_meter)
