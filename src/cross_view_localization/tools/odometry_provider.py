import copy
import math
import time

import numpy as np
import rospy
import tf2_ros as tf2
from rospy import Duration
from tf.transformations import quaternion_conjugate, quaternion_multiply

from cross_view_localization.decoration import deco
from cross_view_localization.tools.transorm_listener_extended import \
    TransformListenerExtended
from cross_view_localization.tools.utils import (angle_between_vectors,
                                                 angle_z_from_quaternion,
                                                 angle_z_from_quaternion_img,
                                                 rotate_pt,
                                                 vector_from_angle_z_world)


class OdometryProvider(object):
    def __init__(self, listener : TransformListenerExtended,
            odom_frame : str, robot_frame : str,
            real_pos_frame : str,
            odometry_sim_error = [0.0, 0.0],
            simulate_odometry = False,
            odom_factor = 1.0):
        self._listener = listener
        self._odom_frame = odom_frame
        self._robot_frame = robot_frame
        self._real_pos_frame = real_pos_frame
        self.LOOKUP_TOLERANCE = 0.4

        self._cur_pose = None
        self._prev_pose = None
        self._odometry_sim_error = odometry_sim_error
        self._simulate_odometry = simulate_odometry
        self._odom_factor = odom_factor
        self._odom_init_pos = None


    def collect_odometry_steps(self):
        self._last_odom_pos_tf = None
        self._last_odom_stamp_tf = None
        self._last_odom_phi_tf = None
        self._odom_steps_tf = []
        self._odom_times_tf = []
        rospy.Timer(Duration(0.2), self.get_new_odom_step_tf) # type: ignore


    # def get_new_odom_step_tf(self, event):
    #     self._last_odom_pos_tf, step = self.calculate_odom_step(
    #         self._last_odom_pos_tf, rospy.Time(0), self._real_pos_frame
    #     )
    #     trans = self._last_odom_pos_tf[0][:2]
    #     self._odom_steps_tf.append([trans[0], trans[1]])
    #     self._odom_times_tf.append(rospy.Time.now().to_nsec())


    def simulate_odometry_step(self, timestamp : rospy.Time):
        ideal_step = self.get_odom_step_main(timestamp, self._real_pos_frame)
        step_with_error = np.array([
            np.random.normal(ideal_step[0], self._odometry_sim_error[0] * ideal_step[0]),
            np.random.normal(ideal_step[1], self._odometry_sim_error[1] * np.abs(ideal_step[1])),
        ])
        print(f'WITH ERRROR: {step_with_error}, {self._odometry_sim_error[0]}')
        return step_with_error


    def true_odometry_step(self, timestamp : rospy.Time):
        return self.get_odom_step_main(timestamp, self._odom_frame)


    def accept_last_step(self):
        self._prev_pose = copy.deepcopy(self._cur_pose)
        self._cur_pose = self._last_pose


    def calculate_odom_step(
            self, prev_pose, cur_pose, next_stamp, source
        ):
        new_pose = self._listener.lookup_with_tolerance(
            source, self._robot_frame, next_stamp, self.LOOKUP_TOLERANCE
        )
        new_trans, new_rot = new_pose
        if cur_pose is None:
            cur_pose = new_pose
            return new_pose, np.array([0., 0.])
        if prev_pose is None:
            prev_pose = cur_pose

        cur_trans, cur_rot = cur_pose
        prev_trans, prev_rot = prev_pose
        move_vector = np.array([
            new_trans[i] - cur_trans[i] for i in range(2)
        ])
        # cur_angle = angle_z_from_quaternion(cur_rot)
        # new_angle = angle_z_from_quaternion_img(new_rot)
        # print(f'CUR ANGLE {cur_angle} NEW_ANGLE {new_angle}')
        # turn_angle = new_angle - cur_angle

        # forward_vector = vector_from_angle_z_world(cur_angle)
        forward_vector = np.array([
            cur_trans[i] - prev_trans[i] for i in range(2)
        ])

        turn_angle = angle_between_vectors(forward_vector, move_vector)

        is_forward = np.abs(turn_angle) <= np.pi / 2
        if not is_forward:
            if turn_angle > 0:
                turn_angle -= np.pi
            else:
                turn_angle += np.pi

        # is_forward = np.sign(np.dot(move_vector, forward_vector))
        if not is_forward:
            print(f'BACK: {move_vector}, {forward_vector}')
        # diff_q = quaternion_multiply(new_rot, quaternion_conjugate(cur_rot))
        # turn_angle = angle_z_from_quaternion(diff_q)
        distance = is_forward * np.linalg.norm(move_vector)
        # print(f'MOVE {move_vector}, FORWARD {forward_vector}, DOT {np.dot(move_vector, forward_vector)}')

        return new_pose, np.array([distance * self._odom_factor, turn_angle])


    def get_odom_step_main(self,
            timestamp : rospy.Time,
            source : str):
        '''Returns the [d, phi].'''
        self._last_pose, step = self.calculate_odom_step(
            self._prev_pose, self._cur_pose, timestamp, source
        )
        return step


    def get_odom_pose(self, timestamp : rospy.Time):
        new_trans, new_rot = self._listener.lookup_with_tolerance(
            self._odom_frame, self._robot_frame, timestamp, self.LOOKUP_TOLERANCE
        )
        phi = angle_z_from_quaternion(np.array(new_rot))
        coord = np.array([new_trans[0], new_trans[1], phi])
        return coord


    def get_odom_pose_rel(self, timestamp : rospy.Time):
        coord = self.get_odom_pose(timestamp)
        if self._odom_init_pos is None:
            self._odom_init_pos = coord
        pt = coord[:2] - self._odom_init_pos[:2]
        pt_rel = rotate_pt(
            pt, np.array([0., 0.]),
            -self._odom_init_pos[2]
        )
        phi = coord[2] - self._odom_init_pos[2]
        return [pt_rel[0], pt_rel[1], phi]


    def determine_movement_direction(self, old_x, old_y, new_x, new_y, robot_angle):
        # Calculate the vector from old position to new position
        delta_x = new_x - old_x
        delta_y = new_y - old_y

        # Calculate the angle between the robot's orientation and the movement vector
        angle_diff = math.atan2(delta_x, delta_y) - math.pi/2 - robot_angle
        print(f'ANGLE_DIFF {angle_diff} ROBOT_ANGLE {robot_angle}')

        # Normalize the angle to be in the range [-pi, pi]
        while angle_diff > math.pi:
            angle_diff -= 2 * math.pi
        while angle_diff < -math.pi:
            angle_diff += 2 * math.pi

        # Determine the movement direction based on the angle difference
        if abs(angle_diff) < math.pi / 2:
            return 1
        else:
            return -1


    def get_odometry_step(self, timestamp : rospy.Time):
        if self._simulate_odometry:
            return self.simulate_odometry_step(timestamp)
        return self.true_odometry_step(timestamp)


    def wait_for_tf(self):
        anim = deco.WaitAnimation(
            'CVL: WAITING TRANSFORM FROM ' +
            f'{self._odom_frame} TO {self._robot_frame}'
        )
        while not rospy.is_shutdown():
            try:
                ts = self._listener.getLatestCommonTime(self._robot_frame, self._odom_frame)
                self._listener.lookupTransform(
                    self._robot_frame, self._odom_frame, ts
                )
                self._odom_init_pos = self.get_odom_pose(ts)
                break
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException) as e: # type: ignore
                anim.update(
                    f'CVL: LOOKUP ERROR: {e}\n' +
                    'CVL: WAITING TRANSFORM FROM ' +
                    f'{self._odom_frame} TO {self._robot_frame}'
                )
                time.sleep(2.0)
                # rospy.logdebug(f'CVL: LOOKUP ERROR: {e}')
                continue
        anim.stop()
        rospy.loginfo('CVL: TRANSFORM AVAILABLE')