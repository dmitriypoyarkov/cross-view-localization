import time

import cv2 as cv
import numpy as np
import torch
from cv2.typing import MatLike
from PIL import Image
from torchvision.transforms.transforms import Compose
from transformers import pipeline

# result = pipe("http://images.cocodataset.org/val2017/000000039769.jpg")
# result["depth"]


class DepthMaker(object):
    def __init__(self):
        self._model_type = 'DPT_SwinV2_T_256'
        # self._model = torch.hub.load('intel-isl/MiDaS', self._model_type)
        # transforms = torch.hub.load("intel-isl/MiDaS", "transforms")
        # self._transform : Compose = transforms.swin256_transform

        self._pipe = pipeline(
            task="depth-estimation", model="Intel/dpt-swinv2-tiny-256",
            device='cuda'
        )


    # def load_model_to_device(self, device='cuda'):
    #     self._device = torch.device('cuda')
    #     self._model.to(device)
    #     self._model.eval()


    def process_frame(self, frame : MatLike):
        # input_img = self._transform(frame).to(self._device)
        pil_frame = Image.fromarray(frame)
        res = self._pipe(pil_frame)
        depth_img = res['depth'] # type: ignore
        depth_ar = np.array(depth_img)
        depth_img = cv.normalize( # type: ignore
            depth_ar, None, 0, 1, # type: ignore
            norm_type = cv.NORM_MINMAX,
            dtype = cv.CV_64F
        )
        return depth_img

        # with torch.no_grad():
        #     pred = self._model(input_img)
        #     pred = torch.nn.functional.interpolate(
        #         pred.unsqueeze(1),
        #         size = frame.shape[:2],
        #         mode = "bicubic",
        #         align_corners = False,
        #     ).squeeze()
        #     depth_img = pred.cpu().numpy()
        #     depth_img = cv.normalize( # type: ignore
        #         depth_img, None, 0, 1, # type: ignore
        #         norm_type = cv.NORM_MINMAX,
        #         dtype = cv.CV_64F
        #     )
        # return depth_img


    def process_and_calibrate(self, frame : MatLike, ref_depth : MatLike):
        proc = self.process_frame(frame)
        cal = self.calibrate_depth(proc, ref_depth)
        return cal


    def process_frame_vis(self, frame):
        depth_img = self.process_frame(frame)
        depth_img = (depth_img*255).astype(np.uint8)
        depth_img = cv.applyColorMap(depth_img, cv.COLORMAP_MAGMA)
        return depth_img


    def calibrate_depth(self, depth_img : np.ndarray, ref_img : np.ndarray):
        ref_points = (3, 5)

        upper_bound = ref_img.shape[0] // 2
        height = ref_img.shape[0] - upper_bound
        offset = (height // 5, ref_img.shape[1] // 5)
        size = (height - 2 * offset[0], ref_img.shape[1] - 2 * offset[1])
        step = (size[0] / ref_points[0], size[1] / ref_points[1])

        ref_list = []
        depth_list = []
        # TODO: deal with case when too little ref data
        for y_i in range(ref_points[0]):
            for x_i in range(ref_points[1]):
                y = int(offset[0] + upper_bound + step[0] * y_i)
                x = int(offset[1] + step[1] * x_i)
                drop_pixel = False
                while (not drop_pixel) and (np.isnan(ref_img[y, x]) or ref_img[y, x] == 0):
                    x += 1
                    y += 1
                    if x >= ref_img.shape[1] or y >= ref_img.shape[0]:
                        drop_pixel = True
                if drop_pixel:
                    continue
                ref_list.append([1/ref_img[y, x]])
                depth_list.append([depth_img[y, x]])
        # print(f'ref and depth: {ref_list}, {depth_list}')

        D = np.hstack((depth_list, np.ones_like(depth_list)))
        A, _, _, _ = np.linalg.lstsq(D, ref_list, rcond=None)  # solve for A using least squares
        scale : float = A[0][0]
        shift : float = A[1][0]
        # print(f'scale and shift {scale, shift}')
        inverse_depth = depth_img
        # inverse_depth[np.isnan(inverse_depth)] = np.inf
        # inverse_depth[inverse_depth <= 0] = np.inf
        depth_meters = 1/(shift + scale * inverse_depth)
        # print(ref_img.shape)

        # print(f'max {np.max(depth_meters), np.min(depth_meters), np.mean(depth_meters)}')
        depth_meters[depth_meters>40] = 0
        depth_meters[depth_meters<=0] = 0
        # calibrated_depth = 255 * cv.normalize(depth_meters, None)

        # residuals = np.array(ref_list) - np.dot(D, A)

        # # Calculate z-scores
        # mean_residual = np.mean(residuals)
        # std_residual = np.std(residuals)
        # z_scores = np.abs((residuals - mean_residual) / std_residual)

        # # Set threshold for outliers
        # threshold = 2  # or any other threshold you choose

        # # Find indices of potential outliers
        # outlier_indices = np.where(z_scores > threshold)[0]

        # print("Potential outliers:", residuals)
        return depth_meters


if __name__ == '__main__':
    import cross_view_localization.package_settings as settings
    maker = DepthMaker()
    # maker.load_model_to_device()
    for i in range(3):
        img = cv.imread(settings.output_path(f'saved_pipelines/pipeline_5/rgb_{i}.bmp'))
        depth = np.load(
            settings.output_path(f'saved_pipelines/pipeline_5/depth_{i}.npy')
        )
        # depth = maker.process_and_calibrate(img, depth)
        depth = maker.process_frame(img)
        cv.imshow('hf', depth)
        cv.waitKey(0)
        cv.destroyAllWindows()