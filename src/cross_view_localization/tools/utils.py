import math

import numpy as np
from geometry_msgs.msg import TransformStamped
from numba import njit
from numba.pycc import CC
from scipy.spatial.transform import Rotation

cc = CC(__name__.split('.')[-1])
cc.verbose = True

X_N = 0
Y_N = 1
Z_N = 2

@njit
@cc.export('determine_area_njit', 'f4[:](f4[:])')
def determine_area_njit(pointcloud_flat : np.ndarray):
    visible_area_shape = np.array([
        pointcloud_flat[0], # right
        pointcloud_flat[0], # left
        pointcloud_flat[1], # top
        pointcloud_flat[1], # bottom
    ])
    # In case the first coord is nan
    i = 0
    while np.isnan(visible_area_shape).any():
        i += 4
        visible_area_shape = np.array([
            pointcloud_flat[i + 0], # right
            pointcloud_flat[i + 0], # left
            pointcloud_flat[i + 1], # top
            pointcloud_flat[i + 1], # bottom
        ])
    i = 0
    while i < pointcloud_flat.shape[0]:
        if not np.isnan(pointcloud_flat[i]):
            if pointcloud_flat[i + X_N] > visible_area_shape[0]:
                visible_area_shape[0] = pointcloud_flat[i + X_N]
            elif pointcloud_flat[i + X_N] < visible_area_shape[1]:
                visible_area_shape[1] = pointcloud_flat[i + X_N]
            if pointcloud_flat[i + Y_N] > visible_area_shape[2]:
                visible_area_shape[2] = pointcloud_flat[i + Y_N]
            elif pointcloud_flat[i + Y_N] < visible_area_shape[3]:
                visible_area_shape[3] = pointcloud_flat[i + Y_N]
        i += 4

    return visible_area_shape


def angle_between_vectors(u, v):
    if np.all(u == np.array([0., 0.])) or np.all(v == np.array([0., 0.])):
        return 0.
    theta = -(math.atan2(v[1], v[0]) - math.atan2(u[1], u[0]))
    if theta <= -np.pi:
        theta += 2 * np.pi
    elif theta > np.pi:
        theta -= 2 * np.pi
    return theta


# Returns positive angle for clockwise rotations
# From -pi to pi.
def angle_z_from_quaternion(q : np.ndarray):
    v0 = np.array([1, 0, 0])
    rot = Rotation.from_quat(q)
    v1 = rot.apply(v0)

    v0_xy = np.array([v0[0], v0[1], 0])
    v1_xy = np.array([v1[0], v1[1], 0])

    v0_xy_norm = v0_xy / np.linalg.norm(v0_xy)
    v1_xy_norm = v1_xy / np.linalg.norm(v1_xy)

    # [-pi, pi]
    angle = np.arccos(np.clip(np.dot(v0_xy_norm, v1_xy_norm), -1.0, 1.0))

    cross = np.cross(v0_xy_norm, v1_xy_norm)
    if cross[2] < 0:
        angle = -angle
    return angle


def angle_z_from_quaternion_img(q):
    v0 = np.array([1, 0, 0])
    rot = Rotation.from_quat(q)
    v1 = rot.apply(v0)

    v0_xy = np.array([v0[0], v0[1]])
    v1_xy = np.array([v1[0], v1[1]])

    return angle_between_vectors(v0_xy, v1_xy)

    v0_xy_norm = v0_xy / np.linalg.norm(v0_xy)
    v1_xy_norm = v1_xy / np.linalg.norm(v1_xy)

    # [-pi, pi]
    angle = np.arccos(np.clip(np.dot(v0_xy_norm, v1_xy_norm), -1.0, 1.0))

    cross = np.cross(v0_xy_norm, v1_xy_norm)
    if cross[2] < 0:
        angle = -angle
    return angle


def quaternion_from_angle_z(angle : float):
    '''Returns ndarray as follows: [x, y, z, w]'''
    rot = Rotation.from_euler('z', angle, degrees=False).as_quat()
    return rot


def unpack_ts(ts : TransformStamped):
    '''Returns (trans, rot) tuple'''
    trans = np.array([ts.transform.translation.x,
                      ts.transform.translation.y,
                      ts.transform.translation.z])
    rot = np.array([ts.transform.rotation.x,
                    ts.transform.rotation.y,
                    ts.transform.rotation.z,
                    ts.transform.rotation.w])
    return trans, rot


def vector_from_angle_z_img(angle_z : float):
    ''' Returns [0, -1] (considered up in img space) for zero angle.'''
    v_rotated = np.array([np.sin(angle_z), -np.cos(angle_z)])
    return v_rotated


def vector_from_angle_z_world(angle_z : float):
    ''' Returns [1, 0] (considered up in world space) for zero angle.'''
    v_rotated = np.array([np.cos(angle_z), np.sin(angle_z)])
    return v_rotated


def angle_z_from_vector_img(vector_img : np.ndarray):
    up = np.array([0., -1.])
    v_norm = vector_img / np.linalg.norm(vector_img)
    dot = np.dot(up, v_norm)
    cross = up[0] * v_norm[1] - up[1] * v_norm[0]
    phi = np.arctan2(cross, dot)
    if phi > np.pi:
        phi -= 2 * np.pi
    elif phi < - np.pi:
        phi += 2 * np.pi
    return phi


def rotate_pt(
        point : np.ndarray, center : np.ndarray,
        angle_rad : float
    ):
    """ Rotate a point around a center by a given angle in radians. """
    px, py = point
    cx, cy = center
    cos = np.cos(angle_rad)
    sin = np.sin(angle_rad)

    tr_x = px - cx
    tr_y = py - cy
    r_x = tr_x * cos - tr_y * sin
    r_y = tr_x * sin + tr_y * cos
    new_x = r_x + cx
    new_y = r_y + cy

    return np.array([new_x, new_y])