import sys

import rospy
import tf
import tf2_ros


class TransformListenerExtended(tf.TransformListener):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.WAIT_TIMEOUT = rospy.Duration(10)


    def wait_and_lookup(self, target_frame : str, source_frame : str, time : rospy.Time):
        while True:
            try:
                self.waitForTransform(target_frame, source_frame, time, self.WAIT_TIMEOUT)
                break
            except tf2_ros.TransformException as e: # type: ignore
                # Lookup would require extrapolation
                if 'past' in str(e):
                    rospy.logwarn(f'Late for transformLookup. Skipping')
                    raise e
                rospy.logerr(f'waitForTransform error: {e}')

        return self.lookupTransform(target_frame, source_frame, time)


    def lookup_with_tolerance(self, target_frame, source_frame, time : rospy.Time, tolerance : float):
        if time == rospy.Time(0):
            print(f'REQUESTED LATEST TIME {time}')
            time = self.getLatestCommonTime(source_frame, target_frame)
            print(f'REQUESTED LATEST {time}')
        next_stamp = time
        tol = rospy.Duration(tolerance) # type: ignore
        while next_stamp <= time + tol:
            try:
                return self.lookupTransform(target_frame, source_frame, next_stamp)
            except tf2_ros.TransformException as e: # type: ignore
                rospy.logdebug(str(e))
                rospy.logdebug('Trying...')
                next_stamp += rospy.Duration(0.05) # type: ignore
                continue
        raise RuntimeError(
            'TransformListenerExtended: Could not lookup' +
            f'{target_frame, source_frame} on time {time} with tolerance {tolerance}'
        )