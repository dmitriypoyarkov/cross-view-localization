from typing import Callable

import numpy as np
import rospy

from cross_view_localization.plugins.cvl_comp_plugin import CVLPoseConverter
from cross_view_localization.tools.transorm_listener_extended import \
    TransformListenerExtended
from cross_view_localization.tools.utils import angle_z_from_quaternion


class RobotPoseProvider(object):
    def __init__(
            self,
            real_pos_frame : str,
            robot_frame : str,
            tf_listener : TransformListenerExtended,
            pose_converter : CVLPoseConverter,
        ):
        self._converter = pose_converter
        self._real_pos_frame = real_pos_frame
        self._robot_frame = robot_frame
        self._listener = tf_listener


    def get_pose_real(self, timestamp : rospy.Time):
        new_trans, new_rot = self._listener.wait_and_lookup(
            self._real_pos_frame, self._robot_frame, timestamp
        )
        phi = angle_z_from_quaternion(np.array(new_rot))
        coord = np.array([new_trans[0], new_trans[1], phi])
        return coord


    def get_pose_img(self, timestamp):
        return self._converter.real_to_img(self.get_pose_real(timestamp))