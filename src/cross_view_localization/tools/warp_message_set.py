from typing import List

import rospy
from sensor_msgs.msg import CameraInfo, Image, PointCloud2


class WarpMessageSet(object):
    def __init__(self, depth : Image,
            depth_rbg_img : Image,
            cam_info : CameraInfo,
            cam_frame : str):
        self._depth = depth
        self._frame = cam_frame
        # if depth_rbg_img.header.frame_id != self._frame:
        #     raise ValueError('frame_id must be the same for all messages!' +
        #         f'frame_id: {self._frame} actual frame_id: {depth_rbg_img.header.frame_id}')
        self._depth_rbg_img = depth_rbg_img
        self._cam_info = cam_info
        self._cam_frame = cam_frame

    @property
    def cam_frame(self):
        return self._cam_frame

    @property
    def cam_info(self):
        return self._cam_info

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        self._depth = value

    @property
    def depth_rgb_img(self):
        return self._depth_rbg_img

    @property
    def frame(self):
        return self._frame

    @property
    def pointcloud(self):
        return self._depth

    @property
    def timestamp(self):
        return self._depth.header.stamp

    @property
    def timestamp_rgb(self):
        return self._depth_rbg_img.header.stamp

    @property
    def timestamp_depth(self):
        return self._depth.header.stamp