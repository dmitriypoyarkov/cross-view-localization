import os

import rospkg


class PackagePath(object):
    '''Class for retreiving absolute paths to package folders.'''
    def __init__(self, package_name : str):
        '''
        Parameters:
        - package_name - Path to package
        '''
        rospack = rospkg.RosPack()
        self._package_path = rospack.get_path(package_name)


    def abs_path(self, rel_path : str) -> str:
        return os.path.join(
            self._package_path, rel_path
        )
