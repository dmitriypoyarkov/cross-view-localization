import os

import rospy

import cross_view_localization.package_settings as settings


class CVLInputManager(object):
    def __init__(
            self,
            palette_path : str,
            labels_path : str,
            sat_img_path : str,
            ref_path_path : str = '',
            run : str = ''
        ):
        ok = True
        if not self.check_palette(palette_path):
            ok = False
        if not self.check_labels(labels_path):
            ok = False
        if not self.check_sat_img(sat_img_path):
            ok = False

        if len(ref_path_path) > 0:
            ref_ok = self.check_ref_path(ref_path_path)
            if not ref_ok:
                ok = False

        if len(run) > 0:
            seg_ok = self.check_segmented_imgs(settings.run_path(run, 'seg_imgs'))
            if not seg_ok:
                ok = False

        if not ok:
            raise RuntimeError()
        info_message = (
            '\nCVL: ALL INPUT FILES PRESENT.\n' +
            f'PALETTE PATH: {palette_path}\n' +
            f'LABELS PATH: {labels_path}\n' +
            f'SATELLITE IMAGE PATH: {sat_img_path}\n'
        )
        if len(ref_path_path) > 0:
            info_message += f'REFERENCE PATH: {ref_path_path}\n'
        if len(run) > 0:
            info_message += f'PREPARED SEGMENTED IMAGES: IN USE\n'
        rospy.loginfo(info_message)


    def check_palette(self, palette_path):
        if not self.check_path_exists(palette_path):
            rospy.logerr(
                f'CVL: Palette file not found on {palette_path}. ' +
                f'Palette is required for system run. '
                f'See readme for details.'
            )
            return False
        return True


    def check_labels(self, labels_path):
        if not self.check_path_exists(labels_path):
            rospy.logerr(
                f'CVL: Labels file not found on {labels_path}. ' +
                f'Labels is required for system run. '
                f'See readme for details.'
            )
            return False
        return True


    def check_sat_img(self, sat_img_path):
        if not self.check_path_exists(sat_img_path):
            rospy.logerr(
                f'CVL: Satellite Image file not found on {sat_img_path}. ' +
                f'Satellite Image is required for system run. '
                f'See readme for details.'
            )
            return False
        return True


    def check_ref_path(self, ref_path_path):
        if not self.check_path_exists(ref_path_path):
            rospy.logerr(
                f'CVL: Reference Path file not found on {ref_path_path}. ' +
                f'This is NOT required. You can remove this path from ROS parameters ' +
                f'to run system without reference path. ' +
                f'See readme for details.'
            )
            return False
        return True


    def check_segmented_imgs(self, segmented_imgs_path):
        if not self.check_path_exists(segmented_imgs_path):
            rospy.logerr(
                f'CVL: Path for prepared segmented images not found on {segmented_imgs_path}.' +
                f'You can remove this path from ROS parameters to' +
                f'run system without prepared segmented images.'
                f'See readme for details.'
            )
            return False
        return True


    def check_path_exists(self, path):
        return os.path.exists(path)
