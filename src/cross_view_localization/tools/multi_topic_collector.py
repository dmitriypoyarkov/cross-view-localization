import math
from typing import Dict, List

import genpy
import rospy
from sensor_msgs.msg import PointCloud2

from cross_view_localization.decoration import deco


class MultiTopicCollector(object):
    def __init__(self, topics : List[str], msg_type : type):
        self._topics = topics
        self._subs = [rospy.Subscriber(
            topic, msg_type, self.topic_callback, queue_size=1,
        ) for topic in self._topics]
        self._msg_queues : Dict[str, List] = {}
        self._msg_queue_max_size = 10


    @property
    def topics(self):
        return self._topics


    def get_nearest_msgs(self, timestamp : rospy.Time):
        self.wait_topics()
        msgs = []
        for queue in self._msg_queues.values():
            msgs.append(min(
                queue,
                key=lambda msg : math.fabs(msg.header.stamp.to_sec() - timestamp.to_sec())
            ))
        msgs = sorted(msgs, key=lambda x: x.header.frame_id)
        return msgs


    def wait_topics(self):
        animation = deco.WaitAnimation(f'NO MESSAGES FOR TOPICS {self._topics}. WAITING')

        while not self.topics_online() and not rospy.is_shutdown():
            rospy.sleep(.5)
            animation.update()
        animation.stop()


    def get_latest_msgs(self):
        return self.get_nearest_msgs(self.get_latest_timestamp())


    def get_latest_timestamp(self):
        self.wait_topics()
        queue = list(self._msg_queues.values())[0]
        last_msg = queue[-1]
        return last_msg.header.stamp


    def topics_online(self):
        # print(f'queues: {len(list(self._msg_queues.items()))} topics: {len(self.topics)}')
        if len(list(self._msg_queues.items())) == len(self.topics):
            return True
        return False


    def topic_callback(self, msg):
        queue = self._msg_queues.setdefault(msg.header.frame_id, [])
        if len(queue) > self._msg_queue_max_size:
            queue.pop(0)
        queue.append(msg)
