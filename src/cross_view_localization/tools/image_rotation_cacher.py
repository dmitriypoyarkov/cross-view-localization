from typing import List
import numpy as np
import cv2 as cv
from imutils import rotate_bound

from cross_view_localization.comparison.processed_image_comparer import ProcessedImageComparer


class ImageCache(object):
    def __init__(
            self,
            img : cv.Mat,
            phi : float,
        ):
        self.BLACK = np.array(
            [0, 0, 0],
            dtype=np.uint8
        )
        self._phi = phi
        if img is None:
            return
        
        self._img = rotate_bound(img, -np.rad2deg(self._phi))
        self._valued_pixels = self.calculate_valued_pixels(self._img)
    
    @property
    def img(self):
        return self._img
    
    @property
    def valued_pixels(self):
        return self._valued_pixels

    
    def calculate_valued_pixels(self, img : cv.Mat):
        valued_pixels_count = 0
        for y in range(img.shape[0]):
            for x in range(img.shape[1]):
                pixel = img[y, x]
                valued_pixels_count += np.any(pixel != self.BLACK)
        return valued_pixels_count
    

    def replace_img(self, img : cv.Mat, phi : float):
        self._img = img
        self._img = rotate_bound(img, -np.rad2deg(self._phi))
        self._valued_pixels = self.calculate_valued_pixels(self._img)


# An object of this class should be
# Created only once and reused since that.
class ImageRotationCacher(object):
    def __init__(self, img : cv.Mat, num_rotations : int,
            prepare_rotations=False):
        self._img = img
        self._prepare_rotations = prepare_rotations
        if not self._prepare_rotations:
            self._img_cache = ImageCache(self._img, 0.)
            return
        self._num_rotations = num_rotations

        self._angles = np.linspace(-np.pi, np.pi, num=num_rotations)
        self._rotated_imgs : List[ImageCache] = []
        for phi in self._angles:
            self._rotated_imgs.append(
                ImageCache(self._img, phi)
            )


    @property
    def colors(self):
        return self._colors


    def replace_img(self, img : cv.Mat):
        if not self._prepare_rotations:
            self._img_cache = ImageCache(img, 0.)
            return
        for i, phi in enumerate(self._angles):
            self._rotated_imgs[i] = ImageCache(
                img, phi
            )


    def calculate_index(self, angle):
        return int((angle + np.pi) / (2 * np.pi) * self._num_rotations) % self._num_rotations


    def get_image(self, phi : float):
        if not self._prepare_rotations:
            return self._img_cache
        return self._rotated_imgs[
            self.calculate_index(phi)
        ]
    

    # def rotation_cached(self, index):
    #     return self._image_shapes[index][0] != 0


    # def assign_rotated_image(self, index : int, rotated_image : np.ndarray):
    #     image_shape = rotated_image.shape[:2]
    #     self._rotated_images[
    #         index,
    #         :rotated_image.shape[:2][0],
    #         :rotated_image.shape[:2][1]
    #     ] = rotated_image
    #     self._image_shapes[index] = image_shape


    # def init_image(self):
    #     colors, color_amounts = self._comparer.get_color_amounts(self._img, 40)
    #     self._colors = colors
    #     index = self.calculate_index(0.)
    #     self._color_amounts = np.zeros((self._num_rotations, colors.shape[0]), dtype=np.int32)
    #     self.assign_rotated_image(index, self._img)
    #     self.assign_variables(
    #         index,
    #         color_amounts
    #     )


    # def get_rotated_image(self, index : int):
    #     angle = self._angles[index]
    #     if not self.rotation_cached(index):
    #         rotated_image = rotate_bound(self._img, -np.rad2deg(angle))
    #         self.assign_rotated_image(index, rotated_image)
    #         self.assign_variables(
    #             index,
    #             self._comparer.get_color_amounts_for_colors(rotated_image, self._colors)
    #         )

    #     return self.retrieve_rotated_image(index)


    def get_color_amounts(self, index : int):
        return self._color_amounts[index]


    # def assign_variables(self, index, color_amounts):
    #     self._color_amounts[index] = color_amounts


    # def get_image_shape(self, index):
    #     return self._image_shapes[index]


class ImageRotationCacherOld(object):
    def __init__(self, img : cv.Mat, num_rotations : int,
                 comparer : ProcessedImageComparer):
        self._img = img
        self._num_rotations = num_rotations
        self._comparer = comparer

        self._angles = np.linspace(-np.pi, np.pi, num=num_rotations)
        self._image_shapes = np.zeros((num_rotations, 2), dtype=np.int32)
        self._image_vars = np.zeros((num_rotations, 2), dtype=np.int32)
        max_width = int(np.ceil(np.sqrt(2) * max(img.shape[:2])))  # Maximum possible width
        max_height = int(np.ceil(np.sqrt(2) * max(img.shape[:2])))  # Maximum possible height
        self._rotated_images = np.zeros((num_rotations, max_height, max_width, img.shape[2]), dtype=np.uint8)

        self.init_image()


    @property
    def colors(self):
        return self._colors


    def calculate_index(self, angle):
        return int((angle + np.pi) / (2 * np.pi) * self._num_rotations) % self._num_rotations


    def rotation_cached(self, index):
        return self._image_shapes[index][0] != 0


    def retrieve_rotated_image(self, index : int) -> cv.Mat:
        return self._rotated_images[
            index,
            :self._image_shapes[index][0],
            :self._image_shapes[index][1]
        ]


    def assign_rotated_image(self, index : int, rotated_image : np.ndarray):
        image_shape = rotated_image.shape[:2]
        self._rotated_images[
            index,
            :rotated_image.shape[:2][0],
            :rotated_image.shape[:2][1]
        ] = rotated_image
        self._image_shapes[index] = image_shape


    def init_image(self):
        colors, color_amounts = self._comparer.get_color_amounts(self._img, 40)
        self._colors = colors
        index = self.calculate_index(0.)
        self._color_amounts = np.zeros((self._num_rotations, colors.shape[0]), dtype=np.int32)
        self.assign_rotated_image(index, self._img)
        self.assign_variables(
            index,
            color_amounts
        )


    def get_rotated_image(self, index : int):
        angle = self._angles[index]
        if not self.rotation_cached(index):
            rotated_image = rotate_bound(self._img, -np.rad2deg(angle))
            self.assign_rotated_image(index, rotated_image)
            self.assign_variables(
                index,
                self._comparer.get_color_amounts_for_colors(rotated_image, self._colors)
            )

        return self.retrieve_rotated_image(index)


    def get_color_amounts(self, index : int):
        return self._color_amounts[index]


    def assign_variables(self, index, color_amounts):
        self._color_amounts[index] = color_amounts


    def get_image_shape(self, index):
        return self._image_shapes[index]