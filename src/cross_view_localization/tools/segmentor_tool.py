import argparse
import sys

import pygame

WINDOW_WIDTH = 1200
PANEL_WIDTH = 100
DIVIDER_WIDTH = 20
BRUSH_SIZE = 20
ERASER_SIZE = 20
TRANSPARENCY = 128

class SatSegmentorTool(object):
    def __init__(self, img_path):
        pygame.init()
        self.image_path = img_path
        self.original_image = pygame.image.load(img_path)
        self.image_width, self.image_height = self.original_image.get_size()
        self.window_height = max(self.image_height, 600)
        self.window = pygame.display.set_mode((WINDOW_WIDTH, self.window_height))
        pygame.display.set_caption("Satellite Segmentor Tool")

        self.image_rect = self.original_image.get_rect()
        self.right_surface = pygame.Surface((self.image_width, self.image_height), pygame.SRCALPHA)
        self.processed_image = pygame.Surface((self.image_width, self.image_height), pygame.SRCALPHA)
        self.overlay = self.original_image.copy()
        self.overlay.set_alpha(TRANSPARENCY)

        self.current_tool = 'brush'
        self.current_color = (255, 0, 0)
        self.colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]

        self._drawmode = False
        self._erasemode = False
        self._x, self._y = 0, 0

    def draw_brush(self, surface, pos, color, size):
        pygame.draw.rect(surface, color, (*pos, size, size))

    def draw_eraser(self, surface, pos, size):
        pygame.draw.rect(surface, (0, 0, 0, 0), (*pos, size, size))

    def save_processed_image(self):
        pygame.image.save(self.processed_image, 'processed_image.png')

    def run(self):
        clock = pygame.time.Clock()
        self._running = True

        while self._running:
            self.process_events()

            self.draw()

            clock.tick(60)

        pygame.quit()
        sys.exit()


    def draw(self):
        self.window.fill((255, 255, 255))
        self.right_surface.fill((0, 0, 0, 0))
        self.right_surface.blit(self.overlay, (0, 0))
        self.window.blit(self.original_image, (0, 0))
        self.right_surface.blit(self.processed_image, (0, 0))
        right_surf_x = self.image_width + DIVIDER_WIDTH
        right_surf_rect = self.window.blit(self.right_surface, (right_surf_x, 0))

        panel_x = right_surf_rect.right + 20
        panel_y = 20
        pygame.draw.rect(self.window, (200, 200, 200), (panel_x, panel_y, PANEL_WIDTH - 40, self.window_height - 40))

        if self._drawmode:
            self.draw_brush(
                self.processed_image, (self._x, self._y),
                self.current_color, BRUSH_SIZE
            )
        elif self._erasemode:
            self.draw_eraser(
                self.processed_image,
                (self._x, self._y),
                ERASER_SIZE
            )

        for i, color in enumerate(self.colors):
            color_rect = pygame.Rect(panel_x + 50, panel_y + 50 + i * 50, 40, 40)
            pygame.draw.rect(self.window, color, color_rect)
            if color == self.current_color:
                pygame.draw.rect(self.window, (0, 0, 0), color_rect, 3)

        pygame.display.flip()


    def process_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    self._drawmode = True
                elif event.button == 3:
                    self._erasemode = True
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    self._drawmode = False
                elif event.button == 3:
                    self._erasemode = False
            elif event.type == pygame.MOUSEMOTION:
                self._x, self._y = event.pos[0] - self.image_width - PANEL_WIDTH, event.pos[1]
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_s:
                    self.save_processed_image()
