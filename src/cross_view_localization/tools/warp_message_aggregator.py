import time
from typing import Dict, List

import rospy
from cv_bridge import CvBridge

from cross_view_localization.tools.depth_maker import DepthMaker
from cross_view_localization.tools.multi_topic_collector import \
    MultiTopicCollector
from cross_view_localization.tools.transorm_listener_extended import \
    TransformListenerExtended
from cross_view_localization.tools.warp_data import WarpData, WarpDataSet
from cross_view_localization.tools.warp_message_set import WarpMessageSet


class WarpMessageAggregator(object):
    def __init__(
            self,
            depth_collector : MultiTopicCollector,
            depth_rgb_image_collector : MultiTopicCollector,
            cam_info_collector : MultiTopicCollector,
            listener : TransformListenerExtended,
            base_link_frame : str,
            cam_frames : List[str],
            postprocess_depth = True
        ):
        # self._pointcloud_collector = pointcloud_collector
        self._depth_collector = depth_collector
        self._depth_rgb_image_collector = depth_rgb_image_collector
        self._cam_info_collector = cam_info_collector

        self._listener = listener
        self._base_link_frame = base_link_frame
        self._msg_per_set = len(depth_collector.topics)

        self._cam_frames = cam_frames
        self._cam_frames.sort()

        self._postprocess_depth = postprocess_depth
        if self._postprocess_depth:
            self.initialize_depth_model()

        if len(cam_frames) != self._msg_per_set:
            raise RuntimeError('Cam frames count does not match topic count!')


    def initialize_depth_model(self):
        self._depth_maker = DepthMaker()
        # self._depth_maker.load_model_to_device()
        self._bridge = CvBridge()


    def try_get_msg_sets(self, timestamp : rospy.Time):
        depth_msgs = self._depth_collector.get_nearest_msgs(timestamp)
        depth_rgb_msgs = self._depth_rgb_image_collector.get_nearest_msgs(timestamp)
        cam_info_msgs = self._cam_info_collector.get_nearest_msgs(timestamp)
        if not self.msg_count_matches([depth_msgs, depth_rgb_msgs, cam_info_msgs]):
            error_msg = ('Frame counts do not match. Awaiting ' +
                f'{self._msg_per_set}, got: {len(depth_rgb_msgs)} rgb' +
                f' and {len(depth_msgs)} depth messages.')
            raise RuntimeError(error_msg)
        msg_sets : List[WarpMessageSet] = []
        for i in range(len(depth_msgs)):
            next_set = WarpMessageSet(
                depth_msgs[i],
                depth_rgb_msgs[i],
                cam_info_msgs[i],
                self._cam_frames[i],
            )
            msg_sets.append(next_set)

        return msg_sets


    def get_msg_sets(self, timestamp : rospy.Time):
        while True:
            try:
                msg_sets = self.try_get_msg_sets(timestamp)
                break
            except RuntimeError as e:
                rospy.logdebug(str(e))
                rospy.sleep(rospy.Duration(.5)) # type: ignore
        if self._postprocess_depth:
            self.postprocess_msg_sets(msg_sets)
        return msg_sets


    def postprocess_msg_sets(self, msg_sets : List[WarpMessageSet]):
        for set in msg_sets:
            depth = self._bridge.imgmsg_to_cv2(set.depth, 'passthrough')
            rgb = self._bridge.imgmsg_to_cv2(set.depth_rgb_img, 'rgb8')
            new_depth = self._depth_maker.process_and_calibrate(rgb, depth)
            new_depth_msg = self._bridge.cv2_to_imgmsg(new_depth, 'passthrough')
            new_depth_msg.header = set.depth.header
            set.depth = new_depth_msg


    def msg_count_matches(self, msgs : list):
        for next_set in msgs[1:]:
            if len(next_set) != self._msg_per_set:
                return False
        return True

    # def get_warp_dataset_old(self, timestamp : rospy.Time):
    #     return WarpDataSet(
    #         [WarpData(msg_set, self._listener, self._base_link_frame)
    #         for msg_set in self.get_msg_sets(timestamp)]
    #     )


    def get_warp_dataset(self, timestamp : rospy.Time):
        return WarpDataSet(
            [WarpData(msg_set, self._listener, self._base_link_frame)
            for msg_set in self.get_msg_sets(timestamp)]
        )