#! /usr/bin/python3

import json
import time
from typing import Dict, List

import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

import cross_view_localization.package_settings as settings
from cross_view_localization.tools.depth_maker import DepthMaker
from cross_view_localization.tools.multi_topic_collector import \
    MultiTopicCollector

MAX_TOPICS = 10

class DepthMakerSet(object):
    def __init__(self, depth_img : Image, rgb_img : Image):
        self._depth_img = depth_img
        self._rgb_img = rgb_img

    @property
    def depth_img(self):
        return self._depth_img

    @property
    def rgb_img(self):
        return self._rgb_img


class DepthMakerNode(object):
    BRIDGE = CvBridge()

    def __init__(
            self,
            depth_collector : MultiTopicCollector,
            rgb_collector : MultiTopicCollector,
            out_topics : List[str],
        ):
        self._depth_collector = depth_collector
        self._rgb_collector = rgb_collector
        self._out_topics = out_topics
        self._msg_per_set = len(self._depth_collector.topics)


        self._pubs : List[rospy.Publisher] = []
        for topic in self._out_topics:
            self._pubs.append(rospy.Publisher(topic, Image, queue_size=1))
        self._last_timestamps = []
        self._depth_maker = DepthMaker()

    @property
    def out_topics(self):
        return self._out_topics


    def try_get_msg_sets(self, timestamp : rospy.Time):
        depth_msgs = self._depth_collector.get_nearest_msgs(timestamp)
        rgb_msgs = self._rgb_collector.get_nearest_msgs(timestamp)
        if not self.msg_count_matches([depth_msgs, rgb_msgs]):
            error_msg = ('Frame counts do not match. Awaiting ' +
                f'{self._msg_per_set}, got: {len(rgb_msgs)} rgb' +
                f' and {len(depth_msgs)} depth messages.')
            raise RuntimeError(error_msg)
        msg_sets : List[DepthMakerSet] = []
        for i in range(len(depth_msgs)):
            next_set = DepthMakerSet(
                depth_msgs[i],
                rgb_msgs[i],
            )
            msg_sets.append(next_set)

        return msg_sets


    def msg_count_matches(self, msgs : list):
        for next_set in msgs[1:]:
            if len(next_set) != self._msg_per_set:
                return False
        return True

    def get_msg_sets(self, timestamp : rospy.Time):
        while True:
            try:
                msg_sets = self.try_get_msg_sets(timestamp)
                break
            except RuntimeError as e:
                rospy.logdebug(str(e))
                rospy.sleep(rospy.Duration(.5)) # type: ignore
        return msg_sets


    def process_and_pub_last_set(self):
        # TODO: figure out if this timestamp already published
        next_ts = self._depth_collector.get_latest_timestamp()
        if next_ts in self._last_timestamps:
            return
        self.process_and_pub(next_ts)


    def process_and_pub(self, timestamp : rospy.Time):
        msg_sets = self.get_msg_sets(timestamp)

        new_depth_msgs = []
        for set in msg_sets:
            depth = self.BRIDGE.imgmsg_to_cv2(set.depth_img, 'passthrough')
            rgb = self.BRIDGE.imgmsg_to_cv2(set.rgb_img, 'rgb8')
            new_depth = self._depth_maker.process_and_calibrate(rgb, depth)
            new_depth_msg = self.BRIDGE.cv2_to_imgmsg(new_depth, 'passthrough')
            new_depth_msg.header = set.depth_img.header
            new_depth_msgs.append(new_depth_msg)
        for i in range(len(new_depth_msgs)):
            self._pubs[i].publish(new_depth_msgs[i])
        self._last_timestamps = (
            [set.depth_img.header.stamp for set in msg_sets] +
            [set.rgb_img.header.stamp for set in msg_sets]
        )


    def run(self):
        while not rospy.is_shutdown():
            time.sleep(.05)
            self.process_and_pub_last_set()


if __name__ == '__main__':
    rospy.init_node('depth_maker')

    robot : str = rospy.get_param('~robot') # type: ignore
    cams_path = settings.robot_path(robot, 'config/cameras.json')
    input_rgb_topics = []
    input_depth_topics = []
    out_topics = []
    with open(cams_path, 'r') as f:
        cams : Dict = json.load(f)
    for cam_name, cam_props in cams.items():
        rgb = cam_props['rgb_topic']
        depth : str = cam_props['topic_for_depth_reg']

        out = f'~{cam_name}'

        input_rgb_topics.append(rgb)
        input_depth_topics.append(depth)
        out_topics.append(out)

    # for i in range(MAX_TOPICS):
    #     try:
    #         input_rgb_topics.append(rospy.get_param(f'~input_rgb_topic_{i}'))
    #         input_depth_topics.append(rospy.get_param(f'~input_depth_topic_{i}'))
    #         out_topics.append(rospy.get_param(f'~out_topic_{i}'))
    #     except KeyError:
    #         continue
    # TODO: print inputs and outputs
    # TODO: error checks
    maker_node = DepthMakerNode(
        depth_collector=MultiTopicCollector(
            input_depth_topics, Image
        ),
        rgb_collector=MultiTopicCollector(
            input_rgb_topics, Image
        ),
        out_topics=out_topics
    )
    maker_node.run()