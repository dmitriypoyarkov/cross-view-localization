import importlib
import time
from typing import Dict, List, Type, Union

import animation
import numpy as np
import rospy
from cv_bridge import CvBridge
from geometry_msgs.msg import Point, PoseWithCovarianceStamped
from sensor_msgs.msg import CameraInfo, Image
from std_msgs.msg import String
from tqdm import tqdm

from cross_view_localization.objects.labels import LabelManager
from cross_view_localization.objects.particle_data import ParticleData
from cross_view_localization.objects.pipe_input import PipelineInput
from cross_view_localization.objects.pipeline_data import PipelineData
from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.pipeline.input_checker import InputChecker
from cross_view_localization.plugins.cvl_comp_plugin import (CVLCompPlugin,
                                                             CVLPoseConverter)
from cross_view_localization.plugins.cvl_filter_plugin import CVLFilterPlugin
from cross_view_localization.plugins.cvl_plugin import PluginReadyChecker
from cross_view_localization.plugins.cvl_proc_plugin import CVLProcPlugin
from cross_view_localization.plugins.cvl_seg_plugin import CVLSegPlugin
from cross_view_localization.plugins.cvl_warp_plugin import CVLWarpPlugin
from cross_view_localization.tools.multi_topic_collector import \
    MultiTopicCollector
from cross_view_localization.tools.odometry_provider import OdometryProvider
from cross_view_localization.tools.robot_pose_provider import RobotPoseProvider
from cross_view_localization.tools.transorm_listener_extended import \
    TransformListenerExtended
from cross_view_localization.tools.warp_data import WarpDataSet
from cross_view_localization.tools.warp_message_aggregator import \
    WarpMessageAggregator


def load_plugin(plugin_path):
    plugin_name = plugin_path.split('.')[-1]
    plugin_path = '.'.join(plugin_path.split('.')[:-1])
    module = importlib.import_module(plugin_path)
    return getattr(module, plugin_name)

class CVLSystem(object):
    def __init__(self,
            seg_plugin : str,
            proc_plugin : str,
            warp_plugin : str,
            comp_plugin : str,
            filter_plugin : str,
            sat_img_path : str,
            labels_path : str,
            palette_path : str,
            robot_frame : str,
            odom_frame : str,
            map_frame : str,
            input_depth_topics : List[str],
            cam_info_topics : List[str],
            cam_frames : List[str],
            cam_names : List[str],
            input_depth_rgb_topics: List[str],
            odom_factor : float = 1.,
            warp_output_shape = np.array([64, 64]),
            warp_visible_area_size = 10.,
            input_set_position_topic = '/initialpose',
            input_system_state_topic = '/system_state',
            output_warped_img_topic = '/warped_img',
            output_processed_img_topic = '/processed_img',
            output_segmented_img_topic = '/segmented_img',
            use_ai_depth = False,
            use_input_checker = True
        ):
        self._listener = TransformListenerExtended()
        self._msg_agr = WarpMessageAggregator(
            MultiTopicCollector(
                input_depth_topics,
                Image
            ),
            MultiTopicCollector(
                input_depth_rgb_topics,
                Image
            ),
            MultiTopicCollector(
                cam_info_topics,
                CameraInfo
            ),
            self._listener,
            robot_frame,
            cam_frames,
            postprocess_depth=use_ai_depth
        )
        self._odom_provider = OdometryProvider(
            self._listener,
            odom_frame,
            robot_frame,
            map_frame,
            odom_factor=odom_factor
        )
        self._bridge = CvBridge()
        self._warped_img_pub = rospy.Publisher(
            'warped_img', Image, queue_size=1
        )

        self._system_state = 'PAUSED'
        self._labels_manager = LabelManager.from_files(labels_path, palette_path)

        seg : CVLSegPlugin = load_plugin(seg_plugin)(self._labels_manager.label_colors)
        self.get_segmented = seg.get_segmented

        proc : CVLProcPlugin = load_plugin(proc_plugin)(
            self._labels_manager.palette
        )
        self.get_processed = proc.get_processed

        warp : CVLWarpPlugin = load_plugin(warp_plugin)(
            self._labels_manager.colors_sorted,
            output_shape=warp_output_shape,
            visible_area_size=warp_visible_area_size,
        )
        # warp.set_output_shape(warp_output_shape)
        # warp.set_warp_area_size(warp_visible_area_size)
        self.get_warped = warp.get_warped

        self._sat_img = SatelliteImage.from_file(sat_img_path)
        self._pose_converter = CVLPoseConverter(self._sat_img)
        self._comparer : CVLCompPlugin = load_plugin(comp_plugin)(
            self._sat_img, self._odom_provider
        )
        self.compare_at = self._comparer.compare_at
        self._pose_provider = RobotPoseProvider(
            robot_frame, map_frame,
            self._listener, self._pose_converter
        )
        self._filter : CVLFilterPlugin = load_plugin(filter_plugin)(
            self._comparer, self._odom_provider
        )
        self.filter_step = self._filter.filter_step

        self._last_pipeline = None
        self._latest_img = None

        self._ready_checker = PluginReadyChecker([
            seg, warp, proc, self._comparer, self._filter
        ])

        self.initialize_topics(
            cams=cam_names,
            input_set_position_topic=input_set_position_topic,
            input_system_state_topic=input_system_state_topic,
            input_depth_rgb_topics=input_depth_rgb_topics,
            output_warped_img_topic=output_warped_img_topic,
            output_processed_img_topic=output_processed_img_topic,
            output_segmented_img_topic=output_segmented_img_topic,
        )

        self._pipeline_input = None
        self._input_checker = InputChecker()

    @property
    def pixels_per_meter(self):
        return self.sat_img.pixels_per_meter

    @property
    def particle_data(self):
        return ParticleData(
            self._filter.get_particles(),
            self._filter.get_weights(),
        )

    @property
    def output_coord_world(self):
        return self._pose_converter.img_to_real(self._filter.get_output_coord())

    @property
    def sat_img(self):
        return self._sat_img

    @property
    def filter(self):
        return self._filter

    @property
    def comparer(self):
        return self._comparer

    @property
    def particles(self):
        return self._filter.get_particles()

    @property
    def particles_world(self):
        return self._pose_converter.img_to_real_arr(self.particles)

    @property
    def last_pipeline(self):
        return self._last_pipeline

    @property
    def labels_manager(self):
        return self._labels_manager

    def wait_ready(self):
        self._ready_checker.wait_plugins()
        self.wait_for_tf()


    def initialize_topics(
        self,
        cams = None,
        input_set_position_topic : str = '/initialpose',
        input_system_state_topic = None,
        input_depth_rgb_topics = None,
        output_warped_img_topic = None,
        output_processed_img_topic = None,
        output_segmented_img_topic = None,
    ):
        if output_warped_img_topic is not None:
            self._warped_img_pub = rospy.Publisher(
                output_warped_img_topic,
                Image, queue_size=1
            )
        if output_processed_img_topic is not None:
            self._processed_img_pub = rospy.Publisher(
                output_processed_img_topic,
                Image, queue_size=1
            )

        if (output_segmented_img_topic is not None and
                cams is not None):
            self._segmented_img_pubs = [
                rospy.Publisher(
                    f'{output_segmented_img_topic}_{cam}',
                    Image, queue_size=1
                )
                for cam in cams
            ]
        if input_depth_rgb_topics is not None:
            rospy.Subscriber(
                input_depth_rgb_topics[0],
                Image, self.store_latest_img_callback,
                queue_size=1
            )
        rospy.Subscriber(
            input_set_position_topic, PoseWithCovarianceStamped, self.init_pos_callback, queue_size=1
        )
        rospy.Subscriber(
            input_set_position_topic + '_img', PoseWithCovarianceStamped, self.init_pos_callback_img, queue_size=1
        )
        rospy.Subscriber(
            input_system_state_topic, String, self.system_state_callback, queue_size=1
        )


    def init_pos_callback(self, msg : PoseWithCovarianceStamped):
        position = msg.pose.pose.position
        real_new_pos = np.array([position.x, position.y, 0.])
        img_new_pos = self._pose_converter.real_to_img(real_new_pos)
        self.initpos_and_run(img_new_pos[0], img_new_pos[1])
        # TODO: razobratsya
        # if self._init_pose is None:
            # interactive_pygame.set_odometry_origin(ODOM_ORIGIN_DEBUG[0], ODOM_ORIGIN_DEBUG[1])
            # interactive_pygame.set_odom_steps_predefined(REF_ODOM)
        # self._init_pos = new_pos
        # self._system_state
        # rospy.loginfo(f'CVL: INIT POSITION SET: {self._init_pos}')
    def init_pos_callback_img(self, msg : PoseWithCovarianceStamped):
        position = msg.pose.pose.position
        img_new_pos = np.array([position.x, position.y, 0.])
        self.initpos_and_run(img_new_pos[0], img_new_pos[1])

    def system_state_callback(self, msg : String):
        if msg.data == 'RUNNING':
            if self._init_pos is None:
                rospy.logwarn('CVL: INIT POSITION NOT SET; CANNOT SET STATE TO RUNNING')
                return
            self._filter.reset(*self._init_pos)
            rospy.loginfo(f'CVL: RESETTING PARTICLE FILTER')
        self._system_state = msg.data
        rospy.loginfo(f'CVL: NEW SYSTEM STATE: {self._system_state}')


    def store_latest_img_callback(self, msg : Image):
        if rospy.Time.now() - msg.header.stamp >= rospy.Duration(5):
            rospy.logdebug(f'CVL: img {msg.header.stamp} skipped; too old')
            return
        self._latest_img = msg


    def initpos_and_run(self, x = None, y = None):
        self._init_pos = np.array([x, y])
        self._filter.reset(x, y)
        self._system_state = 'RUNNING'


    def try_pipeline(self, timestamp : rospy.Time):
        if not self.load_input(timestamp):
            return
        self.pipeline()


    def load_input(self, timestamp : rospy.Time):
        try:
            odom_step = self._odom_provider.get_odometry_step(timestamp)
            odom_step_img = self._pose_converter.odom_to_img(odom_step)
        except RuntimeError as e:
            rospy.logwarn(f'ODOMETRY GET ERROR: {str(e)}')
            return False
        warp_data = self._msg_agr.get_warp_dataset(timestamp)
        loaded_input = PipelineInput(timestamp, odom_step_img, warp_data, odom_step)
        if not self._input_checker.check(loaded_input):
            return False
        self._odom_provider.accept_last_step()
        self._pipeline_input = loaded_input
        return True


    def pipeline(self):
        if self._pipeline_input is None:
            raise Exception('MAKE SURE load_input() BEFORE PIPELINE')
        ts = self._pipeline_input.timestamp
        pbar = tqdm(
            total=100, desc=f'WAITING DATA ({ts})',
            ncols=100, ascii=True, bar_format='\033[34m{l_bar} {bar}| [{elapsed_s}s]\033[00m',
        )
        warp_data = self._pipeline_input.warp_dataset
        odom_step_img = self._pipeline_input.odom_step_img
        pbar.update(30)
        pbar.set_description(f'SEGMENTATION ({ts})')
        try:
            segmented_imgs = self.get_segmented(warp_data)
        except RuntimeError as e:
            pbar.close()
            rospy.logerr(f'SEGMENTED GET ERROR: {str(e)}')
            return

        warp_data.set_segmented(segmented_imgs)

        pbar.update(20)
        pbar.set_description(f'WARP ({ts})')
        warped_img = self.get_warped(warp_data)
        pbar.update(10)
        pbar.set_description(f'PROCESS ({ts})')
        processed_img = self.get_processed(warped_img)

        pbar.update(20)
        pbar.set_description(f'FILTER ({ts})')
        self.filter_step(ts, processed_img, odom_step_img)

        self.publish_step_data(
            ts, warped_img=warped_img.img,
            processed_img=processed_img.img,
            segmented_imgs=segmented_imgs,
        )

        self._last_pipeline = PipelineData(
            warp_data=warp_data,
            warped_img=warped_img,
            processed_img=processed_img,
            odom_step=self._pipeline_input.odom_step,
            odom_step_img=odom_step_img,
            robot_pos=self.filter.get_output_coord(),
            particles=self.particles,
            weights=self._filter.get_weights(),
            clusters=self._filter.get_clusters(),
        )

        pbar.update(20)
        pbar.set_description(f'FINISHED ({ts})')
        pbar.close()


    def publish_step_data(
        self,
        timestamp : rospy.Time,
        warped_img=None,
        processed_img=None,
        segmented_imgs=None
    ):
        if warped_img is not None:
            self._warped_img_pub.publish(
                self._bridge.cv2_to_imgmsg(warped_img, 'rgb8')
            )
        if processed_img is not None:
            self._processed_img_pub.publish(
                self._bridge.cv2_to_imgmsg(processed_img, 'rgb8')
            )
        if segmented_imgs is not None:
            for i in range(len(segmented_imgs)):
                msg = self._bridge.cv2_to_imgmsg(
                    segmented_imgs[i], 'rgb8'
                )
                msg.header.stamp = timestamp
                self._segmented_img_pubs[i].publish(msg)


    def wait_next_frame(self):
        if self._system_state != "RUNNING":
            raise RuntimeError('CVL: System not running yet. Change system state')
        wait_animation = animation.Wait(color='blue')
        wait_animation.start()
        while self._latest_img is None:
            time.sleep(0.05)
        wait_animation.stop()
        timestamp = rospy.Time.from_sec(
            self._latest_img.header.stamp.to_sec()
        )
        self._latest_img = None
        return timestamp


    def wait_for_tf(self):
        self._odom_provider.wait_for_tf()

    # def run_system(self):
    #     # rospy.Subscriber(
    #     #     INPUT_DEPTH_REG_TOPICS[0],
    #     #     Image, store_latest_img_callback,
    #     #     queue_size=1
    #     # )
    #     # rospy.Subscriber(
    #     #     SET_POSITION_TOPIC, Point, set_position_callback, queue_size=1
    #     # )
    #     # rospy.Subscriber(
    #     #     SYSTEM_STATE_TOPIC, String, system_state_callback, queue_size=1
    #     # )
    #     # DEBUG
    #     # odometry_provider.collect_odometry_steps()
    #     while not rospy.is_shutdown():
    #         timestamp = rospy.Time(0)
    #         if self._latest_img is not None:
    #             timestamp = self._latest_img.header.stamp
    #         interactive_pygame.update(
    #             img_robot_trans=particle_filter._state_estimate,
    #             phi=phi,
    #             warped_img=warped_imgg,
    #             odometry_step_img=odometry_step_img,
    #             particles=particle_filter._particles,
    #             weights=particle_filter._weights,
    #             state_estimate=particle_filter._state_estimate,
    #             timestamp=timestamp
    #         )
    #         if transform_available and latest_img:
    #             stamp = latest_img.header.stamp
    #             latest_img = None
    #             if system_state == 'RUNNING':
    #                 try:
    #                     pipeline_iteration(stamp)
    #                     evaluation_debug.add_evaluation(stamp, particle_filter.state_estimate)
    #                 except tf2.TransformException as e:
    #                     rospy.logdebug(f'CVL: SKIPPING FRAME {stamp}...')
    #                     continue
    #             else:
    #                 rospy.logdebug('CVL: SYSTEM PAUSED. SLEEPING...')
    #                 time.sleep(0.1)
    #         else:
    #             time.sleep(0.1)
    #     result = evaluation_debug.save_evaluation(OUT_EVAL_PATH, sat_provider.meters_per_pixel)
    #     final_img = interactive_pygame.create_final_img(result['mean_delta'])
    #     cv.imwrite(OUT_EVAL_PATH_IMG, final_img)
    #     # interactive_pygame.save_odoms_from_world(odometry_provider._odom_steps_tf, odometry_provider._odom_times_tf)
    #     # interactive_pygame.save_odometry_steps()
