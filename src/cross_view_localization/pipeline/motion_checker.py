
import numpy as np


class MotionChecker(object):
    def __init__(
            self, d_threshold : float,
            phi_threshold : float):
        self._d_threshold = d_threshold
        self._phi_thresold = phi_threshold


    def check_motion(self, odom_step : np.ndarray):
        if np.abs(odom_step[0]) >= self._d_threshold:
            return True
        if np.abs(odom_step[1]) >= self._phi_thresold:
            return True
        return False