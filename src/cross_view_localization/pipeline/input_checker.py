

from typing import Union

import rospy

from cross_view_localization.objects.pipe_input import PipelineInput
from cross_view_localization.pipeline.motion_checker import MotionChecker


class InputChecker(object):
    '''
    This class takes the pipe input and decides whether
    this input passes through. If it does, it is also saves
    the input to compare it to the newer inputs in the future.

    The point is to slow down the pipe because too frequent
    updates are presumably harmful for the result.
    '''
    CHECK_TIME = 1 << 0 # 0001
    CHECK_MOTION = 1 << 1 # 0010
    def __init__(self, flags=CHECK_MOTION | CHECK_TIME,
                 time_threshold=0.2, distance_threshold=0.8,
                 angle_threshold=0.4):
        self._checks = {
            self.CHECK_TIME : self.check_time,
            self.CHECK_MOTION : self.check_motion
        }
        self._flags = flags
        self._last_input = None

        self._motion_checker = MotionChecker(
            d_threshold=distance_threshold,
            phi_threshold=angle_threshold
        )

        self._time_threshold = rospy.Duration(secs=time_threshold) # type: ignore


    def check_time(self, filter_input : PipelineInput):
        if self._last_input is None:
            return True
        if filter_input.timestamp - self._last_input.timestamp >= self._time_threshold:
            return True
        return False


    def check_motion(self, filter_input : PipelineInput):
        if self._last_input is None:
            return True
        return self._motion_checker.check_motion(filter_input.odom_step)


    def check(self, pipe_input : PipelineInput):
        # print(f'PRE FILTER: {pipe_input.odom_step}')
        for flag, perform_check in self._checks.items():
            check_required = flag & self._flags
            if not check_required:
                continue
            if not perform_check(pipe_input):
                return False
        self._last_input = pipe_input
        return True