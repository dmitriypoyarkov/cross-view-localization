import hashlib
import inspect

import rospy
from numba.pycc import CC

from cross_view_localization.comparison import processed_image_comparer
from cross_view_localization.debug_stage import debug_image_discreter
from cross_view_localization.tools import (satellite_image_provider, utils,
                                           warp_data)
from cross_view_localization.warp import warper


def compile_numba_functions():
    rospy.logdebug('COMPILING FUNCTIONS...')
    
    modules = [
        processed_image_comparer,
        debug_image_discreter,
        satellite_image_provider,
        utils,
        warp_data,
        warper,
    ]
    for module in modules:
        source_code = inspect.getsource(module)
        checksum = hashlib.sha1(source_code.encode()).hexdigest()
        checksum_filename = f'{module.__name__.split(".")[-1]}_checksum'
        try:
            with open(checksum_filename, "r") as file:
                saved_checksum = file.read()
            if checksum == saved_checksum:
                rospy.logdebug(f'CHECKSUM FILE {checksum_filename} MATCHED. SKIPPING...')
                continue
            else:
                rospy.logdebug(f'CHECKSUM FILE {checksum_filename} OUTDATED. RECOMPILING...')
        except FileNotFoundError:
            rospy.logdebug(f'CHECKSUM FILE {checksum_filename} NOT FOUND. COMPILING...')
        CC(module.__name__.split('.')[-1]).compile()
        with open(checksum_filename, "w") as file:
            file.write(checksum)
    rospy.logdebug('COMPILING FINISHED')