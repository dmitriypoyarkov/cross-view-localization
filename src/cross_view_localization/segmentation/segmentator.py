from typing import List

import cv2 as cv
from cv2.typing import MatLike
from semantic_segmentation.segmentator import SemanticSegmentator

from cross_view_localization.tools.warp_data import WarpDataSet


class SemanticSegmentatorExtended(SemanticSegmentator):
    def __init__(self, label_colors, resize_shape=(640,480)):
        self._resize_shape = resize_shape
        super().__init__(label_colors)

    
    def segment_warp_dataset(self, warp_dataset : WarpDataSet):
        segmented_imgs : List[MatLike] = []
        for warp_data in warp_dataset.warp_datas:
            rgb_img = warp_data.rgb_img
            original_shape = rgb_img.shape[:2]
            resized_img = cv.resize(rgb_img,
                                    self._resize_shape,
                                    interpolation=cv.INTER_LINEAR)
            segmented_img = self.process_image(resized_img)
            unresized_img = cv.resize(segmented_img,
                (original_shape[1], original_shape[0]),
                interpolation=cv.INTER_NEAREST
            )
            segmented_imgs.append(
                unresized_img
            )

        return segmented_imgs