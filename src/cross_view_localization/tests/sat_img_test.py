import os

import numpy as np

from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.plugins.cvl_comp_plugin import CVLPoseConverter

# Get the directory of the current Python file
current_dir = os.path.dirname(os.path.realpath(__file__))

# Construct the path to the "../../../input" folder relative to the current directory
input_folder = os.path.normpath(os.path.join(current_dir, "../../../input"))
img_path = os.path.join(input_folder, 'sat_img.bmp')

print(input_folder)

sat_img = SatelliteImage.from_file(
    img_path
)

converter = CVLPoseConverter(sat_img)

img_poses = np.array([[0., 0., 0.2], [100., 0., 0.2], [0., 100., 0.2], [100., 100., 0.2], [100., 200., 0.2]])

for pose in img_poses:
    real = converter.img_to_real(pose)
    img = converter.real_to_img(real)
    print(pose, real, img)
    assert np.allclose(pose, img)