from abc import ABC, abstractmethod, abstractproperty
from typing import List, Tuple

import cv2 as cv
import numpy as np
from cv2.typing import MatLike

from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.plugins.cvl_plugin import CVLPlugin
from cross_view_localization.tools.odometry_provider import OdometryProvider
from cross_view_localization.tools.warp_data import LandImg, WarpDataSet


class CVLPoseConverter(object):
    def __init__(self, sat_img : SatelliteImage):
        self._sat_img = sat_img


    def img_to_real(self, coord_img : np.ndarray) -> np.ndarray:
        '''Parameters:
        - coord_img (ndarray): [x, y, phi] - coordinates on sat_img
        '''
        coord_world = np.array([
            (self._sat_img.img_center_y - coord_img[1]) / self._sat_img.pixels_per_meter + self._sat_img.center_x,
            (self._sat_img.img_center_x - coord_img[0]) / self._sat_img.pixels_per_meter + self._sat_img.center_y,
            coord_img[2]
        ], dtype=np.float64)
        return coord_world


    def img_to_real_arr(self, coord_img_arr : np.ndarray) -> np.ndarray:
        '''Parameters:
        - coord_img (ndarray): [[x, y, phi], ...] - coordinates on sat_img
        '''
        if coord_img_arr.shape[0] == 0:
            return coord_img_arr
        coord_world_arr = np.array([
            (self._sat_img.img_center_y - coord_img_arr[:, 1]) / self._sat_img.pixels_per_meter + self._sat_img.center_x,
            (self._sat_img.img_center_x - coord_img_arr[:, 0]) / self._sat_img.pixels_per_meter + self._sat_img.center_y,
            coord_img_arr[:, 2]
        ], dtype=np.float64).transpose()
        return coord_world_arr


    def real_to_img(self, coord_real : np.ndarray) -> np.ndarray:
        '''Parameters:
        - coord_real (ndarray): [x, y, phi] - coordinates in world space
        '''
        coord_img = np.array([
            -(coord_real[1] - self._sat_img.center_y) * self._sat_img.pixels_per_meter + self._sat_img.img_center_x,
            -(coord_real[0] - self._sat_img.center_x) * self._sat_img.pixels_per_meter + self._sat_img.img_center_y,
            coord_real[2]
        ], dtype=np.float64)
        return coord_img


    def odom_to_img(self, step_real : np.ndarray) -> np.ndarray:
        '''Parameters:
        - step_real (ndarray): [d, phi] - step in world space
        '''
        return np.array([
            self._sat_img.pixels_per_meter * step_real[0],
            step_real[1]
        ])


    def odom_to_real(self, step_img : np.ndarray) -> np.ndarray:
        '''Parameters:
        - step_img (ndarray): [d, phi] - step in world space
        '''
        return np.array([
            self._sat_img.meters_per_pixel * step_img[0],
            step_img[1]
        ])


class CVLCompPlugin(CVLPlugin):
    def __init__(self, sat_img : SatelliteImage, odom_provider : OdometryProvider):
        self._sat_img = sat_img
        raise NotImplementedError()


    def compare_at(self, x : float, y : float, phi : float) -> float:
        raise NotImplementedError()


    def get_sat_img_shape(self) -> Tuple[int, int]:
        raise NotImplementedError()


    def prepare_img(self, sample_img : LandImg) -> None:
        raise NotImplementedError()


    def get_sat_img(self) -> MatLike:
        raise NotImplementedError()


    def get_pixels_per_meter(self) -> float:
        raise NotImplementedError()


    def get_pose_converter(self):
        return
