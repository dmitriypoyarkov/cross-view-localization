from typing import List

import cv2 as cv

from cross_view_localization.plugins.cvl_plugin import CVLPlugin
from cross_view_localization.tools.warp_data import LandImg, WarpDataSet


class CVLProcPlugin(CVLPlugin):
    def get_processed(self, img : LandImg) -> LandImg:
        raise NotImplementedError()