import numpy as np

from cross_view_localization.plugins.cvl_warp_plugin import CVLWarpPlugin
from cross_view_localization.tools.warp_data import LandImg, WarpDataSet
from cross_view_localization.warp.warper import Warper


class WarpPluginLocal(CVLWarpPlugin):
    def __init__(self, colors_sorted, visible_area_size : float, output_shape : np.ndarray):
        self._warper = Warper(colors_sorted, output_shape, visible_area_size)


    def get_warped(self, warp_dataset: WarpDataSet) -> LandImg:
        return self._warper.warp_multi(warp_dataset)