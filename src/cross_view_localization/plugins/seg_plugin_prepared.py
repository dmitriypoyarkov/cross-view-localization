from cross_view_localization import package_settings as settings
from cross_view_localization.debug_stage.debug_img_loader import DebugImgLoader
from cross_view_localization.plugins.cvl_plugin_local import CVLPluginLocal
from cross_view_localization.plugins.cvl_seg_plugin import CVLSegPlugin
from cross_view_localization.segmentation.segmentator import \
    SemanticSegmentatorExtended
from cross_view_localization.tools.warp_data import LandImg, WarpDataSet


class SegPluginPrepared(CVLSegPlugin):
    def __init__(self, label_colors):
        self._segm = DebugImgLoader(settings.run_path('run_001', 'seg_imgs'))


    def get_segmented(self, warp_dataset : WarpDataSet):
        return self._segm.load_segmented(warp_dataset)