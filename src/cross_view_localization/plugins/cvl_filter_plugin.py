from abc import abstractmethod
from typing import List, Union

import cv2 as cv
import numpy as np
import rospy
from cv2.typing import MatLike

from cross_view_localization.filtering.cluster import Clusters
from cross_view_localization.plugins.cvl_comp_plugin import CVLCompPlugin
from cross_view_localization.plugins.cvl_plugin import CVLPlugin
from cross_view_localization.tools.odometry_provider import OdometryProvider
from cross_view_localization.tools.warp_data import LandImg, WarpDataSet


class CVLFilterPlugin(CVLPlugin):
    def __init__(self, comparer : CVLCompPlugin, odometry_provider : OdometryProvider):
        self._comparer = comparer
        self._odometry_provider = odometry_provider

    def filter_step(
            self,
            timestamp : rospy.Time,
            proc_img : LandImg,
            odom_step_img : np.ndarray
        ):
        raise NotImplementedError()


    def reset(self, x, y) -> None:
        raise NotImplementedError()


    def get_output_coord(self) -> np.ndarray:
        raise NotImplementedError()


    def get_last_warped_img(self) -> MatLike:
        raise NotImplementedError()


    def get_last_processed_img(self) -> Union[MatLike, None]:
        raise NotImplementedError()


    def get_particles(self) -> np.ndarray:
        raise NotImplementedError()


    def get_weights(self) -> np.ndarray:
        raise NotImplementedError()


    def get_last_odom_step(self) -> np.ndarray:
        raise NotImplementedError()


    def get_last_timestamp(self) -> rospy.Time:
        raise NotImplementedError()


    def get_clusters(self) -> Union[Clusters, None]:
        return None