from abc import ABC

import numpy as np
from cv2.typing import MatLike

from cross_view_localization.comparison.processed_image_comparer import \
    ProcessedImageComparer
from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.plugins.cvl_comp_plugin import CVLCompPlugin
from cross_view_localization.tools.odometry_provider import OdometryProvider
from cross_view_localization.tools.satellite_image_provider import \
    SatelliteImageProvider
from cross_view_localization.tools.warp_data import LandImg, WarpDataSet


class CompPluginLocal(CVLCompPlugin):
    def __init__(self, sat_img : SatelliteImage, odom_provider : OdometryProvider):
        self._sat_provider = SatelliteImageProvider(sat_img)


    def get_sat_img(self):
        return self._sat_provider.sat_img


    def set_sat_img(self, sat_img: SatelliteImage):
        self._sat_provider.set_sat_img(sat_img)


    def compare_at(self, x, y, phi):
        return self._sat_provider.compare_at_coord_inplace(x, y, phi)


    def get_sat_img_shape(self):
        return self._sat_provider.img_shape


    def get_pixels_per_meter(self) -> float:
        return self._sat_provider.pixels_per_meter


    def prepare_img(self, sample_img : LandImg) -> None:
        return self._sat_provider.prepare_img(sample_img)

