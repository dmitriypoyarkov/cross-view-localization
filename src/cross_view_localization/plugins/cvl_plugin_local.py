from cross_view_localization.plugins.cvl_plugin import CVLPlugin
from cross_view_localization.tools.package_path import PackagePath


class CVLPluginLocal(CVLPlugin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pack = PackagePath()