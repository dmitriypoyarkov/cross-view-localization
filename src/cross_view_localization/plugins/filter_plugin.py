import os
from typing import Union

import numpy as np
import rospy
from cv2.typing import MatLike

import cross_view_localization.package_settings as settings
from cross_view_localization.filtering.particle_filter import ParticleFilter
from cross_view_localization.filtering.pre_filter import PreFilter
from cross_view_localization.objects.filter_input import FilterInput
from cross_view_localization.plugins.cvl_comp_plugin import CVLCompPlugin
from cross_view_localization.plugins.cvl_filter_plugin import CVLFilterPlugin
from cross_view_localization.tools.warp_data import LandImg


class FilterPluginLocal(CVLFilterPlugin):
    def __init__(self, comparer, odometry_provider):
        super().__init__(comparer, odometry_provider)
        config_file = str(
            rospy.get_param('~filter_config', default='filter_config.json')
        )
        if not os.path.isabs(config_file):
            config_file = settings.config_path(config_file)
        if not os.path.exists(config_file):
            raise Exception(f'Filter configuration not found on {config_file}!')
        self._particle_filter = ParticleFilter.from_config(
            comp_plugin=comparer,
            odom_provider=odometry_provider,
            config_path=config_file,
        )
        self._pre_filter = PreFilter()


    def set_comparer(self, comp : CVLCompPlugin):
        self._particle_filter.set_sat_provider(comp)


    def set_odom_provider(self, odom_provider):
        self._particle_filter.set_odom_provider(odom_provider)


    def filter_step(
            self, timestamp : rospy.Time,
            proc_img : LandImg,
            odom_step_img : np.ndarray
        ):
        # filter_input = FilterInput(timestamp, odom_step_img, proc_img)
        # # TODO: Think about how not to waste the CPU and GPU work
        # # because this check can be performed long before filter's turn
        # if not self._pre_filter.check_input(filter_input):
        #     return self._particle_filter.state_estimate
        return self._particle_filter.step(
            timestamp, proc_img, odom_step_img
        )


    def get_particle_img(self):
        return self._particle_filter.draw_particles()


    def get_particles(self):
        return self._particle_filter._particles


    def get_weights(self):
        return self._particle_filter._weights


    def get_last_odom_step(self):
        return self._particle_filter._last_odom_step


    def get_last_timestamp(self):
        return self._particle_filter._last_timestamp


    def get_last_processed_img(self) -> Union[MatLike, None]:
        return self._particle_filter.last_processed_img


    def get_output_coord(self) -> np.ndarray:
        return self._particle_filter.state_estimate


    def reset(self, x, y):
        self._particle_filter.reset(x, y)


    def get_clusters(self):
        return self._particle_filter.clusters