import time
from abc import ABC
from typing import List

import rospy

from cross_view_localization.decoration.deco import WaitAnimation


class CVLPlugin(ABC):
    def __init__(self):
        pass


    def ready(self) -> bool:
        return True


class PluginReadyChecker(object):
    def __init__(self, plugins : List[CVLPlugin]):
        self._plugins = plugins


    def wait_plugins(self):
        anim = WaitAnimation('CVL: WAITING FOR PLUGINS')
        while not self.all_plugins_ready() and not rospy.is_shutdown():
            time.sleep(.5)
            pl = self.get_not_ready()
            anim.update(f'CVL: PLUGIN {pl} NOT READY. WAITING')
        anim.set_text('CVL: ALL PLUGINS READY')
        anim.stop()


    def all_plugins_ready(self):
        return all(map(lambda pl : pl.ready(), self._plugins))


    def get_not_ready(self):
        readyness = list(map(lambda pl : pl.ready(), self._plugins))
        try:
            return self._plugins[readyness.index(False)]
        except ValueError:
            return None