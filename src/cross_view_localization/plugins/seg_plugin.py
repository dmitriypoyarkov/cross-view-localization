from cross_view_localization.plugins.cvl_seg_plugin import CVLSegPlugin
from cross_view_localization.segmentation.segmentator import \
    SemanticSegmentatorExtended
from cross_view_localization.tools.warp_data import WarpDataSet


class SegPluginLocal(CVLSegPlugin):
    def __init__(self, label_colors):
        self._segmentator = SemanticSegmentatorExtended(label_colors)


    def get_segmented(self, warp_dataset : WarpDataSet):
        return self._segmentator.segment_warp_dataset(warp_dataset)