

import json
import threading
import time
from typing import List

import numpy as np
import rospy
from cv2.typing import MatLike
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from std_msgs.msg import String

from cross_view_localization.plugins.cvl_seg_plugin import CVLSegPlugin
from cross_view_localization.tools.multi_topic_collector import \
    MultiTopicCollector
from cross_view_localization.tools.warp_data import WarpDataSet


class SegPluginRemote(CVLSegPlugin):
    BRIDGE = CvBridge()
    def __init__(
            self, label_colors : np.ndarray,
        ):
        self._ready = False
        self._label_colors = label_colors
        self._colors_pub = rospy.Publisher(
            '~label_colors', String, queue_size=1
        )

        seg_topics = self.get_seg_topics()
        if len(seg_topics) == 0:
            msg = ('Segmentation plugin is SegPluginRemote but ' +
                'remote_seg_topic_[] launch params not specified')
            rospy.logerr(msg)
            raise RuntimeError(msg)
        self._topic_collector = MultiTopicCollector(seg_topics, Image)
        connect_thread = threading.Thread(target=self.connect_with_seg)
        connect_thread.start()


    def __str__(self):
        return 'SegPluginRemote'


    def get_seg_topics(self):
        seg_topics = []
        for i in range(10):
            try:
                next_topic = rospy.get_param(f'~remote_seg_topic_{i}')
                print(f'next topic {next_topic}')
            except KeyError:
                continue
            if len(next_topic) == 0:
                continue
            seg_topics.append(next_topic)
        return seg_topics


    def connect_with_seg(self):
        while not rospy.is_shutdown() and not self._topic_collector.topics_online():
            self._colors_pub.publish(
                String(data=json.dumps(self._label_colors.tolist()))
            )
            time.sleep(1.)
        self._ready = True
        while not rospy.is_shutdown():
            self._colors_pub.publish(
                String(data=json.dumps(self._label_colors.tolist()))
            )
            time.sleep(7.)


    def get_segmented(self, warp_dataset : WarpDataSet) -> List[MatLike]:
        ts = rospy.Time.from_sec(
            warp_dataset.timestamp.to_sec()
        )

        seg_msgs = self._topic_collector.get_nearest_msgs(ts)
        seg_imgs = [self.BRIDGE.imgmsg_to_cv2(msg, 'rgb8') for msg in seg_msgs]
        return seg_imgs


    def ready(self):
        return self._ready
