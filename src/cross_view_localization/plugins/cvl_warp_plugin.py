from typing import List

import cv2 as cv
import numpy as np

from cross_view_localization.plugins.cvl_plugin import CVLPlugin
from cross_view_localization.tools.warp_data import LandImg, WarpDataSet


class CVLWarpPlugin(CVLPlugin):
    def __init__(self, colors_sorted : np.ndarray, visible_area_size, output_shape):
        raise NotImplementedError()


    def get_warped(self, warp_dataset : WarpDataSet) -> LandImg:
        raise NotImplementedError()