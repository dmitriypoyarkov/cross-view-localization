from typing import List

import cv2 as cv
from cv2.typing import MatLike

from cross_view_localization.plugins.cvl_plugin import CVLPlugin
from cross_view_localization.tools.warp_data import WarpDataSet


class CVLSegPlugin(CVLPlugin):
    def __init__(self, label_colors):
        raise NotImplementedError()


    def get_segmented(self, warp_dataset : WarpDataSet) -> List[MatLike]:
        raise NotImplementedError()