from typing import List

import cv2 as cv

from cross_view_localization.objects.labels import Palette
from cross_view_localization.plugins.cvl_proc_plugin import CVLProcPlugin
from cross_view_localization.processing.img_processor import LandImgProcessor
from cross_view_localization.tools.warp_data import LandImg


class ProcPluginLocal(CVLProcPlugin):
    def __init__(self, palette : Palette):
        self._processor = LandImgProcessor(palette)


    def get_processed(self, img : LandImg) -> LandImg:
        return self._processor.process(img)