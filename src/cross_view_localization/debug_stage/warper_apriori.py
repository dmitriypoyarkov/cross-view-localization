import numpy as np
import rospy

from cross_view_localization.tools.odometry_provider import OdometryProvider
from cross_view_localization.tools.satellite_image_provider import \
    SatelliteImageProvider
from cross_view_localization.tools.warp_data import LandImg


# DEPR
class WarperApriori(object):
    def __init__(self, projection_shape,
            sat_provider : SatelliteImageProvider,
            odometry_provider : OdometryProvider,
            warp_shape : tuple = (50, 50)):
        self._projection_shape = projection_shape
        self._sat_provider = sat_provider
        self._odometry_provider = odometry_provider
        self._warp_shape = warp_shape


    def get_warped(self, timestamp : rospy.Time):
        trans, phi = self._sat_provider.get_real_pose_img(timestamp)
        apriori_warped = self._sat_provider.cut_rotated_from_satellite_img(
            trans[0], trans[1], phi, self._warp_shape
        )
        land_img = LandImg(apriori_warped, self._sat_provider.pixels_per_meter)
        return land_img