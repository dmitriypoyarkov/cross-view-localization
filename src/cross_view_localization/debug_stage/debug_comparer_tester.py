import sys

import cv2 as cv
import numpy as np
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

from cross_view_localization.tools.satellite_image_provider import \
    SatelliteImageProvider


class ComparerTester(object):
    def __init__(self, sat_provider : SatelliteImageProvider,
            input_processed_img_topic : str = None,
            output_metric_map_topic : str = None):
        self._sat_provider = sat_provider
        if input_processed_img_topic:
            self._processed_img_sub = rospy.Subscriber(input_processed_img_topic,
                Image, self.processed_img_callback, queue_size=1)
            self._metric_map_pub = rospy.Publisher(output_metric_map_topic,
                Image, queue_size=1)
        self._bridge = CvBridge()


    def processed_img_callback(self, img : Image):
        # TODO: implement
        pass


    def produce_metric_map(self, processed_img : cv.Mat,
            meters_per_pixel : tuple):
        rospy.logdebug('Comparer Tester: Started')

        new_img_shape = (
            int(processed_img.shape[0] * meters_per_pixel[0] / self._sat_provider.meters_per_pixel[0]),
            int(processed_img.shape[1] * meters_per_pixel[1] / self._sat_provider.meters_per_pixel[1])
        )
        rospy.logdebug(f'processed_shape: {processed_img.shape}, m/p: {meters_per_pixel}, sat m/p: {self._sat_provider.meters_per_pixel}')
        processed_img = cv.resize(processed_img, new_img_shape)

        metric_map = np.zeros(
            (self._sat_provider.img_shape[0],
            self._sat_provider.img_shape[1]),
            dtype=np.uint8
        )
        percent_start = 0.25
        percent_end = 0.75
        y_start = int(metric_map.shape[0] * percent_start)
        y_end = int(percent_end * metric_map.shape[0])
        x_start = int(metric_map.shape[1] * percent_start)
        x_end = int(percent_end * metric_map.shape[1])
        for y in range(y_start, y_end, 2):
            for x in range(x_start, x_end, 2):
                if x % 100 == 0:
                    rospy.logdebug(f'Comparer Tester: Next: {x}, {y} /\
{self._sat_provider.img_shape[1], self._sat_provider.img_shape[0]}')
                try:
                    metric_map[y, x] = self._sat_provider.compare_at_coord(
                        processed_img, x, y, 0.)
                except ValueError as e:
                    continue
        metric_map = metric_map[y_start:y_end, x_start:x_end]
        rospy.logdebug('Comparer Tester: Done')
        return metric_map


    def produce_and_publish_metric_map(self, warped_processed_img : np.ndarray):
        metric_map = self.produce_metric_map(warped_processed_img)
        metric_map_imgmsg = self._bridge.cv2_to_imgmsg(metric_map, 'mono8')
        self._metric_map_pub.publish(metric_map_imgmsg)