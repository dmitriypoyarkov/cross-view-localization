import os
import sys
from typing import List

import numpy as np


class CamString(object):
    def __init__(self, cam_name, cam_frame):
        self.cam_name = cam_name
        self.cam_frame = cam_frame


class ImageSearcher(object):
    def __init__(self, folder_path, cam_frames : List[str]):
        self._folder_path = folder_path
        self._cams = []
        for frame in cam_frames:
            self._cams.append(CamString(self.frame_to_cam_name(frame), frame))

        self._timestamps_for_cameras = self.load_timestamps_for_cameras()


    def load_timestamps_for_cameras(self):
        timestamps_dict = {}

        for cam in self._cams:
            timestamps = []
            filenames = []

            # List all files in the folder
            for filename in os.listdir(self._folder_path):
                if filename.startswith(cam.cam_name):
                    # Extract timestamp from filename
                    timestamp = int(filename.split('_')[-1].split('.')[0])
                    timestamps.append(timestamp)
                    path = os.path.join(self._folder_path, filename)
                    filenames.append(path)

            timestamps = np.array(timestamps)
            filenames = np.array(filenames)

            # Sort timestamps
            sort_indices = np.argsort(timestamps)
            timestamps = timestamps[sort_indices]
            
            # print(f'sorted {}')
            filenames = filenames[sort_indices]

            timestamps_dict[cam.cam_frame] = {'timestamps': timestamps, 'filenames': filenames}

        return timestamps_dict
    
    
    def get_img_count(self):
        return len(self._timestamps_for_cameras)
    

    def cam_name_to_frame(self, cam_name):
        return f'/{cam_name.replace("_", "/")}'


    def frame_to_cam_name(self, frame):
        return frame[1:].replace('/', '_')


    def find_closest_image_for_timestamp(self, timestamp_needed, timestamps):
        percent = (timestamp_needed - timestamps[0]) / (timestamps[-1] - timestamps[0])
        if percent < 0.:
            raise RuntimeError(f'NO IMAGE FOR {timestamp_needed}')
        approx_index = int(percent * len(timestamps))

        # Define a small window size for refining the search
        window_size = 5

        # Refine the search within a small neighborhood around the approximate index
        left = max(0, approx_index - window_size)
        right = min(len(timestamps) - 1, approx_index + window_size)
        # print(f'left {left}, right {right}, index {approx_index}, window {window_size} min_stamp {timestamps[0]}')

        closest_image = None
        first_diff = None
        min_diff = float('inf')

        for i in range(left, right + 1):
            diff = abs(timestamps[i] - timestamp_needed)

            if diff == 0:
                closest_image = timestamps[i]
                break

            if diff < min_diff:
                min_diff = diff
                closest_image = timestamps[i]

            # Check if the window is misplaced
            if first_diff is None:
                first_diff = diff
            if first_diff is not None and diff > first_diff:
                break

        return closest_image

    def get_closest_images_for_timestamp(self, timestamp):
        closest_images = {}

        for cam_name, data in self._timestamps_for_cameras.items():
            timestamps = data['timestamps']
            closest_timestamp = self.find_closest_image_for_timestamp(timestamp, timestamps)
            # print(f'closest timestamp {closest_timestamp}, difference {((closest_timestamp - timestamp)/1_000_000_000):.2f} sec')
            closest_image_idx = np.where(timestamps == closest_timestamp)[0][0]
            closest_images[cam_name] = data['filenames'][closest_image_idx]

        return closest_images


if __name__ == '__main__':
    folder_path = '/home/fc/Desktop/bags/BAGS/RITROVER/taa'
    camera_names = ['oakd_fr', 'oakd_fl', 'oakd_back']
    timestamp_needed = 1698323357489252626

    image_searcher = ImageSearcher(folder_path, camera_names)
    closest_images = image_searcher.get_closest_images_for_timestamp(timestamp_needed)
