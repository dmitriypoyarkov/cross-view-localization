#! /usr/bin/python3

# Input: camera image from simulation, where all textures
# are replaced with semantic colors.
# Output: each color on image is replaced to nearest
# label color.

import math
from typing import List, Tuple

import cv2 as cv
import numpy as np
import rospy
from cv_bridge import CvBridge
from numba import njit
from numba.pycc import CC
from rospy import Header
from sensor_msgs.msg import Image

from cross_view_localization.tools.warp_data import WarpDataSet

cc = CC('debug_image_discreter')
cc.verbose = True

@njit
@cc.export('color_diff_hsv_njit', 'f4[:](u1[:], u1[:])')
def color_diff_hsv_njit(
        color1 : np.ndarray,
        color2 : np.ndarray
    ):
    color1 = color1.astype(np.int16)
    color1 = color1.astype(np.int16)
    diffs = color2 - color1
    diffs[0] = np.abs(diffs[0])
    if (diffs[0] > 180):
        diffs[0] = 360 - diffs[0]
    return np.sqrt(diffs[0]**2 + diffs[1]**2 + diffs[2]**2)

@njit
@cc.export('find_nearest_label_njit', 'u1[:](u1[:], u1[:, :])')
def find_nearest_label_njit(
        req_color_hsv : np.ndarray,
        label_colors_hsv : np.ndarray
    ):
    i = 0
    min_index = 0
    min_diff = 1000000.
    while i < label_colors_hsv.shape[0]:
        diff = color_diff_hsv_njit(req_color_hsv, label_colors_hsv[i])
        if diff < min_diff:
            min_diff = diff
            min_index = i
        i += 1
    return min_index

@njit
@cc.export('perform_discretion_njit', 'u1[:, :, :](u1[:, :, :], u1[:, :], u1[:, :])')
def perform_discretion_njit(
        img_cv_hsv: cv.Mat,
        label_colors_rgb : np.ndarray,
        label_colors_hsv : np.ndarray
    ):
    img_labeled_rgb = np.zeros(img_cv_hsv.shape,
        dtype=np.uint8)
    for y in range(img_cv_hsv.shape[0]):
        for x in range(img_cv_hsv.shape[1]):
            label_id = find_nearest_label_njit(
                img_cv_hsv[y][x],
                label_colors_hsv
            )
            label_color = label_colors_rgb[label_id]
            img_labeled_rgb[y][x] = label_color
    return img_labeled_rgb


class ImageDiscreter(object):
    def __init__(self,
            output_image_shape : Tuple = (128, 128)
        ):
        self._output_image_shape = output_image_shape

        self._label_colors_rgb = np.uint8([
                [0, 255, 0], # green for grass
                [105, 105, 105], # gray for road
                [0, 0, 255], # blue for buildings
                [200, 220, 255] # lightblue for sky
            ])
        self._label_colors_hsv = cv.cvtColor(
            np.uint8([self._label_colors_rgb]),
            cv.COLOR_RGB2HSV
        )[0]
        self._bridge = CvBridge()


    def resize(self, img : np.ndarray):
        if (img.shape[0] < self._output_image_shape[0] or
                img.shape[1] < self._output_image_shape[1]):
            return img
        required_height_factor = self._output_image_shape[0] / img.shape[0]
        required_width_factor = self._output_image_shape[1] / img.shape[1]
        size_factor = max(required_height_factor,
            required_width_factor)
        shape_after_factor = (math.ceil(img.shape[1] * size_factor),
            math.ceil(img.shape[0] * size_factor))
        resized_img = cv.resize(img, shape_after_factor, interpolation=cv.INTER_NEAREST)
        return resized_img
        cropped_img = resized_img[
            resized_img.shape[0] // 2 - self._output_image_shape[0] // 2 :
            resized_img.shape[0] // 2 + self._output_image_shape[0] // 2,
            resized_img.shape[1] // 2 - self._output_image_shape[1] // 2 :
            resized_img.shape[1] // 2 + self._output_image_shape[1] // 2
        ]
        return cropped_img


    def unresize(self, img : np.ndarray, shape : Tuple):
        return cv.resize(img, (shape[1], shape[0]), interpolation=cv.INTER_NEAREST)


    def input_image_callback(self, imgmsg: Image):
        img = self._bridge.imgmsg_to_cv2(imgmsg, 'rgb8')

        img_labeled = self.perform_discretion(img)

        labeled_imgmsg = self._bridge.cv2_to_imgmsg(
            img_labeled, 'rgb8')
        labeled_imgmsg.header = Header(
            stamp = imgmsg.header.stamp
        )
        self._output_image_labeled_pub.publish(labeled_imgmsg)
        rospy.logdebug(f'Transformed: {labeled_imgmsg.header.stamp}')


    def perform_discretion(self, img : np.ndarray):
        img_cv_rgb = self.resize(
            img
        )
        img_cv_hsv = cv.cvtColor(img_cv_rgb, cv.COLOR_RGB2HSV)
        img_labeled_rgb = perform_discretion_njit(
            img_cv_hsv, self._label_colors_rgb,
            self._label_colors_hsv
        )
        unresized_img_labeled_color = self.unresize(img_labeled_rgb,
            img.shape)
        return unresized_img_labeled_color


    def color_diff_hsv(self, color1 : np.ndarray,
            color2 : np.ndarray):
        color1 = np.array(color1, dtype=np.int16)
        color2 = np.array(color2, dtype=np.int16)
        diffs = color2 - color1
        diffs[0] = np.abs(diffs[0])
        if (diffs[0] > 180):
            diffs[0] = 360 - diffs[0]
        return np.linalg.norm(diffs)


    def find_nearest_label(self, req_color_hsv : np.ndarray):
        index_of_nearest_color = min(
            [
                (i, self.color_diff_hsv(req_color_hsv, label_color) )
                    for i, label_color in enumerate(self._label_colors_hsv)
            ],
            key=lambda tup : tup[1]
        )[0]
        return index_of_nearest_color
    

    def perform_discretion_multi(self, warp_dataset : WarpDataSet):
        discreted : List[cv.Mat] = []
        for warp_data in warp_dataset.warp_datas:
            discreted.append(self.perform_discretion(warp_data._rgb_img))
        return discreted


def main():
    rospy.init_node('image_discreter',
        log_level=rospy.DEBUG)
    discreter = ImageDiscreter()
    rospy.spin()


if __name__ == '__main__':
    main()
