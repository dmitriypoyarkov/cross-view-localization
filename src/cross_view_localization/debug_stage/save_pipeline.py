import os
import pickle

import cv2 as cv
import numpy as np
import rospy
from cv2.typing import MatLike
from std_msgs.msg import Empty

from cross_view_localization.system.system import CVLSystem
from cross_view_localization.tools.warp_data import LandImg


class PipelineSaveDebug(object):
    def __init__(self, system : CVLSystem, output_folder : str):
        self._system = system
        self._output_folder = output_folder


    def subscribe(self, topic : str):
        self._save_req_sub = rospy.Subscriber(topic,
            Empty, self.save_req_callback, queue_size=1)


    def save_req_callback(self, msg : Empty):
        rospy.logdebug('CVL: PIPELINE SAVE REQUESTED')
        try:
            path = self.save_pipeline()
            rospy.logdebug(f'CVL: PIPELINE SAVED TO {path}')
        except Exception as e:
            rospy.logerr(f'CVL: PIPELNIE NOT SAVED: {e}')


    def count_folders_in_folder(self, folder_path : str):
        count = 0
        for item in os.listdir(folder_path):
            item_path = os.path.join(folder_path, item)
            if os.path.isdir(item_path):
                count += 1
        return count


    def make_unique(self, base_folder, folder_name):
        try:
            folder_names = set(os.listdir(base_folder))
        except OSError:
            return folder_name
        new_folder_name = folder_name
        counter = 1
        while new_folder_name in folder_names:
            new_folder_name = f"{folder_name}_{counter}"
            counter += 1
        return new_folder_name


    def save_rgb(self, path : str, name : str, img : MatLike):
        rgb = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        cv.imwrite(os.path.join(path, name), rgb)


    def save_colormasks(self, path, land_img : LandImg, name : str):
        for color in land_img.colors_sorted:
            mask = land_img.get_mask_by_color(color)
            colored_mask = np.zeros((mask.shape[0], mask.shape[1], 3), dtype=np.uint8)
            colored_mask[mask==255] = color
            self.save_rgb(path, f'{name}_{str(color)}.bmp', colored_mask)


    def save_npy(self, path : str, name : str, depth : MatLike):
        np.save(os.path.join(path, name), depth)


    def save_landimg(self, path, name, landimg : LandImg):
        with open(os.path.join(path, name), 'wb') as f:
            pickle.dump(landimg, f)


    def save_pipeline(self):
        pipeline = self._system.last_pipeline
        if pipeline is None:
            return
        pipeline_name = 'pipeline'
        pipeline_name = self.make_unique(self._output_folder, pipeline_name)
        pipeline_path = os.path.join(self._output_folder, pipeline_name)
        os.makedirs(pipeline_path)

        warp_data_set = pipeline.warp_data

        for i, warp_data in enumerate(warp_data_set.warp_datas):
            if warp_data.segmented_img is None:
                raise Exception('CVL: For some reason, segmented_img was not set for warp_data')
            self.save_rgb(pipeline_path, f'rgb_{i}.bmp', warp_data.rgb_img)
            # self.save_rgb(pipeline_path, f'depth_{i}.bmp', warp_data.depth)
            self.save_rgb(pipeline_path, f'seg_{i}.bmp', warp_data.segmented_img)

            self.save_npy(pipeline_path, f'depth_{i}.npy', warp_data.depth)

        self.save_landimg(pipeline_path, f'proc.pkl', pipeline.processed_img)

        self.save_colormasks(pipeline_path, pipeline.warped_img, 'warped')
        self.save_colormasks(pipeline_path, pipeline.processed_img, 'processed')

        self.save_rgb(pipeline_path, f'processed.bmp', pipeline.processed_img.img)
        self.save_rgb(pipeline_path, f'warped.bmp', pipeline.warped_img.img)