import math
import pickle
import sys
import time

import cv2 as cv
import numpy as np
import pygame
from cv2.typing import MatLike

import cross_view_localization.package_settings as settings
from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.tools.satellite_image_provider import \
    SatelliteImageProvider
from cross_view_localization.tools.warp_data import LandImg


def cv2_to_pygame(image : MatLike):
    image = cv.cvtColor(
        np.transpose(image, axes=(1, 0, 2)),
        cv.COLOR_BGR2RGB
    )
    pygame_image = pygame.surfarray.make_surface(image)
    return pygame_image

def find_local_maxima(image):
    # Define the structure for local maxima detection
    kernel = np.ones((3, 3), np.uint8)

    # Dilate the image
    dilated = cv.dilate(image, kernel)

    # Create a mask of the local maxima
    local_maxima = (image == dilated)

    # Exclude the borders by setting them to False
    local_maxima[:, 0] = local_maxima[:, -1] = False
    local_maxima[0, :] = local_maxima[-1, :] = False

    return local_maxima

def mark_maxima(image, maxima):
    # Convert the grayscale image to a BGR image
    colored_image = cv.cvtColor(image, cv.COLOR_GRAY2BGR)

    # Mark the local maxima with red color
    colored_image[maxima] = [0, 0, 255]  # BGR for red color

    return colored_image

if __name__ == '__main__':
    proc_path = settings.output_path('saved_pipelines/pipeline_8/proc.pkl')
    # warp_path = settings.output_path('saved_pipelines/pipeline_5/warped.bmp')
    with open(proc_path, 'rb') as f:
        proc_img : LandImg = pickle.load(f)
    angle = -np.pi / 2
    sat_img = SatelliteImage.from_file(settings.area_path('building_190', 'sat_img.bmp'))
    sat_provider = SatelliteImageProvider(sat_img)

    sat_provider.prepare_img(proc_img)

    start = [
        int(proc_img.size // 2 + 1 + 0.0 * sat_img.img.shape[0]),
        int(proc_img.size // 2 + 1 + 0.3 * sat_img.img.shape[0]),
    ]
    end = [
        int(sat_img.img.shape[0] - (proc_img.size // 2 + 1) - 0.5 * sat_img.img.shape[0]),
        int(sat_img.img.shape[1] - (proc_img.size // 2 + 1) - 0.0 * sat_img.img.shape[1]),
    ]

    print(f'prepared_size: {sat_provider._prepared_img.img.shape}')
    NUM_IMAGES = 30

    fun_imgs = []
    for phi in np.arange(-np.pi, np.pi, 2 * np.pi / NUM_IMAGES):
        fun_img = np.zeros((sat_img.img.shape[0], sat_img.img.shape[1]), dtype=np.uint8)
        for y in range(start[0], end[0], 1):
            for x in range(start[1], end[1], 1):
                val = sat_provider.compare_at_coord_inplace(x, y, phi)
                fun_img[y, x] = int(max(0, val * 255))
        fun_imgs.append(fun_img)
        print(f'img {phi} added')
    pygame.init()

    marked_imgs = [mark_maxima(image, find_local_maxima(image)) for image in fun_imgs]

    pygame_imgs = [cv2_to_pygame(img) for img in marked_imgs]
    SCREEN_WIDTH, SCREEN_HEIGHT = 800, 600
    SLIDER_WIDTH, SLIDER_HEIGHT = 500, 50
    IMAGE_SIZE = (fun_imgs[0].shape[0], fun_imgs[0].shape[1])  # Size of each image

    # Create screen
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption('Image Slider')

    # Slider
    slider_rect = pygame.Rect((SCREEN_WIDTH - SLIDER_WIDTH) // 2, SCREEN_HEIGHT - 100, SLIDER_WIDTH, SLIDER_HEIGHT)
    slider_pos = slider_rect.x

    def get_image_index(slider_x):
        ratio = (slider_x - slider_rect.x) / slider_rect.width
        return min(NUM_IMAGES - 1, max(0, int(ratio * NUM_IMAGES)))

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN and slider_rect.collidepoint(event.pos):
                slider_pos = event.pos[0]
            elif event.type == pygame.MOUSEMOTION and event.buttons[0]:
                if slider_rect.collidepoint(event.pos):
                    slider_pos = event.pos[0]

        slider_pos = min(max(slider_pos, slider_rect.x), slider_rect.x + slider_rect.width)
        image_index = get_image_index(slider_pos)
        print(f'Image index: {image_index}')
        screen.fill((255, 255, 255))
        screen.blit(pygame_imgs[image_index], ((SCREEN_WIDTH - IMAGE_SIZE[0]) // 2, (SCREEN_HEIGHT - IMAGE_SIZE[1]) // 2))

        pygame.draw.rect(screen, (0, 0, 0), slider_rect, 2)
        pygame.draw.rect(screen, (150, 150, 150), (slider_pos - 5, slider_rect.y - 5, 10, slider_rect.height + 10))

        pygame.display.flip()
        time.sleep(0.1)

    pygame.quit()
    sys.exit()
