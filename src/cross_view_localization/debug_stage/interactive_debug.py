import cv2 as cv
from cross_view_localization.comparison.processed_image_comparer import ProcessedImageComparer

from cross_view_localization.tools.satellite_image_provider import SatelliteImageProvider
from imutils import rotate_bound
import numpy as np
import rospy


class InteractiveDebug(object):
    def __init__(self, sat_provider : SatelliteImageProvider,
            comparer : ProcessedImageComparer):
        self._angle = 0
        self._sat_provider = sat_provider
        self._comparer = comparer
        self._mouse_position = None
        self._sample = None

        self._mode = "BASIC"
        

    def set_small_img(self, small_img):
        self._sample = small_img


    def display(self):
        cv.namedWindow("Big Image")
        cv.setMouseCallback("Big Image", self.mouse_callback)
        while True:
            cv.waitKey(10)
            if self._sample is None:
                cv.waitKey(500)
                continue
            temp_img = self._sat_provider._img.copy()
            if self._mouse_position is not None:
                x, y = self._mouse_position
                
                if self._mode == "BASIC":
                    temp_img = self.paste_small_image(temp_img, self._sample, x, y, self._angle)
                elif self._mode == "MATCHED_PIXELS":
                    matched_pixels = self._sat_provider.get_matched_pixels(self._sample,
                        x, y, self._angle)
                    temp_img = self.paste_small_image(temp_img, matched_pixels, x, y, 0.)


                rotated_sample = self._sat_provider.rotate_sample(self._sample, self._angle)
                rospy.logdebug('ROTATING DONE')
                colors, color_amounts = self._comparer.get_color_amounts(rotated_sample, 80)
                rospy.logdebug('COLOR AMOUNTS DONE')
                metric = self._sat_provider.compare_at_coord(rotated_sample, x, y,
                    colors, color_amounts)
                rospy.logdebug('PUT DONE')

                self.put_text_top_right(temp_img, round(metric, 2))
            
            cv.imshow("Big Image", temp_img)


            key = cv.waitKey(1) & 0xFF
            if key == ord("a"):
                self._angle = (self._angle + 0.1 + np.pi) % (2 * np.pi) - np.pi
            elif key == ord("d"):
                self._angle = (self._angle - 0.1 + np.pi) % (2 * np.pi) - np.pi
            elif key == ord("m"):
                if self._mode == "BASIC":
                    self._mode = "MATCHED_PIXELS"
                else:
                    self._mode = "BASIC"
            elif key == 27:  # Press ESC to exit
                break
        cv.destroyAllWindows()


    def put_text_top_right(self, img, text):
        font = cv.FONT_HERSHEY_SIMPLEX
        font_scale = 1
        color = (255, 255, 255) # white

        # Get the size of the text
        text_size, _ = cv.getTextSize(str(text), font, font_scale, thickness=1)

        # Calculate the position of the text
        text_x = img.shape[1] - text_size[0] - 10 # 10 is a margin
        text_y = text_size[1] + 10 # 10 is a margin

        # Put the text on the img
        cv.putText(img, str(text), (text_x, text_y), font, font_scale, color, thickness=1)


    def mouse_callback(self, event, x, y, flags, param):
        if event == cv.EVENT_LBUTTONDOWN:
            self._mouse_position = (x, y)


    def get_paste_position(self, x, y, img):
        small_h, small_w = img.shape[:2]
        big_h, big_w = self._sat_provider.img_shape[:2]
        paste_x = x - small_w//2
        paste_y = y - small_h//2
        if paste_x < 0:
            paste_x = 0
        elif paste_x+small_w > big_w:
            paste_x = big_w - small_w
        if paste_y < 0:
            paste_y = 0
        elif paste_y+small_h > big_h:
            paste_y = big_h - small_h
        return paste_x, paste_y


    def paste_small_image(self, img, small_img, x, y, angle):
        rotated = rotate_bound(small_img, -np.rad2deg(angle))
        x, y = self.get_paste_position(x, y, rotated)
        small_h, small_w = rotated.shape[:2]
        roi = img[y:y+small_h, x:x+small_w]
        mask = cv.cvtColor(rotated, cv.COLOR_BGR2GRAY)
        ret, mask_inv = cv.threshold(mask, 10, 255, cv.THRESH_BINARY_INV)
        img_bg = cv.bitwise_and(roi, roi, mask=mask_inv)
        small_fg = cv.bitwise_and(rotated, rotated, mask=mask)
        dst = cv.add(img_bg, small_fg)
        img[y:y+small_h, x:x+small_w] = dst
        return img