import os

import cv2
import rospy

from cross_view_localization.debug_stage.image_searcher import ImageSearcher
from cross_view_localization.tools.warp_data import WarpDataSet


class DebugImgLoader(object):
    def __init__(self, folder_path):
        self._last_timestamp = None

        self._seg_imgs = {}
        self._img_searcher = ImageSearcher(folder_path, ['/ritrover/oakd_fr', '/ritrover/oakd_fl', '/ritrover/oakd_back'])
        if self._img_searcher.get_img_count() == 0:
            raise RuntimeError(f'CVL: Debug Image Loader is used, but no images were found in {folder_path}.')
        # Iterate through files in the folder
        # for filename in os.listdir(folder_path):
        #     file_path = os.path.join(folder_path, filename)

        #     # Check if the file is a PNG image
        #     if filename.endswith('.png'):
        #         # Split the filename into camera name and timestamp parts
        #         parts = filename.split('_')

        #         camera_name = f'{parts[0]}_{parts[1]}'
        #         timestamp = rospy.Time(
        #             int(parts[2].split('.')[0])/1000000000
        #         )  # Remove the file extension (.png)

        #         # Create a dictionary for the camera name if it doesn't exist
        #         if camera_name not in self._seg_imgs:
        #             self._seg_imgs[camera_name] = {}

        #         # Store the filename in the dictionary based on the timestamp
        #         self._seg_imgs[camera_name][timestamp] = file_path
        # self._timestamps = list(self._seg_imgs['oakd_fl'].keys())
        # print(f'TIMESTAMPS {type(self._timestamps[0])}')


    def load_img(self, filename):
        img = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
        return img


    def load_segmented(self, warp_dataset : WarpDataSet):
        # if not self._seg_imgs['oakd_fl'].get(ts):
        #     print(f'Not found seg for {ts.to_sec()}')
        #     ts = self.closest_timestamp(ts)
        #     print(f'Closest ts: {ts.to_sec()}')


        # files = [self._seg_imgs['oakd_fl'][ts], self._seg_imgs['oakd_fr'][ts], self._seg_imgs['oakd_back'][ts], ]
        # imgs = [self.load_img(file) for file in files]
        # return imgs
        ts = warp_dataset.warp_datas[0].timestamp
        segmented = self._img_searcher.get_closest_images_for_timestamp(ts.to_nsec())

        seg_imgs = []
        for warp_data in warp_dataset.warp_datas:
            file = segmented[warp_data._cam_frame]
            seg_imgs.append(self.load_img(file))

        return seg_imgs
