#!/usr/bin/env python

import cv2
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

from cross_view_localization import package_settings as settings


class PathBuilder:
    def __init__(self):
        self.image_path = settings.area_path('building_190', 'sat_img.bmp')
        self.bridge = CvBridge()
        self.current_frame_timestamp = None
        self.points = []
        self.image = cv2.imread(self.image_path)
        self.clone = self.image.copy()
        rospy.init_node('path_builder', anonymous=True)
        self.subscriber = rospy.Subscriber('/ritrover/sensors/oakd_fr/rgb/image', Image, self.image_callback)
        self.window_name = 'Path Builder'
        cv2.namedWindow(self.window_name)
        # cv2.resizeWindow(self.window_name, 800, 800)
        cv2.setMouseCallback(self.window_name, self.click_event)

    def image_callback(self, msg):
        self.current_frame_timestamp = msg.header.stamp

    def click_event(self, event, x, y, flags, param):
        if event == cv2.EVENT_MBUTTONDOWN:
            if self.current_frame_timestamp:
                timestamp = self.current_frame_timestamp.to_nsec()
                existing_point = next((p for p in self.points if p[2] == timestamp), None)
                if existing_point:
                    self.points.remove(existing_point)
                new_point = (x, y, timestamp)
                self.points.append(new_point)
                print(f'new point {new_point}')
                self.points.sort(key=lambda p: p[2])
                self.draw_path()
            else:
                print('no timestamp')


    def draw_path(self):
        self.image = self.clone.copy()
        for i in range(1, len(self.points)):
            cv2.line(self.image, self.points[i-1][:2], self.points[i][:2], (0, 0, 255), 1)
        for point in self.points:
            cv2.circle(self.image, (point[0], point[1]), 2, (0, 0, 0), -1)
        cv2.imshow(self.window_name, self.image)


    def run(self):
        while not rospy.is_shutdown():
            cv2.imshow(self.window_name, self.image)
            key = cv2.waitKey(30) & 0xFF
            if key == 27:  # ESC key to break
                break
            if key == 8:  # Backspace key
                if self.points:
                    self.points.pop()
                    self.image = self.clone.copy()
                    self.draw_path()
        self.save_path_data()
        cv2.destroyAllWindows()

    def save_path_data(self):
        with open("path_data.txt", "w") as f:
            for point in self.points:
                f.write(f"{point[0]} {point[1]} {point[2]}\n")

if __name__ == '__main__':
    path_builder = PathBuilder()
    path_builder.run()
