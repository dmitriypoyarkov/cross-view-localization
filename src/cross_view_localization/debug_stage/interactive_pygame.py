import math
import os
import signal
import sys
from typing import Callable, List, Tuple

import cv2 as cv
import numpy as np
import pygame
import rospy
from cv2.typing import MatLike
from imutils import rotate_bound

from cross_view_localization.comparison.processed_image_comparer import \
    ProcessedImageComparer
from cross_view_localization.debug_stage.button import Button
from cross_view_localization.plugins.cvl_comp_plugin import CVLCompPlugin
from cross_view_localization.tools.image_rotation_cacher import ImageCache
from cross_view_localization.tools.odometry_provider import OdometryProvider
from cross_view_localization.tools.satellite_image_provider import \
    SatelliteImageProvider


class InteractivePygame(object):
    def __init__(
            self,
            sat_provider : CVLCompPlugin,
            init_pose_fun : Callable,
            ref_coords : np.ndarray = np.array([]),
        ):
        self._sat_provider = sat_provider
        self._ref_coords = ref_coords
        self._mouse_position = None
        self._sample = None

        self._mode = "BASIC"
        self._robot_trans = np.array([0.,0.])
        self._robot_phi = 0.

        self._warp_img_pos = None

        pygame.init()
        icon_surf = pygame.image.load(os.path.join(os.path.dirname(__file__), "logo1.png"))
        pygame.display.set_icon(icon_surf)
        self._screen_size = (800, 800)
        self._screen = pygame.display.set_mode(self._screen_size)
        pygame.display.set_caption("Cross-View Localization")

        self.TRIGNALE_VERTICES = np.array([
            [0, -10],  # Top vertex
            [-5, 10],  # Bottom-left vertex
            [5, 10],  # Bottom-right vertex
        ])
        self.ROBOT_COLOR = (255, 0, 0)
        self.ROBOT_ODOM_COLOR = (255, 255, 0)
        self.BACKGROUND_COLOR = (255, 255, 255)
        self.ARROW_COLOR = (255, 0, 0)
        self.ROBOT_REAL_COLOR = (0, 230, 250)
        self.ARROW_LENGTH = 30
        self.ARROW_WIDTH = 5

        self.FONT = pygame.font.Font(None, 36)
        screen_width, screen_height = 400, 400
        self._metrics_panel = pygame.Surface((screen_width, screen_height))
        self._metrics = None

        self._pygame_sat_img = self.from_cv(self._sat_provider.get_sat_img())
        self._warp_img = None
        self._warp_img_cache = None
        self._matched_pixels_img = None
        self._odometry_trans, self._odometry_phi = np.array([0., 0.]), 0.
        self._odom_origin_trans, self._odom_origin_rot = np.array([0., 0.]), 0.
        self._odometry_positions = [self._odometry_trans]
        self._pf_positions = []

        self._warp_img_phi = 0.

        self._draw_particles = True
        self._particles = None
        self._estimation = None

        self._triangle_surface = pygame.Surface((screen_width, screen_height), pygame.SRCALPHA)

        # Define button properties
        BUTTON_WIDTH = 150
        BUTTON_HEIGHT = 50
        BUTTON_MARGIN = 20
        PANEL_WIDTH = BUTTON_WIDTH + BUTTON_MARGIN * 2
        PANEL_HEIGHT = screen_height
        PANEL_X = screen_width - PANEL_WIDTH
        PANEL_Y = 0
        self._buttons : List[Button] = []
        self._button_panel = self.create_button_panel()
        self._panel_surface, self._panel_rect = self.create_button_panel()
        button = Button(
            self._panel_rect.left + BUTTON_MARGIN,
            self._panel_rect.top + BUTTON_MARGIN,
            BUTTON_WIDTH, BUTTON_HEIGHT, (0, 255, 0),
            text="INIT POSE", onclick=self.init_pos_btn
        )
        self._buttons.append(button)

        self._aim_icon = pygame.image.load(os.path.join(os.path.dirname(__file__), 'red_aim.png'))
        self._init_pose_state = False

        self._odom_steps_predefined = None

        self._init_pos_call = init_pose_fun

        self._paint_mode = False
        self._painted_points = []

        self._pre_exit_actions = []

        self._localized_path = []

        self._timestamp = 0
        # pygame.draw.polygon(self._triangle_surface, )


    @property
    def warp_img_phi(self):
        return self._warp_img_phi
    
    @warp_img_phi.setter
    def warp_img_phi(self, value):
        self._warp_img_phi = self.normalize_angle(value)

    
    def add_pre_exit_action(self, action : Callable):
        self._pre_exit_actions.append(action)


    def enable_paint_mode(self):
        self._paint_mode = True


    def set_odom_steps_predefined(self, odom_steps : np.ndarray):
        # self._odom_steps_predefined = odom_steps
        if len(odom_steps) == 0:
            return
        timeless_steps = (np.array(odom_steps))[:, 1:3]
        self._odom_steps_predefined = self.rotate_coordinates_numpy(timeless_steps, self._odom_origin_rot)
        self._odom_steps_predefined *= self._odom_provider._odom_factor
        self._odom_steps_with_time = np.concatenate(
            (
                odom_steps[:, 0:1],
                self._odom_steps_predefined[:, 0:1],
                self._odom_steps_predefined[:, 1:2]
            ), axis=1
        )

    
    def set_odom_steps_predefined_(self, odom_steps : np.ndarray):
        fdslga;


    def get_pos_from_timed_list(self, timed_list, timestamp):
        time_column = timed_list[:, 0]
        try:
            nsec = timestamp.to_nsec()
        except AttributeError:
            nsec = timestamp
        closest_index = np.argmin(np.abs(time_column - nsec))

        odom_pos = timed_list[closest_index, :]
        try:
            prev_pos = timed_list[closest_index - 1, :]
        except IndexError:
            prev_pos = odom_pos
        phi = self.calculate_angle(prev_pos[1:3], odom_pos[1:3])
        full_coord = np.array([odom_pos[0], odom_pos[1], odom_pos[2], phi])
        return full_coord

    def get_real_pos(self, timestamp):
        return self.get_pos_from_timed_list(self._ref_coords, timestamp)


    def get_odom_pos(self, timestamp : rospy.Time):
        return self.get_pos_from_timed_list(self._odom_steps_with_time, timestamp)


    def set_odometry_origin(self, trans : np.ndarray, rot : float):
        self._odom_origin_trans = trans
        self._odom_origin_rot = rot


    def save_odometry_steps(self, file):
        steps = np.array(self._odometry_positions)
        np.save('/home/fc/catkin_ws/src/cross_view_localization/launch/odom_steps.npy', steps)


    def init_pos_btn(self, af):
        self._init_pose_state = True
        bitmask = []
        pygame_array = pygame.surfarray.array3d(self._aim_icon)
        # Convert the image to grayscale
        gray_image = np.dot(pygame_array[..., :3], [0.2989, 0.5870, 0.1140]).astype(np.uint8)

        # Define a function to create a bitmask from the grayscale image

        bitmask = []
        bitmask_reverse = []

        height, width = gray_image.shape[:2]
        gray_image = gray_image.flatten()

        for i in range(0, len(gray_image), 8):
            # Extract 8 pixels' alpha values
            pixels = gray_image[i:i+8]

            # Convert alpha values to binary string and then to integer
            bitmask_int = int(''.join(['1' if pixel > 0 else '0' for pixel in pixels]), 2)
            bitmask_reverse_int = int(''.join(['0' if pixel > 0 else '1' for pixel in pixels]), 2)
            bitmask_reverse.append(bitmask_reverse_int)
            bitmask.append(bitmask_int)
        
        pygame.mouse.set_cursor(
            (self._aim_icon.get_width(), self._aim_icon.get_height()),
            (self._aim_icon.get_height() // 2, self._aim_icon.get_height() // 2),
            bitmask,
            bitmask,
        )


    def draw_map(self):
        self._screen.blit(self._pygame_sat_img, (0, 0))
        # if self._warp_img:
        #     self.paste_small_image(self._warp_img, self._robot_trans[0],
        #         self._robot_trans[1], -self._robot_phi)
        self.draw_odometry_steps()
        self.draw_metrics()

        self.draw_ref_path()


    def draw_ref_path(self):
        if self._ref_coords is None or len(self._ref_coords) == 0:
            return
        path = [coord[1:] for coord in self._ref_coords]
        pygame.draw.lines(self._screen, self.ROBOT_REAL_COLOR, False, path, 4)


    def draw_localized_path(self):
        for point in self._localized_path:
            pygame.draw.circle(self._screen, (255, 0, 0), (point[0], point[1]), 3)


    def calculate_angle(self, p2, p1):
        vector = (p2[0] - p1[0], p2[1] - p1[1])
        angle = math.atan2(vector[0], vector[1])

        return angle


    def run(self):
        # Game loop
        signal.signal(signal.SIGINT, self.sigterm_handler)
        running = True
        while running:
            # Clear the screen
            self._screen.fill(self.BACKGROUND_COLOR)

            self.draw_map()

            for button in self._buttons:
                self._screen.blit(button.surface, button.rect)
                self._screen.blit(button.text_surface, button.text_rect)

            # pygame.draw.rect(self._screen, (100, 100, 100), (PANEL_X, PANEL_Y, PANEL_WIDTH, PANEL_HEIGHT))
            self._screen.blit(self._panel_surface, self._panel_rect)
            if self._draw_particles and self._particles is not None:
                self.draw_particles()
            # if self._robot_trans is not None:
            #     odom_pose = self.calculate_odom_pose()
            #     self.draw_robot_odom(odom_pose[0], odom_pose[1], odom_pose[2])

            self.draw_painted_path()
            self.draw_localized_path()
            if self._odom_steps_predefined is not None and len(self._odom_steps_predefined) != 0:
                odom_pos = self.get_odom_pos(self._timestamp)
                pos_abs = self.to_odom_space(odom_pos[1:3])
                self.draw_robot_odom(pos_abs[0], pos_abs[1], odom_pos[3])
            if self._estimation is not None:
                self.draw_robot(self._estimation[0], self._estimation[1], self._estimation[2])
            if self._ref_coords is not None and len(self._ref_coords) != 0:
                pos = self.get_real_pos(self._timestamp)
                self.draw_robot_real(pos[1], pos[2], pos[3])

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 4:
                        self.warp_img_phi += 0.05
                    if event.button == 5:
                        self.warp_img_phi -= 0.05
                    self.handle_mouse_click()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_m:
                    self.toggle_mode()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_p:
                    self._draw_particles = not self._draw_particles
                for button in self._buttons:
                    button.handle_event(event)
            # Update the display
            pygame.display.flip()
        for action in self._pre_exit_actions:
            action()
        pygame.quit()
        sys.exit()


    def create_button_panel(self):
        panel_surface = pygame.Surface((200, self._screen.get_height()))
        panel_surface.fill((50, 50, 50))
        panel_rect = panel_surface.get_rect(topleft=(600, 0))

        return panel_surface, panel_rect


    def create_button(self, x, y, width, height, text):
        button_surface = pygame.Surface((width, height))
        button_surface.fill((100, 100, 100))
        button_rect = button_surface.get_rect(topleft=(x, y))

        font = pygame.font.Font(None, 24)  # Font object with size 24
        text_surface = font.render(text, True, (0,0,0))
        text_rect = text_surface.get_rect(center=button_rect.center)

        return button_surface, button_rect, text_surface, text_rect


    def draw_painted_path(self):
        for point in self._painted_points:
            pygame.draw.circle(self._screen, (255, 0, 0), (point[0], point[1]), 5)


    def save_painted_path(self):
        np.save('/home/fc/catkin_ws/src/cross_view_localization/launch/painted_path.npy',
                np.array(self._painted_points))
        print('PAINTED PATH SAVED')


    def put_point(self, x, y):
        # pygame.draw.circle(self._screen, (0, 150, 150), (x, y), 3)
        self._painted_points.append([x, y])


    def handle_mouse_click(self):
        x, y = pygame.mouse.get_pos()
        if self._paint_mode:
            self.put_point(x, y)
        if self._init_pose_state:
            self._init_pos_call(x, y)
            self._init_pose_state = False
            pygame.mouse.set_cursor(*pygame.cursors.arrow)
        if self._warp_img_cache:
            self._metrics = self._sat_provider.compare_at_coord_inplace(
                self._warp_img_cv,
                x, y, self._warp_img_phi,
                self._warp_img_cache.valued_pixels
            )
            self._similarity = self._sat_provider.get_similarity_inplace(
                self._warp_img_cv,
                x, y, self._warp_img_phi,
                self._warp_img_cache.valued_pixels
            )
        self._warp_img_pos = x, y
        if self._mode == "MATCHED_PIXELS" and self._warp_img:
            try:
                self._matched_pixels_img = self.from_cv(
                    self._sat_provider.get_matched_pixels(
                        self._warp_img_cv,
                        x, y, self._warp_img_phi
                    )
                )
            except ValueError as e:
                print(f'MATCHED PIXELS ERROR: {e}')


    def sigterm_handler(self, signum, frame):
        pygame.quit()  
        print('Pygame received sigterm')
        rospy.signal_shutdown('SIGINT')
        sys.exit(0)    # Exit the program


    def normalize_angle(self, angle):
        # Normalize the angle to be in the range [-pi, pi]
        while angle > math.pi:
            angle -= 2 * math.pi
        while angle < -math.pi:
            angle += 2 * math.pi
        return angle


    def draw_metrics(self):
        if not self._metrics:
            return
        self._metrics_panel.fill((255, 255, 255))
        text = [
            f'Metrics: {self._metrics:.2}',
            f'Valued Pixels: {self._warp_img_cache.valued_pixels}',
            f'Similarity: {self._similarity}',
        ]
        for i, line in enumerate(text):
            line_rendered = self.FONT.render(
                line, True, (0, 0, 0)
            )
            self._metrics_panel.blit(
                line_rendered, 
                line_rendered.get_rect(
                    center=(
                        self._metrics_panel.get_size()[1] // 2,
                        40 * (1 + i),
                    )
                )
            )
        self._screen.blit(self._metrics_panel, (self._sat_provider.img_shape[0], 0))


    def toggle_mode(self):
        if self._mode == "BASIC":
            self._mode = "MATCHED_PIXELS"
        else:
            self._mode = "BASIC"
        rospy.logdebug(f'NEW MODE: {self._mode}')


    def draw_arrow_at(self,
            x : float,
            y : float,
            phi : float,
            color : Tuple[int, int, int],
            size=1.,
            outline=False):
        rotated_triangle = []
        vertices = [size * vertex for vertex in self.TRIGNALE_VERTICES]
        for vertex in vertices:
            new_x = vertex[0] * math.cos(phi) + vertex[1] * math.sin(phi)
            new_y = - vertex[0] * math.sin(phi) + vertex[1] * math.cos(phi)
            # x, y = self.convert([x, y])
            rotated_triangle.append((x + new_x, y + new_y))

        # Draw the rotated triangle on the screen
        pygame.draw.polygon(self._screen, color, rotated_triangle)
        if outline:
            pygame.draw.polygon(self._screen, (0, 0, 0), rotated_triangle, width=3)


    def draw_arrow_from_to(self, end_point, start_point, color=(255, 255, 255)):
        # Calculate the angle of the arrow
        angle = math.atan2(start_point[1] - end_point[1], start_point[0] - end_point[0])

        # Calculate the points for the arrow triangle

        arrowhead1 = (start_point[0] - 5 * math.cos(angle - math.pi / 2), 
                    start_point[1] - 5 * math.sin(angle - math.pi / 2))
        
        arrowhead2 = (start_point[0] - 5 * math.cos(angle + math.pi / 2), 
                    start_point[1] - 5 * math.sin(angle + math.pi / 2))
        
        # Draw the start and end points
        # pygame.draw.circle(self._screen, (255, 0, 0), start_point, 5)  # Red circle for start point
        # pygame.draw.circle(self._screen, (255, 0, 0), end_point, 5)    # Red circle for end point
        
        # Draw the arrow triangle
        pygame.draw.polygon(self._screen, color, (end_point, arrowhead1, arrowhead2))


    def draw_robot(self, x, y, phi):
        self.draw_arrow_at(x, y, phi, self.ROBOT_COLOR, outline=True)


    def draw_robot_odom(self, x, y, phi):
        self.draw_arrow_at(x, y, phi, self.ROBOT_ODOM_COLOR, outline=True)

    
    def draw_robot_real(self, x, y, phi):
        self.draw_arrow_at(x, y, phi, self.ROBOT_REAL_COLOR, outline=True)


    def set_small_img(self, small_img):
        self._sample = small_img


    def put_text_top_right(self, img, text):
        font = cv.FONT_HERSHEY_SIMPLEX
        font_scale = 1
        color = (255, 255, 255) # white

        # Get the size of the text
        text_size, _ = cv.getTextSize(str(text), font, font_scale, thickness=1)

        # Calculate the position of the text
        text_x = img.shape[1] - text_size[0] - 10 # 10 is a margin
        text_y = text_size[1] + 10 # 10 is a margin

        # Put the text on the img
        cv.putText(img, str(text), (text_x, text_y), font, font_scale, color, thickness=1)


    def mouse_callback(self, event, x, y, flags, param):
        if event == cv.EVENT_LBUTTONDOWN:
            self._mouse_position = (x, y)


    def paste_small_image(self, small_img : np.ndarray, x, y, angle):
        rotated_image = pygame.transform.rotate(small_img, -math.degrees(angle))
        self._screen.blit(
            rotated_image,
            (x - rotated_image.get_width()/2,
             y - rotated_image.get_height()/2)
        )
    
    # robot pose in img coordinates, x y
    def set_robot_pose(self, robot_trans : np.ndarray, phi : float):
        self._robot_trans = robot_trans
        # self._robot_trans[0], self._robot_trans[1] = self._robot_trans[1], self._robot_trans[0]
        self._robot_phi = phi

    
    def calculate_odom_pose(self):
        coord = self.rotate_coordinate(self._odometry_trans, self._odom_origin_rot)
        coord += self._odom_origin_trans
        phi = self._odometry_phi
        return np.array([coord[0], coord[1], phi])


    def set_warp_image(self, warp_img : ImageCache):
        self._warp_img_cache = ImageCache(warp_img, 0.)
        self._warp_img_cv = warp_img
        self._warp_img_transposed = cv.transpose(warp_img)
        self._warp_img = self.from_cv(self._warp_img_cv)


    def from_cv(self, img : MatLike):
        return pygame.surfarray.make_surface(
            cv.transpose(img)
        )


    def add_odometry_step(self, d : np.ndarray, delta_phi : float):
        # prev_trans, prev_phi = self._odometry_trans, self._odometry_phi
        new_trans = [self._odometry_trans[0] - d * math.sin(self._odometry_phi),
                     self._odometry_trans[1] - d * math.cos(self._odometry_phi)]
        new_phi = self._odometry_phi + delta_phi
        new_phi = math.atan2(
            math.sin(new_phi),
            math.cos(new_phi)
        )
        self._odometry_trans = new_trans
        self._odometry_phi = new_phi
        self._odometry_positions.append(new_trans)


    def draw_pf_positions(self):
        for pos in self._pf_positions:
            pygame.draw.circle(
                self._screen, (255, 255, 255),
                pos, 2.
            )


    def draw_odometry_steps(self):
        steps = self._odometry_positions
        if self._odom_steps_predefined is not None:
            steps = self._odom_steps_predefined

        for pos in steps:
            abs_pos = pos + self._odom_origin_trans
            pygame.draw.circle(
                self._screen, (255, 255, 0),
                abs_pos, 3.
            )
    

    def to_odom_space(self, coord):
        return coord + self._odom_origin_trans


    def rotate_coordinates_numpy(self, coordinates, angle_radians):
        rotation_matrix = np.array([[np.cos(angle_radians), -np.sin(angle_radians)],
                                    [np.sin(angle_radians), np.cos(angle_radians)]])

        rotated_coordinates = np.dot(rotation_matrix, np.array(coordinates).T).T
        return rotated_coordinates
    

    def rotate_coordinate(self, coordinate, angle_radians):
        return self.rotate_coordinates_numpy(np.array([coordinate]), angle_radians)[0]


    def set_particles(self, particles, weights, estimation):
        self._weights = weights

        self._particles = np.array([self.convert_from_sat(particle) for particle in particles])
        if estimation is not None:
            self._estimation = self.convert_from_sat(estimation)
            # self._estimation[0], self._estimation[1] = self._estimation[0], self._estimation[1]


    def draw_particles(self):
        if self._particles.shape[0] == 0:
            return
        # Get the indices that would sort the weights array
        sorted_indices = np.argsort(self._weights)

        # Reorder the weights and particles arrays based on the sorted indices
        self._weights = self._weights[sorted_indices]
        self._particles = self._particles[sorted_indices]

        max_weight = self._weights[-1]
        min_weight = self._weights[0]
        for i in range(self._particles.shape[0]):
            particle = self._particles[i]
            color = self.color_for_weight(self._weights[i], max_weight, min_weight)
            self.draw_arrow_at(
                particle[0],
                particle[1],
                particle[2],
                color=color,
                size=0.5
            )


    def convert(self, xy : np.ndarray):
        return np.array([xy[1], -xy[0]])


    def color_for_weight(self, weight, max_weight, min_weight):
        if max_weight == min_weight:
            return np.array([192, 64, 64])
        normalized_weight = (weight - min_weight) / (max_weight - min_weight)
        red = np.array([255, 0, 0])
        gray = np.array([128, 128, 128])
        interpolated_color = ((1 - normalized_weight) * gray + normalized_weight * red).astype(np.uint8)
        return interpolated_color


    def convert_to_sat(self, x, y):
        sat_coord = [
            x,
            y,
        ]
        return sat_coord


    def convert_from_sat(self, trans):
        pygame_coords = np.array([
            trans[0],
            trans[1],
            trans[2]
        ])
        return pygame_coords


    def add_pf_position(self, coord):
        self._pf_positions.append(coord[:2])


    def save_odoms_from_world(self, odoms, times):

        # odoms_img = [self._sat_provider.odometry_real_to_img(odom) for odom in odoms]
        # print(np.array(odoms_img))
        arr = np.array([[odoms[i][0], odoms[i][1], times[i]] for i in range(len(odoms))])
        np.save('/home/fc/catkin_ws/src/cross_view_localization/launch/odoms_tff.npy', arr)
    

    def add_localized_pos(self, pos):
        if pos is None:
            return
        self._localized_path.append(pos)


    def put_text(self, surface, text, position, font_name=None, font_size=30, color=(0, 0, 0)):
        # Initialize Pygame font
        pygame.font.init()

        # Set the font and font size
        if font_name:
            font = pygame.font.SysFont(font_name, font_size)
        else:
            font = pygame.font.Font(None, font_size)

        # Render the text
        text_surface = font.render(text, True, color)
        rectangle_width = text_surface.get_width() + 20  # Add some padding
        rectangle_height = text_surface.get_height() + 20  # Add some padding

        # Calculate the position of the gray rectangle background
        rectangle_x = (position[0] - rectangle_width) // 2
        rectangle_y = (position[1] - rectangle_height) // 2

        # Draw the gray rectangle background
        # pygame.draw.rect(surface, (255, 255, 255), (rectangle_x, rectangle_y, rectangle_width, rectangle_height))

        # Blit (draw) the text surface onto the target surface at the specified position
        surface.blit(text_surface, position)


    def create_final_img(self, stderr : float):
        # Clear the screen
        img_surface = pygame.Surface((self._pygame_sat_img.get_width(), self._pygame_sat_img.get_height() + 60))
        y = self._pygame_sat_img.get_height() + 20
        
        img_surface.fill(self.BACKGROUND_COLOR)

        img_surface.blit(self._pygame_sat_img, (0, 0))
        steps = self._odometry_positions
        if self._odom_steps_predefined is not None:
            steps = self._odom_steps_predefined

        for pos in steps:
            abs_pos = pos + self._odom_origin_trans
            pygame.draw.circle(
                img_surface, (255, 255, 0),
                abs_pos, 3.
            )
        if not (self._ref_coords is None or len(self._ref_coords) == 0):
            path = [coord[1:] for coord in self._ref_coords]
            pygame.draw.lines(img_surface, self.ROBOT_REAL_COLOR, False, path, 4)

        for point in self._painted_points:
            pygame.draw.circle(img_surface, (255, 0, 0), (point[0], point[1]), 5)
        for point in self._localized_path:
            pygame.draw.circle(img_surface, (255, 0, 0), (point[0], point[1]), 3)       
        
        dev = np.round(stderr, 2)
        self.put_text(img_surface, f'MEAN_ERR:  {dev}', (20, y))

        img = cv.transpose(cv.cvtColor(pygame.surfarray.array3d(img_surface), cv.COLOR_BGR2RGB))
        return img


    def update(
            self,
            warped_img,
            odometry_step_img,
            particles,
            weights,
            state_estimate,
            timestamp,
        ):
        self.add_pf_position(state_estimate)
        # self.set_robot_pose(img_robot_trans, phi)
        if warped_img is not None:
            self.set_warp_image(warped_img)
        self.add_odometry_step(odometry_step_img[0], odometry_step_img[1])
        self.set_particles(particles, weights, state_estimate)

        self.add_localized_pos(state_estimate)

        self._timestamp = timestamp