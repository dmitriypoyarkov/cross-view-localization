import cv2 as cv
import numpy as np
from cv2.typing import MatLike


def create_sample_image(width : int, height : int):
    image = np.ones((height, width, 3), dtype=np.uint8) * 255
    image[:, :] = np.array([255, 200, 155])

    # Draw the house (rectangle for the main body and triangle for the roof)
    cv.rectangle(image, (300, 300), (500, 450), (200, 150, 100), -1)
    pts = np.array([[280, 300], [520, 300], [400, 200]], np.int32).reshape((-1, 1, 2))
    cv.fillPoly(image, [pts], (150, 100, 50))
    cv.rectangle(image, (370, 350), (430, 450), (100, 50, 50), -1)

    # Draw the sun
    cv.circle(image, (650, 100), 50, (0, 255, 255), -1)
    for i in range(12):
        angle = i * np.pi / 6
        x1 = int(650 + 50 * np.cos(angle))
        y1 = int(100 + 50 * np.sin(angle))
        x2 = int(650 + 70 * np.cos(angle))
        y2 = int(100 + 70 * np.sin(angle))
        cv.line(image, (x1, y1), (x2, y2), (0, 255, 255), 2)

    # Draw a tree
    cv.rectangle(image, (150, 350), (170, 450), (100, 50, 0), -1)
    cv.circle(image, (160, 300), 50, (34, 139, 34), -1)
    cv.circle(image, (130, 350), 40, (34, 139, 34), -1)
    cv.circle(image, (190, 350), 40, (34, 139, 34), -1)

    image = cv.resize(image, (width, height), interpolation=cv.INTER_AREA)

    return image


def depth_to_rgb(depth_image : MatLike) -> MatLike:
    depth_normalized = cv.normalize(
        depth_image, None, 0, 255, cv.NORM_MINMAX, cv.CV_8U # type: ignore
    )
    depth_colored = cv.applyColorMap(depth_normalized, cv.COLORMAP_JET)
    return depth_colored


def rotate_img(img : np.ndarray, angle_rad):
    if img.shape[0] != img.shape[1]:
        raise Exception('Image should be square')
    center = (img.shape[0] // 2, img.shape[0] // 2)
    out_size = int(img.shape[0] * (np.sin(angle_rad) + np.cos(angle_rad)))
    out_shape = (out_size, out_size)
    angle_deg = np.degrees(angle_rad)
    rotation_matrix = cv.getRotationMatrix2D(center, -angle_deg, 1.0)
    rotated_img = cv.warpAffine(img, rotation_matrix, out_shape, flags=cv.INTER_NEAREST)
    return rotated_img


def put_warp_on_map(warp : MatLike, map : MatLike, particle : np.ndarray):
    rotated = rotate_img(warp, particle[2])

    x = int(particle[0]) - rotated.shape[1] // 2
    y = int(particle[1]) - rotated.shape[0] // 2
    x1 = x + rotated.shape[1]
    y1 = y + rotated.shape[0]

    zero = np.array([0, 0, 0])
    overlay = np.zeros_like(map)
    overlay[y:y1, x:x1] = rotated
    overlay_mask = np.any(overlay != zero, axis=-1)
    map[overlay_mask] = overlay[overlay_mask]
    cv.circle(map, (int(particle[0]), int(particle[1])), 4, (255, 255, 255), -1)
    return map


def add_text_to_image(img : MatLike, text : str):
    bottom_left_corner = (20, img.shape[0] - 20)  # Coordinates for bottom-left corner

    font = cv.FONT_HERSHEY_SIMPLEX
    font_scale = 0.6
    font_thickness = 2
    # text_size, _ = cv.getTextSize(text, font, font_scale, font_thickness)
    # text_width, text_height = text_size
    text_position = (bottom_left_corner[0], bottom_left_corner[1] - 10)  # Adjusted for baseline

    return cv.putText(img, text, text_position, font, font_scale, (255, 255, 255), font_thickness)


def list_unique_colors(image):
    pixels = image.reshape(-1, image.shape[2])
    unique_colors = set(tuple(pixel) for pixel in pixels)
    return unique_colors