import pygame

class Button:
    def __init__(
            self, x, y,
            width, height, color,
            onclick=None,
            text='',
            text_color=(255, 255, 255)
        ):
        self._color = color
        self._text = text
        self._text_color = text_color
        self._font = pygame.font.Font(None, 32)
        self._clicked = False

        self._surface = pygame.Surface((width, height))
        self._surface.fill((100, 100, 100))
        self._rect = self._surface.get_rect(topleft=(x, y))
        self._text_surface = self._font.render(text, True, (0,0,0))
        self._text_rect = self._text_surface.get_rect(center=self._rect.center)

        self._onclick = onclick or self.onclick


    def draw(self, surface):
        # Check if the button is currently hovered over by the mouse
        mouse_pos = pygame.mouse.get_pos()
        hovered = self._rect.collidepoint(mouse_pos)
        surface.blit(self._surface, self._rect)
        surface.blit(self._text_surface, self._text_rect)


    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # Left mouse button
                mouse_pos = pygame.mouse.get_pos()
                if self._rect.collidepoint(mouse_pos):
                    self._clicked = True
                    self._onclick(event)
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:  # Left mouse button
                self._clicked = False


    def onclick(self):
        print('BUTTON ACTION NOT SPECIFIED')


    @property
    def clicked(self):
        return self._clicked

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, new_text):
        self._text = new_text

    @property
    def rect(self):
        return self._rect

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, new_color):
        self._color = new_color

    @property
    def hover_color(self):
        return self._hover_color

    @hover_color.setter
    def hover_color(self, new_hover_color):
        self._hover_color = new_hover_color

    @property
    def text_color(self):
        return self._text_color

    @text_color.setter
    def text_color(self, new_text_color):
        self._text_color = new_text_color

    @property
    def font(self):
        return self._font
    
    @property
    def surface(self):
        return self._surface
    
    @property
    def text_surface(self):
        return self._text_surface

    @property
    def text_rect(self):
        return self._text_rect