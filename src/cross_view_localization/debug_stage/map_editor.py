from cross_view_localization.debug_stage.interactive_pygame import InteractivePygame
from cross_view_localization.objects.sat_img import SatelliteImage
from cross_view_localization.tools.satellite_image_provider import SatelliteImageProvider

SAT_IMG_PATH = '/home/fc/catkin_ws/src/cross_view_localization/sample_input/kurch_b190.bmp'
sat_img = SatelliteImage.from_file(SAT_IMG_PATH)
sat_provider=SatelliteImageProvider(
    sat_img=sat_img,
    comparer=None,
    odometry_provider=None
)

interactive_pygame = InteractivePygame(
    sat_provider, None, None, ref_coords=None
)
interactive_pygame.enable_paint_mode()

interactive_pygame.add_pre_exit_action(interactive_pygame.save_painted_path)
interactive_pygame.run()